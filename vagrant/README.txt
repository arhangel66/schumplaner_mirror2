Before starting to do anything with this, please ensure to have Vagrant and VirtualBox installed in current machine.

0. Ensure settings_local.py is present in <REPO>/kuechengeister before initializing VM:
    
    $ cd <REPO>kuechengeister
    $ cp settings_local_example_vm.py settings_local.py

1. Add base box for the VM:

    $ vagrant box add 'smplan/base' http://139.162.6.75/dl/smplan/smplan.base.box

    You also can rebuild the base box locally using the /vagrant/base profile.

2. At the first time, to initialize new virtual machine with Kuechengeister, inside <REPO>/vagrant/smplan run:

    $ vagrant up --provision --provider=virtualbox

3. After this, the machine should be up and have Kuechengeister ready at:

    http://localhost:8080/

4. To restart the uWSGI processes inside the VM, just need to touch the uwsgi.ini file inside <REPO>/kuechengeister:

    $ touch kuechengeister/uwsgi.ini

5. To shutdown/suspend/resume the machine:
    
    $ vagrant halt/suspend/resume

6. Destroy the virtual machine:
    
    $ vagrant destroy

7. SSH into it:
    
    $ vagrant ssh
