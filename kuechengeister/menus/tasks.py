from datetime import date, datetime

from django.db import transaction
from django.db.models import Q
from django.utils.log import getLogger

from kg import celery_app
from kg.models import Order
from menus.models import MenuPreparation, Menu, Price
from menus.helpers import fill_default_menus, cleanup_menu_preparations
from kg_utils.date_helper import DateHelper


logger = getLogger(__name__)


@celery_app.task
def releaseMealPlan(mealplan_ids=None, year=None, week=None):
    """Release the meal plan. Convert all MenuPreparation objects that are
    created before given date into Menu objects."""
    # Find all current MenuPreparations in order of mealplan__id (specific
    # mealplan objects will be released before general ones). This order is
    # IMPORTANT since specific mealplan menu has higher priority
    # than general mealplan one.
    today_iso = datetime.now().date().isocalendar()
    year = year or today_iso[0]
    week = week or today_iso[1]
    week_days = DateHelper.getDaysInCalendarWeek(year, week)
    filter_params = Q(date__gte=week_days[0]) & Q(date__lte=week_days[-1])

    if mealplan_ids:
        mp_params = Q()
        if None in mealplan_ids:
            mp_params = Q(mealplan=None)
            mealplan_ids.remove(None)
            mp_params |= Q(mealplan__id__in=mealplan_ids)
        else:
            mp_params = Q(mealplan__id__in=mealplan_ids)
        filter_params &= mp_params

    cleanup_menu_preparations()
    qsMenuPreparations = MenuPreparation.objects.filter(
        filter_params).order_by("mealplan__id").prefetch_related()

    with transaction.commit_manually():
        # Handle transaction manually commit.
        try:
            obsolete_preparations = []
            for oMenuPreparation in qsMenuPreparations:
                oMenu = Menu.objects.create(
                    description=oMenuPreparation.description,
                    meal=oMenuPreparation.meal,
                    date=oMenuPreparation.date,
                    order_timelimit=oMenuPreparation.order_timelimit,
                    cancel_timelimit=oMenuPreparation.cancel_timelimit,
                    can_be_ordered=oMenuPreparation.can_be_ordered,
                    disabled=oMenuPreparation.disabled,
                    mealplan_id=oMenuPreparation.mealplan_id,
                    required_menu_id=oMenuPreparation.required_menu_id,
                )
                # Save menu's allergens
                for oAllergen in oMenuPreparation.allergens.all():
                    oMenu.allergens.add(oAllergen)
                # Save menu's additionals
                for oAdditional in oMenuPreparation.additionals.all():
                    oMenu.additionals.add(oAdditional)
                # Save menu's prices
                for oPrice in oMenuPreparation.prices.all():
                    Price.objects.create(
                        menu=oMenu,
                        price_group=oPrice.price_group,
                        price=oPrice.price
                    )

                obsolete_preparations.append(oMenuPreparation.id)
                if not oMenuPreparation.mealplan_id:
                    obsolete_preparations.extend(
                        MenuPreparation.objects.filter(
                            meal=oMenuPreparation.meal,
                            date=oMenuPreparation.date,
                        ).exclude(mealplan=None).values_list('id', flat=True)
                    )

            # Remove the MenuPreparation after the conversion is finished.
            MenuPreparation.objects.filter(
                id__in=obsolete_preparations
            ).delete()
            transaction.commit()
        except:
            transaction.rollback()
            logger.exception('Error occured when releasing meal plans.')


@celery_app.task
def removeInvalidOrders(order_date=None):
    """Remove all orders of given day from suspended customers."""
    logger.info('Perform removing day orders of suspended customers.')
    order_date = order_date or date.today()
    qsOrder = Order.objects.filter(
        Q(child__customer__is_suspended=True) &
        Q(menu__date=order_date)
    )
    lOrderIDs = qsOrder.values_list('pk', flat=True)
    qsOrder.delete()
    return 'Invalid order(s) removed:', lOrderIDs


@celery_app.task
def populateWeekMenus(mealplan=None, week=None, year=None):
    """Auto populate default menus of given week (or current week if not
    provided) then release (publish) all of those. If mealplan is missing,
    general mealplan will be applied."""
    if not week or not year:
        year, week, day = datetime.now().isocalendar()
    logger.info('Automatically create menus for week {}, {}'.format(
        week, year))

    prep_menus = fill_default_menus(mealplan, week, year)
    prep_ids = [prep.id for prep in prep_menus]

    # When fill_default_menus() already has it own rollback process,
    # we keep all the rest in new transaction. And restore the newly
    # created MenuPreparation objects if failed.
    with transaction.commit_manually():
        try:
            for prep_menu in prep_menus:
                menu = Menu.objects.create(
                    description=prep_menu.description,
                    meal=prep_menu.meal,
                    date=prep_menu.date,
                    order_timelimit=prep_menu.order_timelimit,
                    cancel_timelimit=prep_menu.cancel_timelimit,
                    can_be_ordered=prep_menu.can_be_ordered,
                    disabled=prep_menu.disabled,
                    mealplan_id=prep_menu.mealplan_id,
                    required_menu_id=prep_menu.required_menu_id,
                )
                # Save menu's allergens
                for allergen in prep_menu.allergens.all():
                    menu.allergens.add(allergen)
                # Save menu's additionals
                for additional in prep_menu.additionals.all():
                    menu.additionals.add(additional)
                # Save menu's prices
                for price in prep_menu.prices.all():
                    Price.objects.create(
                        menu=menu,
                        price_group=price.price_group,
                        price=price.price
                    )
            MenuPreparation.objects.filter(id__in=prep_ids).delete()
            transaction.commit()
        except Exception as ex:
            msg = u'Error occured when populating menus for {}.'.format(
                mealplan.name)
            logger.exception(msg)
            MenuPreparation.objects.filter(id__in=prep_ids).delete()
            transaction.rollback()
