# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Removing unique constraint on 'Menu', fields ['meal', 'date']
        db.delete_unique('menus_menu', ['meal_id', 'date'])

        # Adding field 'MenuPreparation.disabled'
        db.add_column('menus_menupreparation', 'disabled',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding unique constraint on 'MenuPreparation', fields ['meal', 'date', 'mealplan']
        db.create_unique('menus_menupreparation', ['meal_id', 'date', 'mealplan_id'])

        # Adding field 'Menu.mealplan'
        db.add_column('menus_menu', 'mealplan',
                      self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='menu_set', null=True, to=orm['menus.MealPlan']),
                      keep_default=False)

        # Adding field 'Menu.disabled'
        db.add_column('menus_menu', 'disabled',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding unique constraint on 'Menu', fields ['meal', 'date', 'mealplan']
        db.create_unique('menus_menu', ['meal_id', 'date', 'mealplan_id'])


    def backwards(self, orm):
        # Removing unique constraint on 'Menu', fields ['meal', 'date', 'mealplan']
        db.delete_unique('menus_menu', ['meal_id', 'date', 'mealplan_id'])

        # Removing unique constraint on 'MenuPreparation', fields ['meal', 'date', 'mealplan']
        db.delete_unique('menus_menupreparation', ['meal_id', 'date', 'mealplan_id'])

        # Deleting field 'MenuPreparation.disabled'
        db.delete_column('menus_menupreparation', 'disabled')

        # Deleting field 'Menu.mealplan'
        db.delete_column('menus_menu', 'mealplan_id')

        # Deleting field 'Menu.disabled'
        db.delete_column('menus_menu', 'disabled')

        # Adding unique constraint on 'Menu', fields ['meal', 'date']
        db.create_unique('menus_menu', ['meal_id', 'date'])


    models = {
        'menus.additional': {
            'Meta': {'ordering': "['sortKey']", 'object_name': 'Additional'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name_short': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'sortKey': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'})
        },
        'menus.allergen': {
            'Meta': {'ordering': "['name', 'name_short']", 'object_name': 'Allergen'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name_short': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'use_for_menu': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'menus.meal': {
            'Meta': {'ordering': "['sortKey']", 'object_name': 'Meal'},
            'allow_order': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'cancel_day_limit': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'cancel_day_limit_days': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'cancel_time_limit': ('django.db.models.fields.TimeField', [], {'default': "'00:00:00'"}),
            'deleted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mealtime': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['menus.Mealtime']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'name_pos_app': ('django.db.models.fields.CharField', [], {'max_length': '15', 'blank': 'True'}),
            'name_public_menu': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'name_short': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'order_day_limit': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'order_day_limit_days': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'order_time_limit': ('django.db.models.fields.TimeField', [], {'default': "'00:00:00'"}),
            'show_on_public_menu': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'show_price': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'sortKey': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        'menus.mealdefaultprice': {
            'Meta': {'unique_together': "(('meal', 'price_group'),)", 'object_name': 'MealDefaultPrice'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'meal': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['menus.Meal']"}),
            'price': ('django.db.models.fields.DecimalField', [], {'max_digits': '4', 'decimal_places': '2'}),
            'price_group': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['menus.PriceGroup']"}),
            'price_is_variable': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'menus.mealplan': {
            'Meta': {'ordering': "['name']", 'object_name': 'MealPlan'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'meals': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['menus.Meal']", 'symmetrical': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'menus.mealtime': {
            'Meta': {'ordering': "['sortKey']", 'object_name': 'Mealtime'},
            'id': ('django.db.models.fields.PositiveSmallIntegerField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'name_short': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'sortKey': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        'menus.menu': {
            'Meta': {'ordering': "['-date', 'meal']", 'unique_together': "(('meal', 'date', 'mealplan'),)", 'object_name': 'Menu'},
            'additionals': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['menus.Additional']", 'symmetrical': 'False'}),
            'allergens': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['menus.Allergen']", 'symmetrical': 'False'}),
            'can_be_ordered': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'cancel_timelimit': ('django.db.models.fields.DateTimeField', [], {}),
            'date': ('django.db.models.fields.DateField', [], {}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'disabled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'meal': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['menus.Meal']"}),
            'mealplan': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'menu_set'", 'null': 'True', 'to': "orm['menus.MealPlan']"}),
            'order_timelimit': ('django.db.models.fields.DateTimeField', [], {})
        },
        'menus.menupreparation': {
            'Meta': {'unique_together': "(('meal', 'date', 'mealplan'),)", 'object_name': 'MenuPreparation'},
            'additionals': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['menus.Additional']", 'symmetrical': 'False'}),
            'allergens': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['menus.Allergen']", 'symmetrical': 'False'}),
            'can_be_ordered': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'cancel_timelimit': ('django.db.models.fields.DateTimeField', [], {}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'date': ('django.db.models.fields.DateField', [], {}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'disabled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'meal': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['menus.Meal']"}),
            'mealplan': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'menupreparation_set'", 'null': 'True', 'to': "orm['menus.MealPlan']"}),
            'order_timelimit': ('django.db.models.fields.DateTimeField', [], {})
        },
        'menus.price': {
            'Meta': {'unique_together': "(('menu', 'price_group'),)", 'object_name': 'Price'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'menu': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'prices'", 'to': "orm['menus.Menu']"}),
            'price': ('django.db.models.fields.DecimalField', [], {'max_digits': '4', 'decimal_places': '2'}),
            'price_group': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['menus.PriceGroup']"})
        },
        'menus.pricegroup': {
            'Meta': {'ordering': "['name']", 'object_name': 'PriceGroup'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'menus.pricepreparation': {
            'Meta': {'unique_together': "(('menu', 'price_group'),)", 'object_name': 'PricePreparation'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'menu': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'prices'", 'to': "orm['menus.MenuPreparation']"}),
            'price': ('django.db.models.fields.DecimalField', [], {'max_digits': '4', 'decimal_places': '2'}),
            'price_group': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['menus.PriceGroup']"})
        }
    }

    complete_apps = ['menus']