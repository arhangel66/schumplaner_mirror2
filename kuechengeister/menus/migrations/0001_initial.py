# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Allergen'
        db.create_table('menus_allergen', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('name_short', self.gf('django.db.models.fields.CharField')(max_length=20, blank=True)),
            ('use_for_menu', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('menus', ['Allergen'])

        # Adding model 'Additional'
        db.create_table('menus_additional', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('name_short', self.gf('django.db.models.fields.CharField')(max_length=20, blank=True)),
            ('sortKey', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
        ))
        db.send_create_signal('menus', ['Additional'])

        # Adding model 'Mealtime'
        db.create_table('menus_mealtime', (
            ('id', self.gf('django.db.models.fields.PositiveSmallIntegerField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('name_short', self.gf('django.db.models.fields.CharField')(max_length=20, blank=True)),
            ('sortKey', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
        ))
        db.send_create_signal('menus', ['Mealtime'])

        # Insert standard mealtimes
        db.execute(u"INSERT INTO menus_mealtime (id, name, name_short, \"sortKey\") VALUES (1, 'Frühstück', 'F', 10), (2, 'Mittagessen', 'M', 20), (3, 'Vesper', 'V', 30)")

        # Adding model 'Meal'
        db.create_table('menus_meal', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('name_short', self.gf('django.db.models.fields.CharField')(max_length=20, blank=True)),
            ('name_public_menu', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('name_pos_app', self.gf('django.db.models.fields.CharField')(max_length=15, blank=True)),
            ('mealtime', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['menus.Mealtime'])),
            ('show_on_public_menu', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('show_price', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('allow_order', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('order_day_limit', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('order_time_limit', self.gf('django.db.models.fields.TimeField')(default='00:00:00')),
            ('cancel_day_limit', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('cancel_time_limit', self.gf('django.db.models.fields.TimeField')(default='00:00:00')),
            ('sortKey', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('deleted', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('menus', ['Meal'])

        # Adding model 'MealPlan'
        db.create_table('menus_mealplan', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal('menus', ['MealPlan'])

        # Adding M2M table for field meals on 'MealPlan'
        m2m_table_name = db.shorten_name('menus_mealplan_meals')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('mealplan', models.ForeignKey(orm['menus.mealplan'], null=False)),
            ('meal', models.ForeignKey(orm['menus.meal'], null=False))
        ))
        db.create_unique(m2m_table_name, ['mealplan_id', 'meal_id'])

        # Adding model 'Menu'
        db.create_table('menus_menu', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('meal', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['menus.Meal'])),
            ('date', self.gf('django.db.models.fields.DateField')()),
            ('order_timelimit', self.gf('django.db.models.fields.DateTimeField')()),
            ('cancel_timelimit', self.gf('django.db.models.fields.DateTimeField')()),
            ('can_be_ordered', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal('menus', ['Menu'])

        # Adding unique constraint on 'Menu', fields ['meal', 'date']
        db.create_unique('menus_menu', ['meal_id', 'date'])

        # Adding M2M table for field allergens on 'Menu'
        m2m_table_name = db.shorten_name('menus_menu_allergens')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('menu', models.ForeignKey(orm['menus.menu'], null=False)),
            ('allergen', models.ForeignKey(orm['menus.allergen'], null=False))
        ))
        db.create_unique(m2m_table_name, ['menu_id', 'allergen_id'])

        # Adding M2M table for field additionals on 'Menu'
        m2m_table_name = db.shorten_name('menus_menu_additionals')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('menu', models.ForeignKey(orm['menus.menu'], null=False)),
            ('additional', models.ForeignKey(orm['menus.additional'], null=False))
        ))
        db.create_unique(m2m_table_name, ['menu_id', 'additional_id'])

        # Adding model 'PriceGroup'
        db.create_table('menus_pricegroup', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal('menus', ['PriceGroup'])

        # Adding model 'MealDefaultPrice'
        db.create_table('menus_mealdefaultprice', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('meal', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['menus.Meal'])),
            ('price_group', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['menus.PriceGroup'])),
            ('price', self.gf('django.db.models.fields.DecimalField')(max_digits=4, decimal_places=2)),
            ('price_is_variable', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('menus', ['MealDefaultPrice'])

        # Adding unique constraint on 'MealDefaultPrice', fields ['meal', 'price_group']
        db.create_unique('menus_mealdefaultprice', ['meal_id', 'price_group_id'])

        # Adding model 'Price'
        db.create_table('menus_price', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('menu', self.gf('django.db.models.fields.related.ForeignKey')(related_name='prices', to=orm['menus.Menu'])),
            ('price_group', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['menus.PriceGroup'])),
            ('price', self.gf('django.db.models.fields.DecimalField')(max_digits=4, decimal_places=2)),
        ))
        db.send_create_signal('menus', ['Price'])

        # Adding unique constraint on 'Price', fields ['menu', 'price_group']
        db.create_unique('menus_price', ['menu_id', 'price_group_id'])


    def backwards(self, orm):
        # Removing unique constraint on 'Price', fields ['menu', 'price_group']
        db.delete_unique('menus_price', ['menu_id', 'price_group_id'])

        # Removing unique constraint on 'MealDefaultPrice', fields ['meal', 'price_group']
        db.delete_unique('menus_mealdefaultprice', ['meal_id', 'price_group_id'])

        # Removing unique constraint on 'Menu', fields ['meal', 'date']
        db.delete_unique('menus_menu', ['meal_id', 'date'])

        # Deleting model 'Allergen'
        db.delete_table('menus_allergen')

        # Deleting model 'Additional'
        db.delete_table('menus_additional')

        # Deleting model 'Mealtime'
        db.delete_table('menus_mealtime')

        # Deleting model 'Meal'
        db.delete_table('menus_meal')

        # Deleting model 'MealPlan'
        db.delete_table('menus_mealplan')

        # Removing M2M table for field meals on 'MealPlan'
        db.delete_table(db.shorten_name('menus_mealplan_meals'))

        # Deleting model 'Menu'
        db.delete_table('menus_menu')

        # Removing M2M table for field allergens on 'Menu'
        db.delete_table(db.shorten_name('menus_menu_allergens'))

        # Removing M2M table for field additionals on 'Menu'
        db.delete_table(db.shorten_name('menus_menu_additionals'))

        # Deleting model 'PriceGroup'
        db.delete_table('menus_pricegroup')

        # Deleting model 'MealDefaultPrice'
        db.delete_table('menus_mealdefaultprice')

        # Deleting model 'Price'
        db.delete_table('menus_price')


    models = {
        'menus.additional': {
            'Meta': {'ordering': "['sortKey']", 'object_name': 'Additional'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name_short': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'sortKey': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'})
        },
        'menus.allergen': {
            'Meta': {'ordering': "['name', 'name_short']", 'object_name': 'Allergen'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name_short': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'use_for_menu': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'menus.meal': {
            'Meta': {'ordering': "['sortKey']", 'object_name': 'Meal'},
            'allow_order': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'cancel_day_limit': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'cancel_time_limit': ('django.db.models.fields.TimeField', [], {'default': "'00:00:00'"}),
            'deleted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mealtime': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['menus.Mealtime']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'name_pos_app': ('django.db.models.fields.CharField', [], {'max_length': '15', 'blank': 'True'}),
            'name_public_menu': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'name_short': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'order_day_limit': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'order_time_limit': ('django.db.models.fields.TimeField', [], {'default': "'00:00:00'"}),
            'show_on_public_menu': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'show_price': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'sortKey': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        'menus.mealdefaultprice': {
            'Meta': {'unique_together': "(('meal', 'price_group'),)", 'object_name': 'MealDefaultPrice'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'meal': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['menus.Meal']"}),
            'price': ('django.db.models.fields.DecimalField', [], {'max_digits': '4', 'decimal_places': '2'}),
            'price_group': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['menus.PriceGroup']"}),
            'price_is_variable': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'menus.mealplan': {
            'Meta': {'ordering': "['name']", 'object_name': 'MealPlan'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'meals': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['menus.Meal']", 'symmetrical': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'menus.mealtime': {
            'Meta': {'ordering': "['sortKey']", 'object_name': 'Mealtime'},
            'id': ('django.db.models.fields.PositiveSmallIntegerField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'name_short': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'sortKey': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        'menus.menu': {
            'Meta': {'ordering': "['-date', 'meal']", 'unique_together': "(('meal', 'date'),)", 'object_name': 'Menu'},
            'additionals': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['menus.Additional']", 'symmetrical': 'False'}),
            'allergens': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['menus.Allergen']", 'symmetrical': 'False'}),
            'can_be_ordered': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'cancel_timelimit': ('django.db.models.fields.DateTimeField', [], {}),
            'date': ('django.db.models.fields.DateField', [], {}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'meal': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['menus.Meal']"}),
            'order_timelimit': ('django.db.models.fields.DateTimeField', [], {})
        },
        'menus.price': {
            'Meta': {'unique_together': "(('menu', 'price_group'),)", 'object_name': 'Price'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'menu': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'prices'", 'to': "orm['menus.Menu']"}),
            'price': ('django.db.models.fields.DecimalField', [], {'max_digits': '4', 'decimal_places': '2'}),
            'price_group': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['menus.PriceGroup']"})
        },
        'menus.pricegroup': {
            'Meta': {'ordering': "['name']", 'object_name': 'PriceGroup'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        }
    }

    complete_apps = ['menus']