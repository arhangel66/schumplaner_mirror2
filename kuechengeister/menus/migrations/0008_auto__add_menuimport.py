# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'MenuImport'
        db.create_table('menus_menuimport', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal('menus', ['MenuImport'])

        # Adding M2M table for field allergens on 'MenuImport'
        m2m_table_name = db.shorten_name('menus_menuimport_allergens')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('menuimport', models.ForeignKey(orm['menus.menuimport'], null=False)),
            ('allergen', models.ForeignKey(orm['menus.allergen'], null=False))
        ))
        db.create_unique(m2m_table_name, ['menuimport_id', 'allergen_id'])

        # Adding M2M table for field additionals on 'MenuImport'
        m2m_table_name = db.shorten_name('menus_menuimport_additionals')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('menuimport', models.ForeignKey(orm['menus.menuimport'], null=False)),
            ('additional', models.ForeignKey(orm['menus.additional'], null=False))
        ))
        db.create_unique(m2m_table_name, ['menuimport_id', 'additional_id'])

        # Adding M2M table for field mealtimes on 'MenuImport'
        m2m_table_name = db.shorten_name('menus_menuimport_mealtimes')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('menuimport', models.ForeignKey(orm['menus.menuimport'], null=False)),
            ('mealtime', models.ForeignKey(orm['menus.mealtime'], null=False))
        ))
        db.create_unique(m2m_table_name, ['menuimport_id', 'mealtime_id'])

        # Auto create all MenuImport objects based on existing menus
        print 'Generating MenuImport:'
        mi_count = 0
        for menu in orm.Menu.objects.all().order_by('date'):
            try:
                menu_import = orm.MenuImport.objects.get(
                    description__iexact=menu.description)
                menu_import.allergens.clear()
                menu_import.additionals.clear()
            except orm.MenuImport.DoesNotExist:
                menu_import = orm.MenuImport(
                    description=menu.description)
                menu_import.save()
                mi_count += 1
            # All allergens and additionals info will be overrided by the
            # latest data. However mealtimes will be appended to keep it
            # importable byother meal as well.
            for allergen in menu.allergens.values_list('pk', flat=True):
                menu_import.allergens.add(allergen)
            for additional in menu.additionals.values_list('pk', flat=True):
                menu_import.additionals.add(additional)
            menu_import.mealtimes.add(menu.meal.mealtime)
        print ' - %d MenuImport object(s) created' % mi_count

    def backwards(self, orm):
        # Deleting model 'MenuImport'
        db.delete_table('menus_menuimport')

        # Removing M2M table for field allergens on 'MenuImport'
        db.delete_table(db.shorten_name('menus_menuimport_allergens'))

        # Removing M2M table for field additionals on 'MenuImport'
        db.delete_table(db.shorten_name('menus_menuimport_additionals'))

        # Removing M2M table for field mealtimes on 'MenuImport'
        db.delete_table(db.shorten_name('menus_menuimport_mealtimes'))


    models = {
        'menus.additional': {
            'Meta': {'ordering': "['sortKey']", 'object_name': 'Additional'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name_short': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'sortKey': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'})
        },
        'menus.allergen': {
            'Meta': {'ordering': "['name', 'name_short']", 'object_name': 'Allergen'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name_short': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'use_for_menu': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'menus.meal': {
            'Meta': {'ordering': "['sortKey']", 'object_name': 'Meal'},
            'allow_order': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'cancel_day_limit': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'cancel_day_limit_days': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'cancel_time_limit': ('django.db.models.fields.TimeField', [], {'default': "'00:00:00'"}),
            'deleted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mealtime': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['menus.Mealtime']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'name_pos_app': ('django.db.models.fields.CharField', [], {'max_length': '15', 'blank': 'True'}),
            'name_public_menu': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'name_short': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'order_day_limit': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'order_day_limit_days': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'order_time_limit': ('django.db.models.fields.TimeField', [], {'default': "'00:00:00'"}),
            'show_on_public_menu': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'show_price': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'sortKey': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        'menus.mealdefaultprice': {
            'Meta': {'unique_together': "(('meal', 'price_group'),)", 'object_name': 'MealDefaultPrice'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'meal': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['menus.Meal']"}),
            'price': ('django.db.models.fields.DecimalField', [], {'max_digits': '4', 'decimal_places': '2'}),
            'price_group': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['menus.PriceGroup']"}),
            'price_is_variable': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'menus.mealperiod': {
            'Meta': {'unique_together': "(('meal', 'mealplan', 'from_date', 'to_date'),)", 'object_name': 'MealPeriod'},
            'from_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'meal': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'periods'", 'to': "orm['menus.Meal']"}),
            'mealplan': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'meal_periods'", 'to': "orm['menus.MealPlan']"}),
            'to_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'})
        },
        'menus.mealplan': {
            'Meta': {'ordering': "['name']", 'object_name': 'MealPlan'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'menus.mealtime': {
            'Meta': {'ordering': "['sortKey']", 'object_name': 'Mealtime'},
            'id': ('django.db.models.fields.PositiveSmallIntegerField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'name_short': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'sortKey': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        'menus.menu': {
            'Meta': {'ordering': "['-date', 'meal']", 'unique_together': "(('meal', 'date', 'mealplan'),)", 'object_name': 'Menu'},
            'additionals': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['menus.Additional']", 'symmetrical': 'False'}),
            'allergens': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['menus.Allergen']", 'symmetrical': 'False'}),
            'can_be_ordered': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'cancel_timelimit': ('django.db.models.fields.DateTimeField', [], {}),
            'date': ('django.db.models.fields.DateField', [], {}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'disabled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'meal': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['menus.Meal']"}),
            'mealplan': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'menu_set'", 'null': 'True', 'to': "orm['menus.MealPlan']"}),
            'order_timelimit': ('django.db.models.fields.DateTimeField', [], {})
        },
        'menus.menuimport': {
            'Meta': {'object_name': 'MenuImport'},
            'additionals': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['menus.Additional']", 'symmetrical': 'False'}),
            'allergens': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['menus.Allergen']", 'symmetrical': 'False'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mealtimes': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['menus.Mealtime']", 'symmetrical': 'False'})
        },
        'menus.menupreparation': {
            'Meta': {'unique_together': "(('meal', 'date', 'mealplan'),)", 'object_name': 'MenuPreparation'},
            'additionals': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['menus.Additional']", 'symmetrical': 'False'}),
            'allergens': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['menus.Allergen']", 'symmetrical': 'False'}),
            'can_be_ordered': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'cancel_timelimit': ('django.db.models.fields.DateTimeField', [], {}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'date': ('django.db.models.fields.DateField', [], {}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'disabled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'meal': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['menus.Meal']"}),
            'mealplan': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'menupreparation_set'", 'null': 'True', 'to': "orm['menus.MealPlan']"}),
            'order_timelimit': ('django.db.models.fields.DateTimeField', [], {})
        },
        'menus.price': {
            'Meta': {'unique_together': "(('menu', 'price_group'),)", 'object_name': 'Price'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'menu': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'prices'", 'to': "orm['menus.Menu']"}),
            'price': ('django.db.models.fields.DecimalField', [], {'max_digits': '4', 'decimal_places': '2'}),
            'price_group': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['menus.PriceGroup']"})
        },
        'menus.pricegroup': {
            'Meta': {'ordering': "['name']", 'object_name': 'PriceGroup'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'menus.pricepreparation': {
            'Meta': {'unique_together': "(('menu', 'price_group'),)", 'object_name': 'PricePreparation'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'menu': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'prices'", 'to': "orm['menus.MenuPreparation']"}),
            'price': ('django.db.models.fields.DecimalField', [], {'max_digits': '4', 'decimal_places': '2'}),
            'price_group': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['menus.PriceGroup']"})
        }
    }

    complete_apps = ['menus']
