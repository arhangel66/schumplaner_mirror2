"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""
import locale
import json

from datetime import datetime, date, time
from django.test import TestCase
from django.utils import simplejson
from django.conf import settings
from django.db.utils import IntegrityError
from django.db import transaction, DatabaseError

from kg.tests import AdminTestMixin
from kg.models import Order
from menus.helpers import cleanup_menu_preparations
from menus.models import Menu, MealPlan, PriceGroup, MenuPreparation

locale.setlocale(
    locale.LC_ALL, (settings.LANGUAGE_CODE.replace('-', '_'), 'UTF-8'))


class MenuAdminTestCase(AdminTestMixin, TestCase):
    url = '/admin/menus/menu/'

    def setUp(self):
        super(MenuAdminTestCase, self).setUp()
        self._year, self._week, self._weekday = datetime.today().isocalendar()

    def get_menu_index(self, url):
        """Get menu admin index with given url."""
        res = self.client.get(url)
        self.assertEqual(res.status_code, 200)
        fields = ['module_name', 'has_add_permission', 'app_label',
                  'admin_url', 'week_days', 'year', 'week_num',
                  'weeks_for_links', 'current_week_num', 'meals', 'allergens',
                  'additionals', 'has_preparation', 'avail_mealplans',
                  'selected_mealplan']
        for field in fields:
            self.assertIn(field, res.context)
        self.assertEqual(len(res.context['allergens']), 3)
        self.assertEqual(len(res.context['additionals']), 3)
        self.assertEqual(len(res.context['avail_mealplans']), 4)
        self.assertEqual(len(res.context['weeks_for_links']), 11)
        return res

    def test_general_menu_index(self):
        url = '{}?week_num=29&year=2016'.format(self.url)
        res = self.get_menu_index(url)
        self.assertEqual(res.context['has_preparation'], True)
        self.assertEqual(len(res.context['meals']), 4)
        self.assertEqual([meal.id for meal in res.context['meals']],
                         [1, 2, 3, 4])

    def test_specific_mealplan_menu_index(self):
        url = '{}?week_num=31&year=2016&mealplan=4'.format(self.url)
        res = self.get_menu_index(url)
        self.assertEqual(res.context['has_preparation'], True)
        self.assertEqual(len(res.context['meals']), 2)
        self.assertEqual([meal.id for meal in res.context['meals']],
                         [2, 3])

    def ajax_post(self, url, data):
        res = self.client.post(url, data,
                               HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(res.status_code, 200)
        return res

    def save_menu(self, **kwargs):
        url = '{}save/'.format(self.url)
        data = {
            'menu_id': '331',
            'meal_id': '2',
            'menu_date': '21-7-2026',
            'is_preparation': '',
            'mealplan_id': '',
            'description': 'New description',
            'can_be_ordered': '1',
            'allergens': ['1', '2'],
            'additionals': ['1', '2'],
            'order_timelimit': '27.07.2016 23:59',
            'cancel_timelimit': '21.08.2016 08:00',
            'price_fixed': ['1,00', '2,00', '3,00', '4,00', '5,00', '6,00',
                            '7,00', '8,00'],
            'price_group_id_fixed': ['1', '2', '3', '4', '5', '6', '7', '8']
        }
        data.update(kwargs)
        res = self.ajax_post(url, data)
        # Check the menu after updating.
        query_params = dict(
            meal_id=data['meal_id'],
            date=datetime.strptime(data['menu_date'], '%d-%m-%Y').date()
        )
        if data['mealplan_id']:
            query_params['mealplan_id'] = data['mealplan_id']
        menu = Menu.objects.get(**query_params)
        self.assertEqual(menu.description, 'New description')
        self.assertEqual(menu.order_timelimit, datetime(2016, 7, 27, 23, 59))
        self.assertEqual(menu.cancel_timelimit, datetime(2016, 8, 21, 8, 0))
        self.assertEqual({item.id for item in menu.allergens.all()}, {1, 2})
        self.assertEqual({item.id for item in menu.additionals.all()}, {1, 2})
        prices = [float(price.price) for price in menu.prices.order_by('id')]
        self.assertEqual(prices,  [1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0])
        result = json.loads(res.content)
        self.assertTrue(result['success'])
        return menu

    def test_edit_general_menu(self):
        menu = self.save_menu(can_be_ordered='1')
        self.assertEqual(menu.can_be_ordered, True)

    def test_disable_order_general_menu(self):
        menu = self.save_menu(menu_id='222', can_be_ordered='', meal_id='3')
        self.assertEqual(menu.can_be_ordered, False)
        self.assertEqual(menu.order_set.count(), 0)

    def test_drag_and_drop_general_menu(self):
        url = '/admin/menus/menu/save_droppable/?source_menu_id=331&' \
              'target_menu_id=329&target_meal_id=2&target_date=20-7-2016'
        res = self.client.get(url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(res.status_code, 200)
        result = json.loads(res.content)
        self.assertTrue(result['success'])
        source = Menu.objects.get(pk=331)
        target = Menu.objects.get(pk=329)
        self.assertEqual(source.description, target.description)
        self.assertEqual(list(source.allergens.all()),
                         list(target.allergens.all()))
        self.assertEqual(list(source.additionals.all()),
                         list(target.additionals.all()))
        self.assertEqual(list(source.prices.values('price')),
                         list(target.prices.values('price')))

    def test_save_specific_mealplan_menu(self):
        menu = self.save_menu(menu_id='333', meal_id='2', mealplan_id='4')
        self.assertEqual(menu.mealplan_id, 4)
        self.assertNotEqual(menu.id, 333)

    def test_search_menu_to_import(self):
        url = '/admin/menus/menu/search/?phrase=Ves'
        res = self.client.get(url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(res.status_code, 200)
        try:
            json_resp = simplejson.loads(res.content)
        except:
            json_resp = None
        self.assertIsNotNone(json_resp)
        self.assertEqual(len(json_resp), 1)
        self.assertEqual({item['description'] for item in json_resp},
                         {'Vesper'})

    def test_load_menu(self):
        res = self.client.get(
            '/admin/menus/menu/load-menu/?menu_id=352&meal_id=1&menu_date=05-08-2016')
        self.assertEqual(res.status_code, 200)
        self.assertEqual(
            simplejson.loads(res.content),
            {
                "order_timelimit": "27-07-2016 23:59",
                "description": "Vesper",
                "disabled": False,
                "required_menu": None,
                "id": 352,
                "allergens": [2, 1, 3],
                "mealplan": 3,
                "fixed_prices": [
                    {
                        "price_group": "KITA Grieben",
                        "price": "0,55",
                        "price_group_id": 5,
                        "id": 2803, "object_id": "28035"
                    }
                ],
                "cancel_timelimit": "05-08-2016 08:00",
                "in_past": True,
                "additionals": [1, 6, 7, 8],
                "inherited": False,
                "can_be_ordered": True,
                "variable_prices": []
            }
        )

    def test_download_pdf(self):
        res = self.client.post(
            self.url,
            {
                'download-pdf': 1,
            }
        )
        self.assertEqual(res.status_code, 200)
        self.assertEqual(res._headers['content-type'][1], 'application/pdf')
        self.assertEqual(
            res._headers['content-disposition'][1].split('=')[-1],
            u'Speiseplan Alle-{0}-{1}.pdf'.format(self._week, self._year)
        )

    def test_download_pdf_week(self):
        week = self._week + 2 if self._week < 49 else self._week - 2
        res = self.client.post(
            self.url+'?year={0}&week_num={1}'.format(self._year, week),
            {
                'download-pdf': 1,
            }
        )
        self.assertEqual(res.status_code, 200)
        self.assertEqual(res._headers['content-type'][1], 'application/pdf')
        self.assertEqual(
            res._headers['content-disposition'][1].split('=')[-1],
            u'Speiseplan Alle-{0}-{1}.pdf'.format(week, self._year)
        )

    def test_download_mealplan_pdf(self):
        week = self._week + 2 if self._week < 49 else self._week - 2
        mealplan = MealPlan.objects.get(pk=3)
        res = self.client.post(
            self.url+'?mealplan=3&week_num='+str(week),
            {
                'download-pdf': 1,
                'pricegroup': ''
            }
        )
        self.assertEqual(res.status_code, 200)
        self.assertEqual(res._headers['content-type'][1], 'application/pdf')
        self.assertEqual(
            res._headers['content-disposition'][1].split('=')[-1],
            u'Speiseplan {0}-{1}-{2}.pdf'.format(
                mealplan.name, week, self._year)
        )

    def test_download_mealplan_pdf_pricegroup(self):
        week = self._week + 2 if self._week < 49 else self._week - 2
        pricegroup = PriceGroup.objects.get(pk=5)
        res = self.client.post(
            self.url+'?mealplan={0}&week_num={1}'.format(
                pricegroup.mealplan.id, week),
            {
                'download-pdf': 1,
                'pricegroup': pricegroup.id
            }
        )
        self.assertEqual(res.status_code, 200)
        self.assertEqual(res._headers['content-type'][1], 'application/pdf')
        self.assertEqual(
            res._headers['content-disposition'][1].split('=')[-1],
            u'Speiseplan {0}-{1}-{2}.pdf'.format(
                pricegroup.mealplan.name, week, self._year)
        )


class MealAdminTestCase(AdminTestMixin, TestCase):
    url = '/admin/menus/meal/'

    def test_change_list(self):
        res = self.client.get(self.url)
        self.assertEqual(res.status_code, 200)

    def test_change_form(self):
        res = self.client.get(self.url+'1/')
        self.assertEqual(res.status_code, 200)
        self.assertEqual(
            res.context['adminform'].form.initial,
            {
                'order_time_limit': time(23, 59, 59),
                'cancel_time_limit': time(8, 0),
                'name': u'Fr\xfchst\xfcck',
                'order_day_limit': 30,
                'sortKey': 10,
                'name_public_menu': u'Fr\xfchst\xfcck',
                'cancel_day_limit_days': 0,
                'name_pos_app': u'Fr\xfchst\xfcck',
                'default_menu': None,
                'name_short': u'F',
                'deleted': False,
                'allow_order': True,
                'allergy_meal': False,
                'order_day_limit_days': 27,
                'id': 1,
                'mealtime': 1,
                'cancel_day_limit': 10
            }
        )


class MenuReleaseTestCase(TestCase):
    fixtures = ['tests/user.json', 'tests/menus.json', 'tests/kg.json']

    def test_cleanup_menu_general(self):
        preparation_total = MenuPreparation.objects.count()
        data = {
            "order_timelimit": "2016-07-27T23:59:00",
            "cancel_timelimit": "2016-08-04T08:00:00",
            "date": "2016-08-04",
            "mealplan": None,
            "description": "Just another menu",
            "can_be_ordered": True,
            "meal_id": 1
        }
        preparation = MenuPreparation(**data)
        MenuPreparation.objects.bulk_create((preparation,))
        cleanup_menu_preparations()
        self.assertTrue(
            not MenuPreparation.objects.filter(pk=preparation.pk).exists())
        self.assertEqual(MenuPreparation.objects.count(), preparation_total)

    def test_cleanup_menu_with_mealplan(self):
        data = {
            "order_timelimit": "2016-06-27T23:59:00",
            "cancel_timelimit": "2016-07-22T08:00:00",
            "date": "2016-07-22",
            "mealplan_id": 1,
            "description": "Just another menu",
            "can_be_ordered": True,
            "meal_id": 3
        }
        preparation = MenuPreparation(**data)
        MenuPreparation.objects.bulk_create((preparation,))
        cleanup_menu_preparations()
        self.assertTrue(
            not MenuPreparation.objects.filter(pk=preparation.pk).exists())

    def test_cleanup_menu_with_different_mealplan(self):
        data = {
            "order_timelimit": "2016-07-27T23:59:00",
            "cancel_timelimit": "2016-08-04T08:00:00",
            "date": "2016-08-04",
            "mealplan_id": 3,
            "description": "Just another menu",
            "can_be_ordered": True,
            "meal_id": 1
        }
        preparation = MenuPreparation.objects.create(**data)
        cleanup_menu_preparations()
        self.assertTrue(
            MenuPreparation.objects.filter(pk=preparation.pk).exists())


class MenuDuplicationTestCase(TestCase):
    fixtures = ['tests/user.json', 'tests/menus.json', 'tests/kg.json']

    def __init__(self, *args, **kwargs):
        super(MenuDuplicationTestCase, self).__init__(*args, **kwargs)
        self.data_mealplan = {
            "order_timelimit": "2017-07-27T23:59:00",
            "mealplan_id": 3,
            "description": "Just another menu",
            "cancel_timelimit": "2017-08-04T08:00:00",
            "date": datetime.strptime("2017-08-04", '%Y-%m-%d'),
            "can_be_ordered": True,
            "meal_id": 2
        }
        self.data_general = {
            "order_timelimit": "2017-07-27T23:59:00",
            "mealplan": None,
            "description": "Just another menu",
            "cancel_timelimit": "2017-08-04T08:00:00",
            "date": datetime.strptime("2017-08-04", '%Y-%m-%d'),
            "can_be_ordered": True,
            "meal_id": 1
        }

    def setUp(self):
        for cls in (Menu, MenuPreparation):
            cls.objects.all().delete()

    def test_mealplan_preparation(self):
        prep0 = prep1 = None
        prep0 = MenuPreparation.objects.create(**self.data_mealplan)
        with self.assertRaises(IntegrityError):
            prep1 = MenuPreparation.objects.create(**self.data_mealplan)
        self.assertIsNotNone(prep0)
        self.assertIsNone(prep1)

    def test_mealplan_menu(self):
        menu0 = menu1 = None
        menu0 = Menu.objects.create(**self.data_mealplan)
        with self.assertRaises(IntegrityError):
            menu1 = Menu.objects.create(**self.data_mealplan)
        self.assertIsNotNone(menu0)
        self.assertIsNone(menu1)

    def test_general_preparation(self):
        preparation_total = MenuPreparation.objects.count()
        prep0 = prep1 = None
        prep0 = MenuPreparation.objects.create(**self.data_general)
        with self.assertRaises(IntegrityError):
            prep1 = MenuPreparation.objects.create(**self.data_general)
        self.assertEqual(preparation_total+1, MenuPreparation.objects.count())
        self.assertIsNotNone(prep0)
        self.assertIsNone(prep1)

    def test_general_menu(self):
        menu_total = Menu.objects.count()
        menu0 = menu1 = None
        menu0 = Menu.objects.create(**self.data_general)
        with self.assertRaises(IntegrityError):
            menu1 = Menu.objects.create(**self.data_general)
        self.assertEqual(menu_total+1, Menu.objects.count())
        self.assertIsNotNone(menu0)
        self.assertIsNone(menu1)

    def test_add_preparation_to_general_menu(self):
        menu_total = Menu.objects.count()
        menu = Menu.objects.create(**self.data_general)
        prep = None
        with self.assertRaises(IntegrityError):
            prep = MenuPreparation.objects.create(**self.data_general)
        self.assertEqual(menu_total+1, Menu.objects.count())
        self.assertIsNotNone(menu)
        self.assertIsNone(prep)

    def test_add_preparation_to_mealplan_menu(self):
        menu_total = Menu.objects.count()
        menu = Menu.objects.create(**self.data_mealplan)
        prep = None
        with self.assertRaises(IntegrityError):
            prep = MenuPreparation.objects.create(**self.data_mealplan)
        self.assertEqual(menu_total+1, Menu.objects.count())
        self.assertIsNotNone(menu)
        self.assertIsNone(prep)


class MenuPreparationTestCase(TestCase):
    fixtures = ['tests/user.json', 'tests/menus.json', 'tests/kg.json']
    menu_data = {
        "mealplan_id": 3,
        "meal_id": 4,
        "order_timelimit": "2017-07-27T23:59:00",
        "description": "Menu description",
        "cancel_timelimit": "2017-08-04T08:00:00",
        "date": datetime.strptime("2017-08-04", '%Y-%m-%d'),
        "can_be_ordered": True,
    }

    def test_meal_of_single_mealplan(self):
        menu_preparation = MenuPreparation.objects.create(**self.menu_data)
        self.assertIsNone(menu_preparation.mealplan)

    def test_meal_of_multiple_mealplan(self):
        new_data = self.menu_data.copy()
        new_data['meal_id'] = 2
        menu_preparation = MenuPreparation.objects.create(**new_data)
        self.assertEqual(menu_preparation.mealplan.id, 3)

    def test_existing_menu_preparation(self):
        MenuPreparation.objects.bulk_create([MenuPreparation(**self.menu_data)])
        menu_preparation = MenuPreparation.objects.latest('id')
        self.assertEqual(menu_preparation.mealplan.id, 3)
        menu_preparation.save()
        self.assertIsNone(menu_preparation.mealplan)


class OrderTransferringTestCase(TestCase):
    fixtures = ['tests/user.json', 'tests/menus.json', 'tests/kg.json']

    def setUp(self):
        self.menu_data = {
            "order_timelimit": datetime(2017, 7, 12, 10, 20),
            "cancel_timelimit": datetime(2017, 7, 13, 10, 20),
            "description": "Fr\u00fchst\u00fcck",
            "disabled": False,
            "can_be_ordered": True,
            "meal_id": 2,
        }
        self.order_data = {
            "modified_by_id": 162,
            "price": "1.50",
            "last_modified": datetime(2017, 7, 2, 10, 20),
            "child_id": 312,
            "quantity": 1
        }
        self.mealplan = MealPlan.objects.get(pk=1)
        self.general_menu = Menu.objects.create(
            date=date(2017, 7, 10),
            **self.menu_data
        )
        self.specific_menu = Menu.objects.create(
            date=date(2017, 7, 11),
            mealplan=self.mealplan,
            **self.menu_data
        )
        self.order = Order.objects.create(
            menu=self.general_menu,
            **self.order_data
        )
        self.order = Order.objects.create(
            menu=self.specific_menu,
            **self.order_data
        )

    def test_create_specific_menu(self):
        orders = self.general_menu.order_set.all()
        self.assertEqual(orders.count(), 1)
        specific_menu = Menu.objects.create(
            date=date(2017, 7, 10),
            mealplan=self.mealplan,
            **self.menu_data
        )
        self.assertEqual(self.general_menu.order_set.count(), 0)
        self.assertEqual(specific_menu.order_set.count(), 1)
        for order in orders:
            self.assertEqual(order.menu, specific_menu)

    def test_create_general_menu(self):
        self.assertEqual(self.specific_menu.order_set.count(), 1)
        general_menu = Menu.objects.create(
            date=date(2017, 7, 11),
            mealplan=None,
            **self.menu_data
        )
        self.assertEqual(self.specific_menu.order_set.count(), 1)
        self.assertEqual(general_menu.order_set.count(), 0)

    def test_delete_specific_menu(self):
        general_menu = Menu.objects.create(
            date=date(2017, 7, 11),
            mealplan=None,
            **self.menu_data
        )
        self.assertEqual(self.specific_menu.order_set.count(), 1)
        self.assertEqual(general_menu.order_set.count(), 0)
        self.specific_menu.delete()
        self.assertEqual(general_menu.order_set.count(), 1)
