# -*- coding: utf-8 -*-

"""
 The site specific settings

 Overwrite the site wide settings
 Configure all the site specific settings that are not confidential
 The confidential settings are set in /settings_local.py
"""

# New Customer Configuration
EXIST_CUST_REGISTRATION_TYPE = 'CUSTOMERNUMBER'
EXIST_CUST_REGISTRATION = 'Kundennummer'
NEW_CUSTOMER_FORM_AGB_CHECKBOX = False


# Set number of weeks to show in menu view
MENU_WEEKS_CUSTOMER = 6
MENU_WEEKS_HOMEPAGE = 4
MENU_START_DATE = '07.08.2017'

# E-MAIL Configuration
DEFAULT_FROM_EMAIL = 'Evangelische Schule Dettmannsdorf <bestellsystem@esd-essen.de>'


# SEPA Configuration
SEPA = {'SEPA_NAME': '',
        'SEPA_EREF_PREFIX': '',
        'SEPA_CREDITOR_ID': '',
        'SEPA_IBAN': '',
        'SEPA_BIC': '',
        'SEPA_DEBIT_FRST_DAYS': '3',
        'SEPA_DEBIT_RCUR_DAYS': '3',
        'SEPA_FORM_COMPANY_LONG': '',
        'SEPA_FORM_COMPANY_SHORT': '',
        }


# Site specific context
SITE_CONTEXT = {'SITE_HTML_TITLE'                      : 'ESD-Essen',
                'SITE_HTML_ADMIN_TITLE'                : 'ESD-Essen - Verwaltung',
                'SITE_NAME'                            : 'www.esd-essen.de',
                'SITE_URL'                             : 'https://www.esd-essen.de',
                'SITE_REGARDS'                         : 'Ihre Evangelische Schule Dettmannsdorf',
                'SITE_HEADER_SUBTITLE'                 : '',
                'SITE_HEADER_BACKGROUND'               : '6ca80e',
                'INVOICE_COMPANY_NAME'                 : 'Evangelische Schule Dettmannsdorf',
                'INVOICE_COMPANY_NAME_1'               : '',
                'INVOICE_COMPANY_NAME_2'               : '',
                'INVOICE_COMPANY_ADDRESS_STREET'       : '',
                'INVOICE_COMPANY_ADDRESS_POSTCODE_TOWN': '',
                'INVOICE_COMPANY_TELEFONE'             : '',
                'INVOICE_COMPANY_EMAIL'                : '',
                'INVOICE_COMPANY_TAXID'                : '',
                'INVOICE_COMPANY_ACCOUNT_NUMBER'       : '',
                'INVOICE_COMPANY_ACCOUNT_SORTCODE'     : '',
                'INVOICE_COMPANY_ACCOUNT_BANKNAME'     : '',
                'INVOICE_COMPANY_ACCOUNT_IBAN'         : '',
                'INVOICE_COMPANY_ACCOUNT_BIC'          : '',
                'INVOICE_PAYMENT_PERIOD_IN_DAYS'       : 10,
                'INVOICE_SHOW_TAX'                     : True,
                'PRINTMENUE_COLORSTRONG'               : '6CA80E',
                'PRINTMENUE_COLORLIGHT'                : 'E4E4E4',
                'MENU_SUBTEXT'                         : '',
                'MENU_FOOTER_LINE_1'                   : 'Änderungen und Irrtümer vorbehalten',
                'MENU_ADDITIVES'                       : 'Zusatzstoffe: 1 mit Farbstoff; 2 mit Konservierungsstoff;  3 mit Antioxidationsmittel; 4 mit Geschmacksverstärker; 5 gewachst; 6 geschwärzt; 7 geschwefelt<br />Allergene: A1 Glutenhaltiges Getreide; B Milch, einschl. Laktose; C Schalenfrüchte; D Fisch; E Erdnüsse; F Eier; G Soja; H Nüsse; I Sellerie; K Sesam; L Schwefeldioxid/ Sulfite; M Senf; N Lupine; O Fruktose',
                }

# Prepaid module settings
MODULE_PREPAID = True
PREPAID_SETTINGS_CREDIT = '0.00'  # the credit the customer is allowed to have in his account
PREPAID_SETTINGS = {'SEPA_DEBIT': False,
                    'BANK_TRANSFER': True,
                    'FLATRATE': False,
                    }

MODULE_FLATRATE = False

# Paper Invoice Module
MODULE_PAPER_INVOICE = False
PAPER_INVOICE_SETTINGS = {'SITE_TEXT': '<p>Bitte beachten Sie: <br />Für den Postversand von Rechnungen in Papierform berechnen wir Unkosten von 1,00 € pro Rechnung.',  # individual text on site views_kunden.rechnungen_papier as html
                          'INVOICE_TEXT': 'Rechnung in Papierform',  # individual text on invoice
                          'INVOICE_FEE': '1.00',  # the fee for the paper invoice as String
                          }

# Accounting Module Configuration
MODULE_ACCOUNTING = False
ACCOUNTING_SETTINGS = { 'CSV_DELIMITER': ';',  # the csv delimiter
                        'CSV_CONTRAACCOUNT_NUMBER': 'Auftraggeber IBAN',  # Name des Gegenkonto Kontonummer Kopffeldes
                        'CSV_CONTRAACCOUNT_ICODE': 'Auftraggeber BIC',  # Name des Gegenkonto Bankleitzahl Kopffeldes
                        'CSV_CONTRAACCOUNT_OWNER': 'Auftraggebername',  # Name des Gegenkonto Inhaber Kopffeldes
                        'CSV_DATE': 'Wertstellung',  # Name des Datum Kopffeldes
                        'CSV_DATE_FORMAT': '%d.%m.%Y',  # date format for parsing with strptime() function
                        'CSV_SUBJECT_FIELDS': ['Verwendungszweck 4', 'Verwendungszweck 5',
                                               'Verwendungszweck 6'],  # Name der Verwendungszweck Kopffelder
                        'CSV_AMOUNT': 'Betrag',  # Name des Betrag Kopffeldes
                        'CSV_TYPE': 'GSV',  # Name des Buchungstyp Kopffeldes
                        'CSV_TYPE_CHOICES': {'TYPE_BANK_TRANSFER_IN': ['166',],  # Überweisung
                                             'TYPE_DEBIT': [],  # Lastschrift
                                             'TYPE_DEBIT_RETURN': [], # Rücklastschrift
                                            },
                        'CSV_REGEX': 'Verpflegung.*?([0-9]{5}) ',
                        'DEBIT_RETURN_FEE': 'ORIGINAL',  # Kosten für Lastschriftrückgabe
                        'REMINDER_FEE': '3.00',  # Kosten für 1. Mahnung
                        'REMINDER_FEE_2': '6.00',  # Kosten für 2. Mahnung
                        'REMINDER_FEE_3': '9.00',  # Kosten für 3. Mahnung
                        'REMINDER_PAYMENT_PERIOD': 10,  # Zahlungsfrist für 1. Mahnung
                        'REMINDER_PAYMENT_PERIOD_2': 5,  # Zahlungsfrist für 2. Mahnung
                        'REMINDER_PAYMENT_PERIOD_3': 5,  # Zahlungsfrist für 3. Mahnung
                        }