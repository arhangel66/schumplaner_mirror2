# -*- coding: utf-8 -*-

"""
 The site specific settings

 Overwrite the site wide settings
 Configure all the site specific settings that are not confidential
 The confidential settings are set in /settings_local.py
"""

DEMO_MODUS = True

# New Customer Configuration
EXIST_CUST_REGISTRATION_TYPE = 'CUSTOMERNUMBER'
EXIST_CUST_REGISTRATION = 'Kundennummer'
NEW_CUSTOMER_FORM_AGB_CHECKBOX = False

# E-MAIL Configuration
DEFAULT_FROM_EMAIL = 'Schulmenüplaner Demo System <support@schulmenueplaner.de>'

# API Key
API_KEY = 'YLqJExvrepEULxanXMcCmrFkzBrX24'

# SEPA Configuration
SEPA = {'SEPA_NAME': 'Demo GmbH',
        'SEPA_EREF_PREFIX': 'DEMO',
        'SEPA_CREDITOR_ID': 'DE91ZZZ00000647900',
        'SEPA_IBAN': 'DE0011112222333344',
        'SEPA_BIC': 'COBADEFFXXX',
        'SEPA_DEBIT_FRST_DAYS': 6,
        'SEPA_DEBIT_RCUR_DAYS': 3,
        'SEPA_FORM_COMPANY_LONG': 'die Demo GmbH, Musterstraße 33, 09123 Musterstadt',
        'SEPA_FORM_COMPANY_SHORT': 'der Demo GmbH',
        }


# Site specific context
SITE_CONTEXT = {'SITE_HTML_TITLE'                      : 'Schulmenüplaner Demo',
                'SITE_HTML_ADMIN_TITLE'                : 'Schulmenüplaner Demo - Verwaltung',
                'SITE_NAME'                            : 'demo.schulmenueplaner.de',
                'SITE_URL'                             : 'http://demo.schulmenueplaner.de',
                'SITE_REGARDS'                         : 'Ihr Schulmenüplaner Team',
                'SITE_HEADER_SUBTITLE'                 : '<em>Das online Schulessenbestellsystem</em>',
                'SITE_HEADER_BACKGROUND'               : '3a622e',
                'INVOICE_COMPANY_NAME'                 : 'Demo GmbH',
                'INVOICE_COMPANY_NAME_1'               : '',
                'INVOICE_COMPANY_NAME_2'               : '',
                'INVOICE_COMPANY_ADDRESS_STREET'       : 'Musterstraße 123',
                'INVOICE_COMPANY_ADDRESS_POSTCODE_TOWN': '09123 Musterstadt',
                'INVOICE_COMPANY_TELEFONE'             : '(0371) 1234567',
                'INVOICE_COMPANY_EMAIL'                : 'info@schulmenueplaner.de',
                'INVOICE_COMPANY_TAXID'                : '000/111/12345',
                'INVOICE_COMPANY_ACCOUNT_BANKNAME'     : 'Musterbank',
                'INVOICE_COMPANY_ACCOUNT_IBAN'         : 'DE22 1111 0000 2222 3333 44',
                'INVOICE_COMPANY_ACCOUNT_BIC'          : 'COBADEFFXXX',
                'INVOICE_PAYMENT_PERIOD_IN_DAYS'       : 10,
                'INVOICE_SHOW_TAX'                     : True,
                'PRINTMENUE_COLORSTRONG'               : '3a622e',
                'PRINTMENUE_COLORLIGHT'                : 'cce5cc',
                'MENU_FOOTER_LINE_1'                   : 'Änderungen und Irrtümer vorbehalten',
                'MENU_ADDITIVES'                       : 'Zusatzstoffe: 1 mit Farbstoff, 2 mit Konservierungsstoff,  3 mit Phosphat, 4 geschwefelt, 5 mit Antioxidationsmittel, 6 mit Geschmacksverstärker, 7 geschwärzt, 8 gewachst (nicht zum Verzehr geeignet), 9 mit Süßstoff (Cyclamat, Saccharin u.a.), 10 enthält eine Phenylalaninquelle, 11 kann bei übermäßigem Verzehr abführend wirken, 12 mit Milcheiweiß, 13 mit Eiklar, 14 gentechnisch verändert, 15 bestrahlt, 16 chininhaltig, 17 koffeinhaltig<br />Allergene: A Sellerie*, B Weichtiere*, C Fisch*, D Sesam*, E Schalenfrüchte (Nüsse)*, F Soja*, G Eier*, H Lupine*, I Schwefeldioxid/Sulfite, J Krebstiere*, K Milch (einschließl. Laktose)*,  L Glutenhaltiges Getreide*, M Erdnüsse*, N Senf* (* und daraus hergestellte Erzeugnisse)',
                }


# Set number of weeks to show in menu view
MENU_WEEKS_CUSTOMER = 4
MENU_WEEKS_HOMEPAGE = 4

WEEK_DAYS = 7

CANTEEN_MODE = True  # Canteen mode on
MODULE_TERMINAL = True

MODULE_FLATRATE = False  # No Flatrate

# Prepaid module settings
MODULE_PREPAID = False

# Accounting Module Configuration
MODULE_ACCOUNTING = True
ACCOUNTING_SETTINGS = { 'CSV_DELIMITER': ';',  # the csv delimiter
                        'CSV_ACCOUNT_NUMBER': 'Kontonummer',  # Name des Konto Kontonummer Kopffeldes
                        'CSV_ACCOUNT_ICODE': 'BLZ',  # Name des Konto Bankleitzahl Kopffeldes
                        'CSV_CONTRAACCOUNT_NUMBER': 'Gegenkonto',  # Name des Gegenkonto Kontonummer Kopffeldes
                        'CSV_CONTRAACCOUNT_ICODE': 'Gegenkonto BLZ',  # Name des Gegenkonto Bankleitzahl Kopffeldes
                        'CSV_CONTRAACCOUNT_OWNER': 'Gegenkonto Inhaber',  # Name des Gegenkonto Inhaber Kopffeldes
                        'CSV_DATE': 'Datum',  # Name des Datum Kopffeldes
                        'CSV_DATE_FORMAT': '%d.%m.%Y',  # date format for parsing with strptime() function
                        'CSV_SUBJECT_FIELDS': ['Verwendungszweckzeile 1',
                                               'Verwendungszweckzeile 2'
                                               ],  # Liste mit Namen aller Verwendungszweck Kopffelder
                        'CSV_AMOUNT': 'Betrag',  # Name des Betrag Kopffeldes
                        'CSV_TYPE': 'Art',  # Name des Buchungstyp Kopffeldes
                        'CSV_TYPE_CHOICES': {'TYPE_BANK_TRANSFER_IN': ['UEBERWEISUNGSGUTSCHRIFT',
                                                                    'SEPA-CT HABEN EINZELBUCHUNG',
                                                                    'GUTSCHR. NEUTR. UEBW. EZUE'],  # Überweisung
                                             'TYPE_DEBIT': ['SEPA-DD SAMMLER-HABEN CORE',
                                                            'SEPA-DD EINZELLB.HABEN CORE'],  # Lastschrift
                                             'TYPE_DEBIT_RETURN': ['RETOURENHUELLE LASTSCHRIFT',
                                                                   'SEPA-DD SOLL RUECKBEL. CORE'],  # Rücklastschrift
                                             },
                        'DEBIT_RETURN_FEE': 8.11,  # Kosten für Lastschriftrückgabe
                        'REMINDER_FEE': 3.00,  # Kosten für 1. Mahnung
                        'REMINDER_PAYMENT_PERIOD': 10,  # Zahlungsfrist für Mahnung
                        }

# Paper Invoice Module
MODULE_PAPER_INVOICE = False
PAPER_INVOICE_SETTINGS = {'SITE_TEXT': '<p>Bitte beachten Sie: <br />Für den Postversand von Rechnungen in Papierform berechnet die Demo GmbH Unkosten von 1,00 €.</p>',
                          'INVOICE_TEXT': 'Zusendung der Rechnung in Papierform',
                          'INVOICE_FEE': '1.00',
                          }