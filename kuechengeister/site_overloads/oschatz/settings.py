# -*- coding: utf-8 -*-

"""
 The site specific settings

 Overwrite the site wide settings
 Configure all the site specific settings that are not confidential
 The confidential settings are set in /settings_local.py
"""

# New Customer Configuration
EXIST_CUST_REGISTRATION_TYPE = 'CUSTOMERNUMBER'
EXIST_CUST_REGISTRATION = 'Kundennummer'
NEW_CUSTOMER_FORM_AGB_CHECKBOX = True

# Set number of weeks to show in menu view
MENU_WEEKS_CUSTOMER = 8
MENU_WEEKS_HOMEPAGE = 4
MENU_START_DATE = '01.01.2017'

# E-MAIL Configuration
DEFAULT_FROM_EMAIL = 'Lebenshilfe Oschatz <info@essenbestellung-lebenshilfe.de>'


# SEPA Configuration
SEPA = {'SEPA_NAME': 'Lebenshilfe e.V. RV Oschatz',
        'SEPA_EREF_PREFIX': 'LHRVO',
        'SEPA_CREDITOR_ID': 'DE61LHV00000293072',
        'SEPA_IBAN': 'DE08860555921090081215',
        'SEPA_BIC': 'WELADE8LXXX',
        'SEPA_DEBIT_FRST_DAYS': '15.',
        'SEPA_DEBIT_RCUR_DAYS': '15.',
        'SEPA_FORM_COMPANY_LONG': 'Lebenshilfe e.V. RV Oschatz, Ernst-Schneller-Straße 14, 04758 Oschatz',
        'SEPA_FORM_COMPANY_SHORT': 'Lebenshilfe e.V. RV Oschatz',
        }


# Site specific context
SITE_CONTEXT = {'SITE_HTML_TITLE'                      : 'Essenbestellsystem der Lebenshilfe Oschatz',
                'SITE_HTML_ADMIN_TITLE'                : 'Essenbestellsystem - Verwaltung',
                'SITE_NAME'                            : 'essenbestellung-lebenshilfe.de',
                'SITE_URL'                             : 'https://www.essenbestellung-lebenshilfe.de',
                'SITE_REGARDS'                         : 'Ihre Lebenshilfe Oschatz',
                'SITE_HEADER_SUBTITLE'                 : '',
                'SITE_HEADER_BACKGROUND'               : '6ca80e',
                'INVOICE_COMPANY_NAME'                 : 'Lebenshilfe Oschatz',
                'INVOICE_COMPANY_NAME_1'               : '',
                'INVOICE_COMPANY_NAME_2'               : '',
                'INVOICE_COMPANY_ADDRESS_STREET'       : 'Ernst-Schneller-Straße 14',
                'INVOICE_COMPANY_ADDRESS_POSTCODE_TOWN': '04758 Oschatz',
                'INVOICE_COMPANY_TELEFONE'             : '',
                'INVOICE_COMPANY_EMAIL'                : '',
                'INVOICE_COMPANY_TAXID'                : '',
                'INVOICE_COMPANY_ACCOUNT_NUMBER'       : '',
                'INVOICE_COMPANY_ACCOUNT_SORTCODE'     : '',
                'INVOICE_COMPANY_ACCOUNT_BANKNAME'     : 'Stadt- und Kreissparkasse Leipzig',
                'INVOICE_COMPANY_ACCOUNT_IBAN'         : 'DE08860555921090081215',
                'INVOICE_COMPANY_ACCOUNT_BIC'          : 'WELADE8LXXX',
                'INVOICE_PAYMENT_PERIOD_IN_DAYS'       : 13,
                'INVOICE_SHOW_TAX'                     : True,
                'PRINTMENUE_COLORSTRONG'               : '6CA80E',
                'PRINTMENUE_COLORLIGHT'                : 'E4E4E4',
                'MENU_SUBTEXT'                         : '',
                'MENU_FOOTER_LINE_1'                   : 'Änderungen und Irrtümer vorbehalten',
                'MENU_ADDITIVES'                       : '1 Farbstoff, 2 Konservierungsstoff, 3 Antioxidationsmittel, 4 Geschmacksverstärker, 5 geschwefelt, 6 geschwärzt, 7 mit Phosphat, 9 mit Süßungsmittel, 10 mit Süßungsmitteln, 11 mit einer Zuckerart und Süßungsmitteln, 12 enthält eine Phenylalainquelle, 13 kann bei übermäßigen Verzehr abführend wirken, 14 koffeinhaltig, 15 chininhaltig, 17 mit Milcheiweiß, 18 mit Eiklar, 19 unter Verwendung von Milch, 20 mit Pflanzeneiweiß<br />Allergenkennzeichnung: enthält a Glutenhaltiges Getreide*, b Sellerie*, c Erdnüsse*, d Sesamsamen*, e Eier*, f Krebstiere*, g Schalenfrüchte (Nüsse)*, h Soja*, i Milch* einschl. Laktose, j Fisch*, k Senf*, l Schwefeldioxid und Sulfite, m Lupine*, n Weichtiere* (* und Erzeugnisse daraus)',
                }

# Prepaid module settings
MODULE_PREPAID = False
PREPAID_SETTINGS_CREDIT = '0.00'  # the credit the customer is allowed to have in his account
PREPAID_SETTINGS = {'SEPA_DEBIT': False,
                    'BANK_TRANSFER': True,
                    'FLATRATE': True,
                    }

# Paper Invoice Module
MODULE_PAPER_INVOICE = False
PAPER_INVOICE_SETTINGS = {'SITE_TEXT': '<p>Bitte beachten Sie: <br />Für den Postversand von Rechnungen in Papierform berechnen wir Unkosten von 1,00 € pro Rechnung.',  # individual text on site views_kunden.rechnungen_papier as html
                          'INVOICE_TEXT': 'Rechnung in Papierform',  # individual text on invoice
                          'INVOICE_FEE': '1.00',  # the fee for the paper invoice as String
                          }

# Accounting Module Configuration
MODULE_ACCOUNTING = True
ACCOUNTING_SETTINGS = { 'CSV_DELIMITER': ';',  # the csv delimiter
                        'CSV_ACCOUNT_NUMBER': 'Kontonummer',  # Name des Konto Kontonummer Kopffeldes
                        'CSV_ACCOUNT_ICODE': 'BLZ',  # Name des Konto Bankleitzahl Kopffeldes
                        'CSV_CONTRAACCOUNT_NUMBER': 'Gegenkonto',  # Name des Gegenkonto Kontonummer Kopffeldes
                        'CSV_CONTRAACCOUNT_ICODE': 'Gegenkonto BLZ',  # Name des Gegenkonto Bankleitzahl Kopffeldes
                        'CSV_CONTRAACCOUNT_OWNER': 'Gegenkonto Inhaber',  # Name des Gegenkonto Inhaber Kopffeldes
                        'CSV_DATE': 'Datum',  # Name des Datum Kopffeldes
                        'CSV_DATE_FORMAT': '%d.%m.%Y',  # date format for parsing with strptime() function
                        'CSV_VALUTA': 'Valuta',  # Name des Valuta Kopffeldes
                        'CSV_VALUTA_FORMAT': '%d.%m.%Y',  # valuta date format for parsing with strptime() function
                        'CSV_SUBJECT_1': 'Verwendungszweck',  # Name des Verwendungszweck Kopffeldes
                        'CSV_SUBJECT_2': 'Verwendungszweck 2',  # Name des Verwendungszweck 2 Kopffeldes
                        'CSV_SUBJECT_MORE': 'Weitere Verwendungszwecke',  # Name des Weitere Verwendungszwecke Kopffeldes
                        'CSV_AMOUNT': 'Betrag',  # Name des Betrag Kopffeldes
                        'CSV_TYPE': 'Art',  # Name des Buchungstyp Kopffeldes
                        'CSV_TYPE_CHOICES': {'TYPE_BANK_TRANSFER_IN': ['UEBERWEISUNGSGUTSCHRIFT',
                                                                    'SEPA-CT HABEN EINZELBUCHUNG',
                                                                    'GUTSCHR. NEUTR. UEBW. EZUE'],  # Überweisung
                                             'TYPE_DEBIT': ['SEPA-DD SAMMLER-HABEN CORE',
                                                            'SEPA-DD EINZELLB.HABEN CORE'],  # Lastschrift
                                             'TYPE_DEBIT_RETURN': ['RETOURENHUELLE LASTSCHRIFT',
                                                                   'SEPA-DD SOLL RUECKBEL. CORE'], # Rücklastschrift
                                            },
                        'DEBIT_RETURN_FEE': 'ORIGINAL',  # Kosten für Lastschriftrückgabe
                        'REMINDER_FEE': '5.00',  # Kosten für 1. Mahnung
                        'REMINDER_FEE_2': '10.00',  # Kosten für 2. Mahnung
                        'REMINDER_FEE_3': '15.00',  # Kosten für 3. Mahnung
                        'REMINDER_PAYMENT_PERIOD': 10,  # Zahlungsfrist für 1. Mahnung
                        'REMINDER_PAYMENT_PERIOD_2': 5,  # Zahlungsfrist für 2. Mahnung
                        'REMINDER_PAYMENT_PERIOD_3': 5,  # Zahlungsfrist für 3. Mahnung
                        }