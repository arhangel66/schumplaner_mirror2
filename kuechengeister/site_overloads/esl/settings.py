# -*- coding: utf-8 -*-

"""
 The site specific settings

 Overwrite the site wide settings
 Configure all the site specific settings that are not confidential
 The confidential settings are set in /settings_local.py
"""

# New Customer Configuration
EXIST_CUST_REGISTRATION_TYPE = 'CUSTOMERNUMBER'
EXIST_CUST_REGISTRATION = 'Kundennummer'
NEW_CUSTOMER_FORM_AGB_CHECKBOX = False
NEW_CUSTOMER_REGISTRATION = False

# Customer model extra attributes configuration
CUST_EXTRA = {'ACTIVE': True, 'BLANK': True, 'VERBOSE_NAME': 'Debitor-Nr.', 'UNIQUE': False, 'HELP_TEXT': '', 'CHOICES': None}

# Set number of weeks to show in menu view
MENU_WEEKS_CUSTOMER = 8
MENU_WEEKS_HOMEPAGE = 4
MENU_START_DATE = '03.10.2016'

# E-MAIL Configuration
DEFAULT_FROM_EMAIL = 'ELBLAND Service- und Logistik GmbH <info@esl-essenbestellung.de>'


# SEPA Configuration
SEPA = {'SEPA_NAME': 'ELBLAND Service- und Logistik GmbH',
        'SEPA_EREF_PREFIX': 'ESL',
        'SEPA_CREDITOR_ID': 'DE30ZZZ00001703331',
        'SEPA_IBAN': 'DE16850550003033013642',
        'SEPA_BIC': 'SOLADES1MEI',
        'SEPA_DEBIT_FRST_DAYS': 6,
        'SEPA_DEBIT_RCUR_DAYS': 3,
        'SEPA_FORM_COMPANY_LONG': 'ELBLAND Service- und Logistik GmbH, Weinbergstraße 8, 01589 Riesa',
        'SEPA_FORM_COMPANY_SHORT': 'ELBLAND Service- und Logistik GmbH',
        }


# Site specific context
SITE_CONTEXT = {'SITE_HTML_TITLE'                      : 'ESL - Essenbestellung',
                'SITE_HTML_ADMIN_TITLE'                : 'ESL-Essenbestellung - Verwaltung',
                'SITE_NAME'                            : 'www.esl-essenbestellung.de',
                'SITE_URL'                             : 'https://www.esl-essenbestellung.de',
                'SITE_REGARDS'                         : 'Ihre ELBLAND Service- und Logistik GmbH',
                'SITE_HEADER_SUBTITLE'                 : '',
                'SITE_HEADER_BACKGROUND'               : '6ca80e',
                'INVOICE_COMPANY_NAME'                 : 'ELBLAND Service- und Logistik GmbH',
                'INVOICE_COMPANY_NAME_1'               : '',
                'INVOICE_COMPANY_NAME_2'               : '',
                'INVOICE_COMPANY_ADDRESS_STREET'       : 'Weinbergstraße 8',
                'INVOICE_COMPANY_ADDRESS_POSTCODE_TOWN': '01589 Riesa',
                'INVOICE_COMPANY_TELEFONE'             : '',
                'INVOICE_COMPANY_EMAIL'                : 'info@esl-essenbestellung.de',
                'INVOICE_COMPANY_TAXID'                : '',
                'INVOICE_COMPANY_ACCOUNT_NUMBER'       : '',
                'INVOICE_COMPANY_ACCOUNT_SORTCODE'     : '',
                'INVOICE_COMPANY_ACCOUNT_BANKNAME'     : '',
                'INVOICE_COMPANY_ACCOUNT_IBAN'         : '',
                'INVOICE_COMPANY_ACCOUNT_BIC'          : '',
                'INVOICE_PAYMENT_PERIOD_IN_DAYS'       : 10,
                'INVOICE_SHOW_TAX'                     : True,
                'PRINTMENUE_COLORSTRONG'               : '153d5e',
                'PRINTMENUE_COLORLIGHT'                : '7baf2c',
                'MENU_SUBTEXT'                         : '',
                'MENU_FOOTER_LINE_1'                   : 'Änderungen und Irrtümer vorbehalten',
                'MENU_ADDITIVES'                       : 'enthält 1 Glutenhaltiges Getreide*, 2 Ei*, 3 Milch (Laktose)*, 4 Soja*, 5 Fisch*, 6 Erdnuss*, 7 Sellerie*, 8 Senf*, 9 Krebstiere*, 10 Sesam*, 11 Schalenfrüchte (Nüsse)*, 12 Schwefeldioxid und Sulfite, 13 Lupine*, 14 Weichtiere* (* und Erzeugnisse daraus)'
                }

# Paper Invoice Module
MODULE_PAPER_INVOICE = False
PAPER_INVOICE_SETTINGS = {'SITE_TEXT': '<p>Bitte beachten Sie: <br />Für den Postversand von Rechnungen in Papierform berechnen wir Unkosten von 1,00 € pro Rechnung.',  # individual text on site views_kunden.rechnungen_papier as html
                          'INVOICE_TEXT': 'Rechnung in Papierform',  # individual text on invoice
                          'INVOICE_FEE': '1.00',  # the fee for the paper invoice as String
                          }

# Accounting Module Configuration
MODULE_ACCOUNTING = False

MODULE_INVOICE_BILLING_MONTHS = 4  # number of month to show in /admin/kg/rechnung/abrechnung/