# -*- coding: utf-8 -*-

"""
 Example confidential site specific settings for Live Server

 Overrides the site wide settings
 Sets all the confidential site specific settings
 The settings that are not confidential are in /site_overloads/OVERLOAD_SITE/settings.py
"""
import os

# Root of the project
PROJECT_ROOT = os.path.dirname(__file__)

# The name of the directory the site overloads are in
OVERLOAD_SITE = 'name_of_directory'


# Confidential E-Mail settings
EMAIL_HOST_USER     = 'nameuser'
EMAIL_HOST_PASSWORD = 'password'


# Confidential Database settings
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'database_name',
        'USER': 'username',
        'PASSWORD': 'password',
        'HOST': '',
        'PORT': '5432'
    }
}

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(name)s %(process)d %(thread)d %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'null': {
            'level': 'DEBUG',
            'class': 'django.utils.log.NullHandler',
        },
        'log_file': {
            'level': 'ERROR',
            'class': 'logging.FileHandler',
            'filename': os.path.join(PROJECT_ROOT, 'logs/django.log'),
            'formatter': 'verbose',
        },
        'sentry': {
            'level': 'WARNING',
            'class': 'raven.contrib.django.raven_compat.handlers.SentryHandler',
            'formatter': 'verbose',
            'tags': {'instance': OVERLOAD_SITE},
        },
    },
    'loggers': {
        'django': {
            'handlers': ['null'],
            'propagate': True,
            'level': 'INFO',
        },
        'django.request': {
            'handlers': ['log_file', 'sentry'],
            'level': 'WARNING',
            'propagate': False,
        },
        '': {
            'handlers': ['log_file', 'sentry'],
            'level': 'WARNING',
            'propagate': False,
        },
    }
}

RAVEN_CONFIG = {
    # Please uncomment "dsn" and fill the real Sentry DSN below for real use
    # 'dsn': 'http://...',
}
