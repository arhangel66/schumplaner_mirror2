# -*- coding: utf-8 -*-
"""
 Example confidential site specific settings for Live Server

 Overrides the site wide settings
 Sets all the confidential site specific settings
 The settings that are not confidential are in /site_overloads/OVERLOAD_SITE/settings.py
"""

from os import path

# The name of the directory the site overloads are in
OVERLOAD_SITE = 'demo'

# Confidential E-Mail settings
EMAIL_HOST_USER = 'nameuser'
EMAIL_HOST_PASSWORD = 'password'


# Confidential Database settings
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'smenu',
        'USER': 'smenu_user',
        'PASSWORD': 'smenu_password',
        'HOST': '127.0.0.1',
        'PORT': '5432'
    }
}

USER_RUNNING = 'vagrant'

BASE_DIR = '/home/%s/sites/develop' % USER_RUNNING

STATIC_ROOT = path.join(BASE_DIR, 'static')

# Customize INVOICE_ROOT for local development
INVOICE_ROOT = path.join(BASE_DIR, 'usercontent/invoices')
# Customize REMINDER_ROOT for local development
REMINDER_ROOT = path.join(BASE_DIR, 'usercontent/reminders')
