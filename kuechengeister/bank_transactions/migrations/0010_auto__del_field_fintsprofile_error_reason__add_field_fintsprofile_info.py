# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'FinTSProfile.error_reason'
        db.delete_column('bank_transactions_fintsprofile', 'error_reason')

        # Adding field 'FinTSProfile.information'
        db.add_column('bank_transactions_fintsprofile', 'information',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Adding field 'FinTSProfile.error_reason'
        db.add_column('bank_transactions_fintsprofile', 'error_reason',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Deleting field 'FinTSProfile.information'
        db.delete_column('bank_transactions_fintsprofile', 'information')


    models = {
        'bank_transactions.fintsprofile': {
            'Meta': {'object_name': 'FinTSProfile'},
            'account_no': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'fints_blz': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'fints_login': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'fints_url': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'information': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'last_update_at': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '80'}),
            'pin': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'status': ('django.db.models.fields.SmallIntegerField', [], {}),
            'transaction_count': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['bank_transactions']