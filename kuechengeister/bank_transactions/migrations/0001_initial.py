# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'FinTSProfileStatus'
        db.create_table('bank_transactions_fintsprofilestatus', (
            ('id', self.gf('django.db.models.fields.CharField')(max_length=30, primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal('bank_transactions', ['FinTSProfileStatus'])

        # Adding model 'FinTSProfile'
        db.create_table('bank_transactions_fintsprofile', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal('bank_transactions', ['FinTSProfile'])


    def backwards(self, orm):
        # Deleting model 'FinTSProfileStatus'
        db.delete_table('bank_transactions_fintsprofilestatus')

        # Deleting model 'FinTSProfile'
        db.delete_table('bank_transactions_fintsprofile')


    models = {
        'bank_transactions.fintsprofile': {
            'Meta': {'object_name': 'FinTSProfile'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'bank_transactions.fintsprofilestatus': {
            'Meta': {'object_name': 'FinTSProfileStatus'},
            'id': ('django.db.models.fields.CharField', [], {'max_length': '30', 'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['bank_transactions']