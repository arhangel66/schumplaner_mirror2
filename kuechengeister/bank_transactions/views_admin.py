# -*- coding: utf-8 -*-
from django.contrib.admin.views.decorators import staff_member_required
from django.shortcuts import redirect
from django.core.urlresolvers import reverse

from django.contrib import messages

from kg.models import BankAccountTransaction

from aq_wrapper import (
    AqWrapper,
    PinException,
    ProfileDataException
)

from models import FinTSProfile

@staff_member_required
def load_transactions(request):
    iID = request.GET.get('id')
    #sGoTo = reverse('admin:kg_bank_account_transaction')
    sGoTo = '/admin/kg/bankaccounttransaction/'
    if iID:
        oFinTSProfile = FinTSProfile.objects.get(pk=iID)
        if oFinTSProfile:
            oAqWrapper = AqWrapper(oFinTSProfile.fints_url, oFinTSProfile.id, oFinTSProfile.fints_blz, oFinTSProfile.fints_login, oFinTSProfile.account_no, request.POST.get('pin'))
            
            try:
                lTransactions = oAqWrapper.loadTransactions(oFinTSProfile.getLastUpdateDate())
                
                iDuplicates = 0
                iSaved = 0
                for oTransaction in lTransactions:
                    oFinTSProfile.updateLastUpdateDate(oTransaction.date)
                    oTr = BankAccountTransaction()
                    tStatus = oTr.saveTransaction(oTransaction.purpose, 
                                                  oTransaction.value, 
                                                  None, 
                                                  oTransaction.remoteName, 
                                                  oTransaction.remoteAccountNumber, 
                                                  oTransaction.remoteBankCode, 
                                                  oTransaction.transactionCode, 
                                                  oTransaction.date)
                                      
                    if tStatus[0] and tStatus[1] == None:
                        iSaved += 1
                    # Already imported set status as Duplicate
                    elif not tStatus[0] and tStatus[1] == 'DUP':
                        iDuplicates += 1
                
                sMessage = 'Es wurden %s Buchungen importiert.' % iSaved
                if iDuplicates > 0:
                    sMessage += ' %s Buchungen waren schon vorhanden.' % iDuplicates
                messages.add_message(request, messages.INFO, sMessage)
            except PinException:
                messages.add_message(request, messages.ERROR, 'Falsche PIN')
            except ProfileDataException:
                messages.add_message(request, messages.ERROR, 'Fehler beim Einrichten des Kontos')
    return redirect(sGoTo)
