# -*- coding: utf-8 -*-
from django import template

from bank_transactions.models import FinTSProfile

register = template.Library()  
  
@register.filter(name='fin_ts_profiles')
def fin_ts_profiles(value):
    return FinTSProfile.objects.all()