# -*- coding: utf-8 -*-
import os
import time
import shutil
import random
import StringIO
from subprocess import (
    Popen,
    PIPE
)
from shlex import split
from string import Template

from aqparser import HBCI_response

from settings import (
    AQHBCI_TOOL4_PATH, 
    AQBANKING_CLI,
    FINTS_ACCOUNTS_PATH
)


class PinException(Exception):
    pass


class ProfileDataException(Exception):
    pass


class AqWrapper(object):
    def __init__(self, psFinTSUrl, psName, psFinTSBLZ, psFinTSLogin, psAccountNo, psPin):
        # Import here because can't import while models file is not compiled
        from models import (
            TOOL_AQHBCI_TOOL4,
            TOOL_AQBANKING_CLI,
            AQWrapperLog
        )
        
        # Prepare log object and store tools for later use
        self.oLog = AQWrapperLog()
        self.sToolAqHbciTool4 = TOOL_AQHBCI_TOOL4
        self.sToolAqBankingCli = TOOL_AQBANKING_CLI
        
        self.sPin = psPin
        self.sName = psName
        self.sFinTSUrl = psFinTSUrl
        self.sFinTSBLZ = psFinTSBLZ
        self.sFinTSLogin = psFinTSLogin
        self.sAccountNo = psAccountNo
    
    @property
    def pinFile(self):
        """Build and Return binary FinTS API PIN object"""
        
        bPinFileBinary = b'PIN_%s_%s="%s"' % (self.sFinTSBLZ,
                                              self.sFinTSLogin,
                                              self.sPin)
        
        return bPinFileBinary
    
    @property
    def accountPath(self):
        """Build and Return FinTS Profile system account path"""
        
        sFullPath = os.path.join(FINTS_ACCOUNTS_PATH, str(self.sName))

        return sFullPath
    
    def simulateCreation(self):
        """Simulate profile creation when no real FinTSProfile object is created"""
        self.sName = 'sim_%d' % random.randrange(0, 10000)
        bStatus = self.createFinTSAccount()

        self.removeFinTSAccount()
        if bStatus:
            return True
        else:
            return False
    
    def createFinTSAccount(self):
        """Build FinTS account. Return True if account created or False if not"""

        tCommandTpl = Template('$aqhbci_tool4 -C $path -P /dev/stdin -A -n adduser -s $fints_url -b $fints_blz -u $fints_login -N $name -t pintan')
        sCommand = tCommandTpl.substitute(aqhbci_tool4=AQHBCI_TOOL4_PATH,
                                          path=self.accountPath,
                                          fints_url=self.sFinTSUrl,
                                          fints_blz=self.sFinTSBLZ,
                                          fints_login=self.sFinTSLogin,
                                          name=self.sName)
        oStdOutAqbanking = Popen(split(sCommand), stdout=PIPE, stdin=PIPE,stderr=PIPE).communicate(input=self.pinFile)

        tAddFlagCommandTpl = Template('$aqhbci_tool4 -C $path adduserflags -f forceSsl3 -u $fints_login')
        sAddFlagCommand = tAddFlagCommandTpl.substitute(aqhbci_tool4=AQHBCI_TOOL4_PATH, path=self.accountPath, fints_login=self.sFinTSLogin)
        oStdOutAqbanking = Popen(split(sAddFlagCommand), stdout=PIPE, stderr=PIPE).communicate()

        tGetSysIdCommandTpl = Template('$aqhbci_tool4 -C $path -P /dev/stdin -A -n getsysid')
        sGetSysIdCommand = tGetSysIdCommandTpl.substitute(aqhbci_tool4=AQHBCI_TOOL4_PATH, path=self.accountPath)
        oStdOutAqbanking = Popen(split(sGetSysIdCommand), stdout=PIPE, stdin=PIPE, stderr=PIPE).communicate(input=self.pinFile)
        
        # Log command and command output
        self.logOutput(self.sToolAqHbciTool4, sCommand, oStdOutAqbanking[1].decode())
        
        # Something went wrong 
        if 'Error getting system id (-1)' in oStdOutAqbanking[1].decode():
            # Wrong fints url, wrong fintsblz - Error getting system id (-1)
            return False
        else:
            # All ok
            return True
    
    def removeFinTSAccount(self):
        # If exist then remove existing account
        if os.path.isdir(self.accountPath):
            shutil.rmtree(self.accountPath, ignore_errors=True)
            
            return True
        else:
            return False
    
    def changeFinTSAccount(self):
        """Remove existing account and create new acount. Return True if all ok or False if error"""
        
        # If exist then remove existing account
        if self.removeFinTSAccount():
            # When account removed just create new with new data
            return self.createFinTSAccount()
        else:
            # If account don't exist return False
            return False
    
    def loadTransactions(self, pdFromDate):
        """Load and return list of bank transactions. Load transactions using from date. In case of error False is returned"""
        tCommandTpl = Template('$aqbanking_cli -D $path -P /dev/stdin -A -n request --transactions -a $account_no --fromdate=$fromdate')
        sCommand = tCommandTpl.substitute(aqbanking_cli=AQBANKING_CLI,
                                          path=self.accountPath,
                                          account_no=self.sAccountNo,
                                          fromdate=pdFromDate.strftime('%Y%m%d'))
        oStdOutAqbanking = Popen(split(sCommand), stdout=PIPE, stdin=PIPE, stderr=PIPE).communicate(input=self.pinFile)
        oTransactions = oStdOutAqbanking[0].decode()
        oVirtFile = StringIO.StringIO(oStdOutAqbanking[0])
        
        # Log command and command output
        self.logOutput(self.sToolAqBankingCli, sCommand, oStdOutAqbanking[1].decode())
        
        if 'Not a single job successfully executed' in oStdOutAqbanking[1].decode():
            raise PinException('Falsche PIN') # Probably wrog pin
        elif 'No requests created' in oStdOutAqbanking[1].decode():
            raise ProfileDataException('Fehler beim Einrichten des Kontos')  # Wrong account data
        
            # Wrong pin
            # 3:2016/03/21 13-30-55:aqhbci(17350):outbox.c: 1390: Error performing queue (-2)
            # 5:2016/03/21 13-30-55:aqbanking(17350):banking_online.c:  119: Error executing backend's queue
            # 4:2016/03/21 13-30-55:aqbanking(17350):banking_online.c:  137: Not a single job successfully executed
        
            # Wrong account or other data
            # 3:2016/03/21 13-33-01:aqbanking-cli(17370):request.c:  422: No requests created
        else:
            oHBCIResp = HBCI_response(oVirtFile)
            lTransactions = list()
            
            for oAccountInfo in oHBCIResp.accountInfoList.accountInfo:
                for oTransaction in oAccountInfo.transactionList:
                    lTransactions.append(oTransaction)
            
            return lTransactions
    
    def logOutput(self, psTool, psCommand, psOutput):
        # Log command and command output
        self.oLog.tool = psTool
        self.oLog.command = psCommand
        self.oLog.output = psOutput
        self.oLog.save()