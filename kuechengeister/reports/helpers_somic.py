# -*- coding: utf-8 -*-
from datetime import date, datetime
from decimal import Decimal
import xlsxwriter
from xlsxwriter.utility import xl_range, xl_rowcol_to_cell
import string
import StringIO

from django.db.models import Sum, Count, Avg
from django.template.loader import render_to_string

from reports.helpers import generate_pdf
from kg.models import Order, Child, Customer
from menus.models import Meal, Menu, Mealtime, Price, MealDefaultPrice
from settings import SITE_OVERLOAD_STATICFILES_DIR, MEALTIMES


def get_list_of_divisions():
    """ Returns list of SOMIC divisions """
    return ['Components', 'Somicon', 'Verpackung', 'Verwaltung']


def generate_somic_metzgerei_pdf_content(oDate=date.today()):
    """ Generate Content HTML for SOMIC Metzgerei order PDF """

    # total orders per brotzeit
    lSumList = []
    iTotalSum = 0
    for oMeal in Meal.objects.filter(mealtime__id=MEALTIMES['breakfast']):
        qsOrders = Order.objects.filter(menu__date=oDate, menu__meal=oMeal)
        dOrders = qsOrders.aggregate(sum=Sum('quantity'))
        if dOrders['sum'] is not None:
            lSumList.append({'name': qsOrders[0].menu.description,
                             'total': dOrders['sum']})
            iTotalSum += dOrders['sum']

    # orders per employee per unit
    # the unit is saved as string in the Customer.extra_attribute2 field
    lUnitsList = []
    lRowList = []
    iUnitTotal = 0
    iCounter = 0
    iRowCounter = 0
    lUnits = Child.objects.distinct('customer__extra_attribute2').values('customer__extra_attribute2').order_by('customer__extra_attribute2', 'name', 'surname')
    for dUnit in lUnits:
        sUnitName = dUnit['customer__extra_attribute2']
        qsChildren = Child.objects.filter(customer__extra_attribute2__iexact=sUnitName).order_by('name', 'surname')
        for oChild in qsChildren:
            iCounter += 1
            qsOrders = (Order.objects.select_related('menu')
                                     .filter(child=oChild,
                                             menu__date=oDate,
                                             menu__meal__mealtime__id=MEALTIMES['breakfast'])
                                     .order_by('menu__meal__sortKey'))
            if qsOrders:
                iRowCounter += 1
                lRowList.append({'child': oChild, 'orders': qsOrders, 'counter': iRowCounter})
                for oOrder in qsOrders:
                    iUnitTotal += oOrder.quantity

        if len(lRowList) > 0:
            lUnitsList.append({'unit_name': sUnitName,
                               'row_list': lRowList,
                               'unit_total': iUnitTotal})
        lRowList = []
        iUnitTotal = 0

    sContentHtml = render_to_string('reports/somic_metzgerei.html', {'sum_list': lSumList,
                                                                     'total_sum': iTotalSum,
                                                                     'unit_list': lUnitsList})

    return sContentHtml


def generate_somic_metzgerei_pdf(oDate=date.today()):
    """ Generate Content HTML for SOMIC Metzgerei order PDF """

    sContentHtml = generate_somic_metzgerei_pdf_content(oDate)
    oOutput = generate_pdf(
        'reports/pdf/somic_metzgerei_pdf.html',
        file_object=None,
        context={
            'list_content_html': sContentHtml,
            'list_date': oDate,
            'print_time': datetime.now(),
            'STATICFILES_DIR': SITE_OVERLOAD_STATICFILES_DIR
        }
    )
    oOutput.seek(0)
    return oOutput


def generate_somic_metzgerei_xlsx(oDateFrom=date.today(), oDateTo=date.today()):
    """ Generate Excel Sheet of SOMIC customers orders sum for Brotzeit """

    lDivisions = get_list_of_divisions()
    lColumnLetters = list(string.ascii_uppercase)

    # create workbook and cell formats
    oOutput = StringIO.StringIO()
    oWorkbook = xlsxwriter.Workbook(oOutput)
    oBoldFormat = oWorkbook.add_format({'bold': True})
    oMoneyFormat = oWorkbook.add_format({'num_format': u'0.00 €'})
    oBoldMoneyFormat = oWorkbook.add_format({'num_format': u'0.00 €', 'bold': True})
    oDivisionHeaderFormat = oWorkbook.add_format({'bold': True, 'valign': 'center'})

    # create and fill worksheet
    oWorksheet = oWorkbook.add_worksheet('Metzgerei')
    oWorksheet.write(0, 0, 'Metzgerei %s bis %s' % (oDateFrom.strftime('%d.%m.%Y'), oDateTo.strftime('%d.%m.%Y')), oBoldFormat)
    oWorksheet.write(1, 0, 'Stand: %s' % (datetime.now().strftime('%d.%m.%Y %H:%M:%S')))
    oWorksheet.set_column(0, 0, 28)  # set width for column 0
    oWorksheet.set_column(1, 20, 10)  # set width for column 1 to 20

    # write row 3: division header
    oWorksheet.write_row(3, 0, ['Brotzeit', 'Einzelpreis'] , oBoldFormat)
    iCol = 2
    for sDivisionName in lDivisions:
        oWorksheet.merge_range(3, iCol, 3, iCol + 1, sDivisionName, oDivisionHeaderFormat)
        iCol += 2
    oWorksheet.write_row(3, iCol, ['Gesamt', 'Gesamtpreis'], oBoldFormat)

    # start to write data
    qsMeals = Meal.objects.filter(mealtime__id=MEALTIMES['breakfast'])
    iDataStartRow = 4
    iRow = iDataStartRow
    for oMeal in qsMeals:
        iMealTotal = 0
        oWorksheet.write(iRow, 0, oMeal.name)
        # use average price under the assumption that price does not change within a month
        dPrice = (Price.objects.filter(menu__meal=oMeal,
                                       menu__date__gte=oDateFrom,
                                       menu__date__lte=oDateTo)
                  .aggregate(Avg('price')))
        oWorksheet.write(iRow, 1, dPrice['price__avg'], oMoneyFormat)
        iCol = 2
        for sDivision in lDivisions:
            # number of meal per division
            iMealsPerDivision = 0
            lMealCounter = (Order.objects.values('menu__meal')
                                         .filter(child__customer__extra_attribute2__istartswith=sDivision,
                                                 menu__meal=oMeal,
                                                 menu__date__gte=oDateFrom,
                                                 menu__date__lte=oDateTo)
                                         .annotate(ocount=Sum('quantity')))
            if lMealCounter:
                iMealsPerDivision = lMealCounter[0]['ocount']
            oWorksheet.write(iRow, iCol, iMealsPerDivision)
            iCol += 1
            # meal total per divsion
            oWorksheet.write_formula(iRow, iCol, '=%s*%s' % (xl_rowcol_to_cell(iRow, iCol-1), xl_rowcol_to_cell(iRow, 1)), oMoneyFormat)
            iMealTotal += iMealsPerDivision
            iCol += 1
        oWorksheet.write(iRow, iCol, iMealTotal)
        iCol += 1
        # add meal total
        oWorksheet.write_formula(iRow, iCol, '=%s*%s' % (xl_rowcol_to_cell(iRow, 1), xl_rowcol_to_cell(iRow, iCol-1)), oMoneyFormat)
        iRow += 1

    # add overall SUM fields
    oWorksheet.write(iRow, 0, 'Summen', oBoldFormat)
    iCol = 3
    for sDivisionName in lDivisions:
        oWorksheet.write_formula(iRow, iCol, '=SUM(%s)' % (xl_range(iDataStartRow, iCol, iRow - 1, iCol)), oBoldMoneyFormat)
        iCol += 2
    oWorksheet.write_formula(iRow, iCol, '=SUM(%s)' % (xl_range(iDataStartRow, iCol, iRow - 1, iCol)), oBoldMoneyFormat)

    oWorkbook.close()
    oOutput.seek(0)
    
    return oOutput


def generate_somic_divisions_xlsx(oDateFrom=date.today(), oDateTo=date.today(), sDivision=''):
    """ Generate Excel Sheet of SOMIC companies Brotzeit totals """

    # create workbook and cell formats
    oOutput = StringIO.StringIO()
    oWorkbook = xlsxwriter.Workbook(oOutput)
    oBoldFormat = oWorkbook.add_format({'bold': True})
    oNumberFormat = oWorkbook.add_format({'num_format': '0.00'})
    oBoldNumberFormat = oWorkbook.add_format({'num_format': '0.00', 'bold': True})
    
    # create and fill employees per division worksheets
    oWorksheet = oWorkbook.add_worksheet(sDivision)
    oWorksheet.set_column(0, 0, 10)  # set width for column 0
    oWorksheet.set_column(1, 2, 14)  # set width for column 1, 2
    oWorksheet.write_row(0, 0, ['Personal-Nr.', 'Nachname', 'Vorname', 'Summe'], oBoldFormat)
    iRow = 1
    qsChildren = Child.objects.filter(customer__extra_attribute2__istartswith=sDivision).order_by('customer__extra_attribute')
    for oChild in qsChildren:
        oTotal = Decimal('0.00')
        qsOrders = Order.objects.filter(child=oChild,
                                        menu__meal__mealtime__id=MEALTIMES['breakfast'],
                                        menu__date__gte=oDateFrom,
                                        menu__date__lte=oDateTo)
        for oOrder in qsOrders:
            oPrice = Price.objects.get(menu=oOrder.menu,
                                       price_group=oChild.facility_subunit.price_group)
            oTotal = oTotal + oOrder.quantity * oPrice.price
        if oTotal > 0:
            oWorksheet.write(iRow, 0, oChild.customer.extra_attribute)
            oWorksheet.write(iRow, 1, oChild.customer.name)
            oWorksheet.write(iRow, 2, oChild.customer.surname)
            oWorksheet.write_number(iRow, 3, oTotal, oNumberFormat)
            iRow += 1

    # add overall SUM field
    oWorksheet.write_formula(iRow, 3, '=SUM(%s)' % (xl_range(1, 3, iRow-1, 3)), oBoldNumberFormat)
    iRow += 2

    # add creation date
    oWorksheet.write(iRow, 0, 'Stand: %s' % (datetime.now().strftime('%d.%m.%Y %H:%M:%S')))


    oWorkbook.close()
    oOutput.seek(0)

    return oOutput
