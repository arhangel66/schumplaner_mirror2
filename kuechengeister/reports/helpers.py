# -*- coding: utf-8 -*-
from __future__ import with_statement

from django.db.models import Q, Sum
from django.template.loader import render_to_string
from django.utils.log import getLogger
from django.utils.translation import ugettext_lazy as _

from kg.models import Child, Order, Facility, FacilitySubunit, FacilityType
from menus.models import Meal, Mealtime, Menu, MealPlan, Allergen
from kg_utils.date_helper import DateHelper
from django.conf import settings

from weasyprint import HTML, CSS
from weasyprint.formatting_structure.boxes import TableBox
from settings import OVERLOAD_SITE, ORDER_EXCEED_WARNING

logger = getLogger(__name__)


def generate_pdf(template, file_object=None, response=None, context=None):
    """
    Generate PDF content from given template and context.
    - If the file_object is provided, generated content will be write to that
      file then return it.
    - If the response is provided, it will be returned with generated content
      inside.
    - Otherwise, the PDF as bytestring will be returned.
    """
    style = CSS(string='@page {font-family: Arial, Verdana, Helvetica, sans-serif;}')

    try:
        # write_obj() return None if target is an file object. Then we return
        # the file_object itself.
        pdf_doc = HTML(string=render_to_string(template, context)).render(
            stylesheets=[style])

        # Auto resize the page to adapt large content
        for page in pdf_doc.pages:
            for children in page._page_box.descendants():
                if isinstance(children, TableBox):
                    needed_width = children.width + \
                        page._page_box.margin_left + \
                        page._page_box.margin_right

                    # Override width only if the table doesn't fit
                    if page.width < needed_width:
                        page._page_box.width = needed_width
                        page.width = needed_width
        pdf_file = pdf_doc.write_pdf(target=file_object)
    except:
        logger.exception(
            _('Error when exporting PDF content using template {}.').format(
                template)
        )

    if file_object is not None:
        return file_object

    # Add pdf content into response object if provided
    if response is not None:
        response.content = pdf_file
        return response

    return pdf_file


def get_dayreport_html(request, facility, date, bShowEmptyRows=True,
                       lExcludeMealIds=[], subunit_page=False):

    # retrieve necessary objects
    params = request.GET
    subunits = FacilitySubunit.objects.filter(facility=facility)
    qsMeals = set()
    for mealplan in MealPlan.objects.filter(facilitysubunit__facility=facility):
        [qsMeals.add(meal) for meal in mealplan.meals()
         if meal.id not in lExcludeMealIds]
    lMeals = sorted(list(qsMeals), key=lambda obj: obj.sortKey)
    if not lMeals:  # check if meals are configured
        return ""
    qsOrders = Order.objects.filter(menu__date=date, child__facility=facility)
    year, week = date.isocalendar()[:2]

    # add breakfast from next day if FAX or Allergiker report
    iVirtualMealId = 99  # id for additional meal (breakfast next day)
    if params['dayreport'] in ['2', '3']:
        # add next day breakfast only for facilities that have breakfast
        if lMeals[0].mealtime.is_breakfast:
            lMeals.append(Meal(id=iVirtualMealId, name='Fruehstueck - Naechster Morgen', name_short='FM'))
            oOrdersfnextbusday = Order.objects.filter(menu__date=DateHelper.getNextBusinessday(date)).filter(menu__meal__name_short='F')
            # make dict out of fruehstueck orders and menus queryset for faster handling later on
            dOrdersfnextbusday = dict()
            for order in oOrdersfnextbusday:
                dOrdersfnextbusday[str(order.child.id) + '_' + str(iVirtualMealId)] = order

    # Store days having order of each child
    dOrderedDays = {}
    # make dict out of orders and menus queryset for faster handling later on
    dOrders = {}
    for order in qsOrders:
        dOrders['%d_%d' % (order.child.id, order.menu.meal.id)] = order.quantity
        if order.child.id in dOrderedDays:
            dOrderedDays[order.child.id].add(order.menu.date.day)
        else:
            dOrderedDays[order.child.id] = {order.menu.date.day}

    dTotal = {}
    for oMeal in lMeals:
        dTotal[oMeal.id] = 0

    # go through list of all children and create order lists
    lSubunits = []
    for subunit in subunits:
        lChildren = []
        dSubtotal = {}
        for oMeal in lMeals:
            dSubtotal[oMeal.id] = 0
        oChildren = Child.objects.select_related().filter(
            facility_subunit=subunit).order_by('name', 'surname')
            # id__in=[it[0] for it in children_ordered]).order_by('name', 'surname')
        for oChild in oChildren:
            dRow = {}
            lOrders = []
            iRowTotal = 0
            for oMeal in lMeals:
                # extra handling of FAX report -> add breakfast of next day
                if params['dayreport'] == '2' and oMeal.id == iVirtualMealId:
                    if '%d_%d' % (order.child.id, order.menu.meal.id) in dOrdersfnextbusday:
                        iQuantity = dOrdersfnextbusday['%d_%d' % (order.child.id, order.menu.meal.id)]
                        lOrders.append(iQuantity)
                        dTotal[oMeal.id] = dTotal[oMeal.id] + iQuantity
                        dSubtotal[oMeal.id] = dSubtotal[oMeal.id] + iQuantity
                    else:
                        lOrders.append(0)
                    continue    # NEXT element skip below

                # extra handling of Allergiker report -> add breakfast of next day
                if params['dayreport'] == '3' and oMeal.id == iVirtualMealId:
                    if '%d_%d' % (order.child.id, order.menu.meal.id) in dOrdersfnextbusday:
                        lOrders.append(oMeal.name_short)
                    continue    # NEXT element skip below
                if '%d_%d' % (oChild.id, oMeal.id) in dOrders:
                    iQuantity = dOrders['%d_%d' % (oChild.id, oMeal.id)]
                    if params['dayreport'] == '3':  # gebe zeiten mit fuer allergikerreport
                        lOrders.append(oMeal.name_short)
                    else:
                        lOrders.append(iQuantity)

                    iRowTotal += iQuantity
                    dTotal[oMeal.id] += iQuantity
                    dSubtotal[oMeal.id] += iQuantity
                else:
                    if params['dayreport'] == '3':  # keine leeren bestellungen fuer Allergiker
                        pass
                    else:
                        lOrders.append(0)
            if bShowEmptyRows or iRowTotal > 0:
                dRow = {
                    'child': oChild,
                    'orders': lOrders,
                    'suspended': oChild.customer.is_suspended
                }
                if ORDER_EXCEED_WARNING and oChild.id in dOrderedDays:
                    order_limit = 0
                    try:
                        order_limit = int(oChild.extra_attribute)
                    except ValueError:
                        pass
                    ordered_day_count = Order.count_ordered_weekdays(
                        oChild, year, week)
                    if ordered_day_count > order_limit:
                        dRow['ordered_day_count'] = ordered_day_count
                lChildren.append(dRow)

        lSubunits.append({'subunit': subunit, 'child_list': lChildren, 'subtotal' : dSubtotal})

    #set if facility is Kindergarten or not
    bIsKindergarten = facility.type_id == FacilityType.KINDERGARTEN

    #choose template for FAX / Normal pdf
    if params['dayreport'] == '2':
        sHtmltemplate = 'reports/pdf/utils_dayreport_fax.html'
    elif params['dayreport'] == '3':
        sHtmltemplate = 'reports/pdf/utils_dayreport_allergiker.html'
    else:
        sHtmltemplate = 'reports/pdf/utils_dayreport.html'

    return render_to_string(
        sHtmltemplate,
        {
            'subunit_list': lSubunits,
            'mealtimes': lMeals,
            'facility': facility,
            'date': date,
            'is_kindergarten': bIsKindergarten,
            'subunit_page': subunit_page,
            'total': dTotal
        }
    )


def get_detailed_weekreport_html(request, facility, week, year,
                                 lExcludeMealIds=None, ordered_only=False,
                                 **kwargs):

    def build_order_key(child_id, day_id, meal_id):
        return '{0}_{1}_{2}'.format(child_id, day_id, meal_id)

    lExcludeMealIds = lExcludeMealIds or []
    qsMeals = Meal.objects.filterAvailable(
        Q(periods__mealplan__facilitysubunit__facility=facility)
    ).exclude(id__in=lExcludeMealIds).filter(allow_order=True)

    days_in_week = DateHelper.getDaysInCalendarWeek(year, week)
    dates_list = list(Menu.objects.filter(
        date__in=days_in_week, meal__in=qsMeals).dates('date', 'day'))

    # retrieve necessary objects
    subunits = FacilitySubunit.objects.select_related().filter(
        facility=facility)
    lMeals = list(qsMeals)
    if not lMeals:  # check if meals are configured
        return ""
    meals_per_day = len(lMeals)

    orders = Order.objects.select_related().filter(menu__date__in=days_in_week)

    # make dict out of orders and menus queryset for faster handling later on
    orders_dict = dict()
    for order in orders:
        day_id = days_in_week.index(order.menu.date)
        order_key = build_order_key(
            order.child.id, day_id, order.menu.meal.id)
        orders_dict[order_key] = order

    facility_total = [0]*meals_per_day*len(dates_list)
    # go through list of all children and create order lists
    lSubunits = list()
    for subunit_id, subunit in enumerate(subunits):
        table_rows = []
        sub_total = [0]*meals_per_day*len(dates_list)

        oChildren = Child.objects.filter(
            facility_subunit=subunit).values_list(
                'id', 'name', 'surname', 'customer__is_suspended'
            ).order_by('name', 'surname')

        allergen_records = Allergen.objects.filter(
            child__in=[_[0] for _ in oChildren]
        ).values_list('child', 'name')
        children_allergen = {}
        for record in allergen_records:
            if record[0] in children_allergen:
                children_allergen[record[0]].append(record[1])
            else:
                children_allergen[record[0]] = [record[1]]

        for oChild in oChildren:
            row = dict()
            orders_list = list()
            for day_id, menu_date in enumerate(dates_list):
                for meal_id, oMeal in enumerate(lMeals):
                    order_key = build_order_key(oChild[0], day_id, oMeal.id)
                    if order_key in orders_dict:
                        orders_list.append(True)
                        col_id = day_id*meals_per_day + meal_id
                        sub_total[col_id] += 1
                    else:
                        orders_list.append(False)

                    if oChild[0] in children_allergen:
                        allergen = ', '.join(children_allergen[oChild[0]])
                    else:
                        allergen = ''
                    row = {
                        'name': u'{}, {}'.format(oChild[1], oChild[2]),
                        'allergen': allergen,
                        'orders': orders_list,
                        'suspended': oChild[3],
                    }
            if not ordered_only or True in orders_list:
                table_rows.append(row)

        # Only list non-empty subfacilities when ordered_only=True
        if not ordered_only or len(table_rows):
            lSubunits.append({
                'subunit': subunit,
                'child_list': table_rows,
                'subtotal': sub_total
            })

        # Aggregate to facility total
        for total_id, total_value in enumerate(sub_total):
            facility_total[total_id] += total_value

    return render_to_string(
        'reports/pdf/utils_weekreport_detailed.html',
        {
            'facility': facility,
            'subunit_list': lSubunits,
            'mealtimes': qsMeals,
            'dates_list': dates_list,
            'facility_total': facility_total
        }
    )


def get_weekreport_html(request, facility, week, year,
                        lExcludeMealIds=None, **kwargs):

    lExcludeMealIds = lExcludeMealIds or []
    days_in_week = DateHelper.getDaysInCalendarWeek(year, week)

    qsMeals = Meal.objects.filterAvailable(
        Q(periods__mealplan__facilitysubunit__facility=facility)
    ).filter(allow_order=True)
    if len(lExcludeMealIds) > 0:
        qsMeals = qsMeals.exclude(id__in=lExcludeMealIds)
    subunits = FacilitySubunit.objects.filter(facility=facility)

    # select days with meals
    dates_list = Menu.objects.filter(date__in=days_in_week, meal__in=qsMeals).dates('date', 'day')

    table_rows = list()
    for subunit in subunits:
        row = list()
        row.append(subunit.name)
        for day in dates_list:
            for oMeal in qsMeals:
                dOrderSum = Order.objects.filter(child__facility_subunit=subunit, menu__date=day,
                                                 menu__meal=oMeal).aggregate(sum=Sum('quantity'))
                iOrderSum = dOrderSum['sum'] or 0
                row.append(iOrderSum)
        table_rows.append(row)

    total = list()
    for day in dates_list:
        for oMeal in qsMeals:
            dOrderSum = Order.objects.filter(child__facility=facility, menu__date=day,
                                             menu__meal=oMeal).aggregate(sum=Sum('quantity'))
            iOrderSum = dOrderSum['sum'] or 0
            total.append(iOrderSum)

    return render_to_string(
        'reports/pdf/utils_weekreport.html',
        {
            'facility': facility,
            'mealtimes': qsMeals,
            'dates_list': dates_list,
            'table_rows': table_rows,
            'total': total
        }
    )


def get_weekreport_compact_html(request, year, week, lExcludeMealIds=[]):

    days_in_week = DateHelper.getDaysInCalendarWeek(year, week)

    qsFacilities = Facility.objects.filter(is_active=True)
    qsMeals = Meal.objects.filter(allow_order=True)
    if len(lExcludeMealIds) > 0:
        qsMeals = qsMeals.exclude(id__in=lExcludeMealIds)
    qsDatesList = Menu.objects.filter(date__in=days_in_week, meal__in=qsMeals).dates('date', 'day') # select days with meals

    table_rows = []
    for oFacility in qsFacilities:
        row = []
        row.append(oFacility.name)
        for oDay in qsDatesList:
            day_total = 0
            for oMeal in qsMeals:
                dOrderSum = Order.objects.filter(child__facility=oFacility,
                                                 menu__date=oDay,
                                                 menu__meal=oMeal).aggregate(sum=Sum('quantity'))
                iOrderSum = dOrderSum['sum'] or 0
                day_total += iOrderSum
                row.append(iOrderSum)
            row.append(day_total)

        # calculate row totals
        columns_per_date = len(qsMeals)+1
        for i in range(0, columns_per_date):
            temp_total = 0
            for j in range(1, len(qsDatesList)*columns_per_date, columns_per_date):
                temp_total += row[j+i]
            row.append(temp_total)

        table_rows.append(row)

    total = []
    for i in range(1, len(table_rows[0])):
        total.append(0)
        for row in table_rows:
            total[i-1] += row[i]

    return render_to_string(
        'reports/pdf/utils_weekreport_compact.html',
        {
            'meals': qsMeals,
            'dates_list': qsDatesList,
            'table_rows': table_rows,
            'total': total,
            'week': week
        }
    )


class WorksheetRow(object):
    """ Give more information for exporting a single row to worksheet.
    """
    values = []
    height = 1
    extras = {}
    __worksheet = None

    def __init__(self, worksheet, id=0, height=1, values=None, **kwargs):
        self.__worksheet = worksheet
        self.id = id
        self.height = height
        self.values = values or []
        self.extras = kwargs or {}

    def set_extras(self, items):
        self.extras.update(items)

    def flush(self):
        """ Flush current row into current sheet.
        """
        if not self.values or self.__worksheet is None:
            return
        for col, value in enumerate(self.values):
            self.__worksheet.write(self.id, col, value)
        if self.height > 1:
            self.__worksheet.set_row(self.id, 12*self.height)
