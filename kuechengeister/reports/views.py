# -*- coding: utf-8 -*-

from __future__ import with_statement
from datetime import date, datetime

from django.http import HttpResponse, Http404, HttpResponseBadRequest
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import permission_required
from django.contrib.messages import error
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from django.core.urlresolvers import reverse
from django.db.models import Sum

from kg_utils.date_helper import DateHelper
from kg.forms_admin import WeekSelectionForm, FibuBuchForm, FibuStammForm
from kg.models import Facility, FacilityType, FacilitySubunit, Child, PayType
from menus.models import Meal, Mealtime, Order

from reports.helpers_somic import generate_somic_metzgerei_pdf, generate_somic_metzgerei_xlsx, generate_somic_divisions_xlsx
from reports.helpers_hytera import generate_hytera_fibu_xlsx, generate_hytera_caterer_xlsx
from reports.helpers_diamant3 import generate_diamant3_export_buch, generate_diamant3_export_stamm
from reports.helpers_datev import generate_datev_export_buch, generate_datev_export_stamm
from settings import OVERLOAD_SITE, REPORT_WEEK_DETAILED, MEALTIMES
from reports.helpers import (
    generate_pdf, get_weekreport_html, get_weekreport_compact_html,
    get_detailed_weekreport_html, get_dayreport_html
)


@staff_member_required
@permission_required('kg.create_reports')
def somic(request):
    """ Returns certain lists for SOMIC """

    if OVERLOAD_SITE != 'somic':
        raise Http404

    oDateFrom = date.today()
    oDateTo = date.today()
    if 'date_from' in request.POST:
        oDateFrom = datetime.strptime(request.POST['date_from'], "%d.%m.%Y")
    if 'date_to' in request.POST:
        oDateTo = datetime.strptime(request.POST['date_to'], "%d.%m.%Y")

    # decide which report to generate per 'report' POST value
    if 'report' in request.POST and request.POST['report'] == 'metzgerei_bestelliste':
        sFilename = 'Metzgerei_Bestellliste_%s.pdf' % oDateFrom.strftime('%Y-%m-%d')
        oOutput = generate_somic_metzgerei_pdf(oDateFrom)
    elif 'report' in request.POST and request.POST['report'] == 'fibu_metzgerei':
        sFilename = 'Metzgerei_%s_bis_%s.xlsx' % (oDateFrom.strftime('%Y-%m-%d'), oDateTo.strftime('%Y-%m-%d'))
        oOutput = generate_somic_metzgerei_xlsx(oDateFrom, oDateTo)
    elif 'report' in request.POST and request.POST['report'] == 'fibu_firmen' and 'division' in request.POST:
        sFilename = '%s_%s_bis_%s.xlsx' % (request.POST['division'], oDateFrom.strftime('%Y-%m-%d'), oDateTo.strftime('%Y-%m-%d'))
        oOutput = generate_somic_divisions_xlsx(oDateFrom, oDateTo, request.POST['division'])
    else:
        raise Http404

    oResponse = HttpResponse(oOutput.read(), content_type="application/octet-stream")
    oResponse['Content-Disposition'] = 'attachment; filename=%s' % sFilename

    return oResponse


@staff_member_required
@permission_required('kg.create_reports')
def somic_fibu_metzgerei(request):
    """ Returns Excel Sheet of SOMIC customers with orders totals for Brotzeit """

    if OVERLOAD_SITE != 'somic':
        raise Http404

    oDateFrom = date.today()
    oDateTo = date.today()
    if 'date_from' in request.POST:
        oDateFrom = datetime.strptime(request.POST['date_from'], "%d.%m.%Y")
    if 'date_to' in request.POST:
        oDateTo = datetime.strptime(request.POST['date_to'], "%d.%m.%Y")

    oOutput = generate_somic_metzgerei_xlsx(oDateFrom, oDateTo)
    oResponse = HttpResponse(oOutput.read(), content_type="application/octet-stream")
    oResponse['Content-Disposition'] = 'attachment; filename=Metzgerei_%s_bis_%s.xlsx' % (oDateFrom.strftime('%Y-%m-%d'), oDateTo.strftime('%Y-%m-%d'))

    return oResponse


@staff_member_required
@permission_required('kg.create_reports')
def somic_fibu_firmen(request):
    """ Returns Excel Sheet of SOMIC companies with orders totals for Brotzeit for choosen time period """

    if OVERLOAD_SITE != 'somic':
        raise Http404

    oDateFrom = date.today()
    oDateTo = date.today()
    if 'date_from' in request.POST:
        oDateFrom = datetime.strptime(request.POST['date_from'], "%d.%m.%Y")
    if 'date_to' in request.POST:
        oDateTo = datetime.strptime(request.POST['date_to'], "%d.%m.%Y")

    oOutput = generate_somic_divisions_xlsx(oDateFrom, oDateTo, request.POST['division'])
    oResponse = HttpResponse(oOutput.read(), content_type="application/octet-stream")
    oResponse['Content-Disposition'] = 'attachment; filename=Metzgerei_%s_bis_%s.xlsx' % (oDateFrom.strftime('%Y-%m-%d'), oDateTo.strftime('%Y-%m-%d'))

    return oResponse


@staff_member_required
@permission_required('kg.create_reports')
def hytera(request):
    """ Returns Excel Sheet for Hytera with orders totals for choosen time period """

    if OVERLOAD_SITE != 'hytera':
        raise Http404

    oDateFrom = date.today()
    oDateTo = date.today()
    if 'date_from' in request.POST:
        oDateFrom = datetime.strptime(request.POST['date_from'], "%d.%m.%Y")
    if 'date_to' in request.POST:
        oDateTo = datetime.strptime(request.POST['date_to'], "%d.%m.%Y")

    # decide which report to generate per 'report' POST value
    if 'report' in request.POST and request.POST['report'] == 'fibu':
        sFilename = 'Abrechnung_Essen_Buchhaltung'
        oOutput = generate_hytera_fibu_xlsx(oDateFrom, oDateTo)
    elif 'report' in request.POST and request.POST['report'] == 'caterer':
        sFilename = 'Abrechnung_Essen_Caterer'
        oOutput = generate_hytera_caterer_xlsx(oDateFrom, oDateTo)
    else:
        raise Http404

    oResponse = HttpResponse(oOutput.read(), content_type="application/octet-stream")
    oResponse['Content-Disposition'] = 'attachment; filename=%s_%s_bis_%s.xlsx' % (sFilename, oDateFrom.strftime('%Y-%m-%d'), oDateTo.strftime('%Y-%m-%d'))

    return oResponse


@staff_member_required
@permission_required('kg.create_reports')
def index(request):
    # show the form for extended report selection if no report was requested per post parameter
    week_form = WeekSelectionForm()
    facilities = Facility.ex_objects.filter(is_active=True).order_by('name')
    return render_to_response(
        'reports/index.html',
        {
            'date': date.today(),
            'form_week': week_form,
            'facilities': facilities.values('id', 'name'),
            'OVERLOAD_SITE': OVERLOAD_SITE,
            'REPORT_WEEK_DETAILED': REPORT_WEEK_DETAILED
        },
        context_instance=RequestContext(request)
    )


@staff_member_required
@permission_required('kg.create_reports')
def week_report_compact(request):
    """Create compact week report based on submitted value. This seems targeting
    to specific instances only."""
    if request.method == 'POST':
        if 'year' in request.POST and 'week' in request.POST:
            week = request.POST['week']
            year = request.POST['year']
        else:
            year, week, day = date.today().isocalendar()

        report_html = get_weekreport_compact_html(request, year, week)

        resp = HttpResponse(content_type='application/pdf')
        resp['Content-Disposition'] = 'attachment; filename=Wochenliste_KW' + str(week) + '_' + str(year) + '.pdf'
        result = generate_pdf(
            'reports/pdf/report_landscape.html',
            response=resp,
            context={'report_html': report_html})
        return result
    return HttpResponseBadRequest()


@staff_member_required
@permission_required('kg.create_reports')
def week_report(request):
    """Create week report based on submitted value."""
    if request.method == 'GET':
        if 'year' in request.GET and 'week' in request.GET:
            week = request.GET['week']
            year = request.GET['year']
        else:
            weeks = DateHelper.getWeeks(date.today(), 2)
            week = weeks[1]['week']
            year = weeks[1]['year']

        qsFacilities = Facility.ex_objects.filter(is_active=True)
        if OVERLOAD_SITE == 'KUECHENGEISTER':
            qsFacilities = qsFacilities.filter(type__name='Kindergarten') # select only Kindergarten

        lExcludeMealIds = []
        if OVERLOAD_SITE in ['somic']:
            breakfast = Meal.objects.filter(mealtime__id=MEALTIMES['breakfast'])
            lExcludeMealIds = list(oMeal.id for oMeal in breakfast)

        # Identify which weekly report (details or subtotal)?
        if request.GET['weekreport'] == '2':
            report_method = get_detailed_weekreport_html
            qsFacilities = qsFacilities.filter(id=request.GET['facility'])
        else:
            report_method = get_weekreport_html

        report_params = {
            'request': request,
            'week': week,
            'year': year,
            'lExcludeMealIds': lExcludeMealIds
        }
        if 'ordered-only' in request.GET:
            report_params['ordered_only'] = request.GET['ordered-only']

        report_html = ""
        for facility in qsFacilities:
            report_html += report_method(facility=facility, **report_params)

        # DEBUG ONLY
        # return render_to_response(
        #     'reports/pdf/report_landscape.html',
        #     {'report_html': report_html}
        # )

        resp = HttpResponse(content_type='application/pdf')
        resp['Content-Disposition'] = 'attachment; filename=Wochenliste_KW' + str(week) + '_' + str(year) + '.pdf'
        result = generate_pdf(
            'reports/pdf/report_landscape.html',
            response=resp,
            context={'report_html': report_html})
        return result
    return HttpResponseBadRequest()


@staff_member_required
@permission_required('kg.create_reports')
@csrf_exempt
def day_report(request):
    """Create day report based on submitted value."""
    if request.method == 'GET':
        form_data = request.GET
        report_day = form_data.get('dayreport')
        try:
            report_date = datetime.strptime(form_data.get('date'), "%d.%m.%Y")
        except (TypeError, ValueError):
            report_date = date.today()

        facility_ids = form_data.getlist('fc')
        if not facility_ids:
            error(request, 'Please select at least one facility for report.')
            return redirect(reverse('reports:index'))
        qsFacilities = Facility.ex_objects.filter(is_active=True)
        if 'all' not in facility_ids:
            qsFacilities = qsFacilities.filter(id__in=facility_ids)

        report_html = ""

        # set the filename for the output pdf
        sFilename = 'Tagesliste'

        # set the filename for the Fax Tagesliste
        if report_day == '2':
            sFilename = 'Fax_Tagesliste'

        # set the filename for the Allergiker Tagesliste
        if report_day == '3':
            sFilename = 'Allergiker_Tagesliste'

        PDF_PAGE_BREAK = getattr(settings, 'PDF_PAGE_BREAK', '')

        for facility in qsFacilities:
            lExcludeMealIds = []
            if OVERLOAD_SITE in ['somic']:
                lExcludeMealIds = list(oMeal.id for oMeal in Meal.objects.filter(mealtime__id=MEALTIMES['breakfast']))
            temp_html = get_dayreport_html(
                request, facility, report_date,
                bShowEmptyRows=not form_data.get('ordered-only', False),
                lExcludeMealIds=lExcludeMealIds,
                subunit_page=form_data.get('subunit-page', False)
            )
            if temp_html:
                report_html += temp_html
                if form_data['dayreport'] == '2':
                    # only in kita! put in empty page for odd number of subunits
                    if facility.type_id == FacilityType.KINDERGARTEN and facility.facilitysubunit_set.count() & 1:
                        report_html += PDF_PAGE_BREAK
                else:
                    report_html += PDF_PAGE_BREAK
        # Clear all page breaks at the end
        while report_html[-len(PDF_PAGE_BREAK):] == PDF_PAGE_BREAK:
            report_html = report_html.strip(PDF_PAGE_BREAK).strip()

        # DEBUG ONLY
        # return render_to_response(
        #    'reports/pdf/report.html', {'report_html': report_html})

        resp = HttpResponse(content_type='application/pdf')
        resp['Content-Disposition'] = 'attachment; filename=%s_%s.pdf' % (sFilename, report_date.strftime('%Y_%m_%d'))
        result = generate_pdf(
            'reports/pdf/report.html',
            response=resp,
            context={'report_html': report_html})
        return result
    return HttpResponseBadRequest()


@staff_member_required
@permission_required('kg.create_reports')
def month_report_stats(request, *args, **kwargs):
    """ Makes Excel Sheet of monthly orders for Kobrow """
    import xlsxwriter
    import StringIO
    from xlsxwriter.utility import xl_rowcol_to_cell
    oDateFrom = date.today()
    oDateTo = date.today()
    if 'date_from' in request.POST:
        oDateFrom = datetime.strptime(request.POST['date_from'], "%d.%m.%Y")
    if 'date_to' in request.POST:
        oDateTo = datetime.strptime(request.POST['date_to'], "%d.%m.%Y")
    facility_id = request.POST['facility']

    # Get all available meals
    meals = set()
    for subunit in FacilitySubunit.objects.filter(facility_id=facility_id):
        meals.update(Meal.objects.getAvailableInRange(
            mealplan=subunit.mealplan,
            from_date=oDateFrom,
            to_date=oDateTo
            )
        )
    sorted_meals = sorted(meals, key=lambda meal: meal.sortKey)

    iMealStartCol = 2
    lExtraCols = []
    if OVERLOAD_SITE == 'sonnenblick':
        iMealStartCol = 3
        lExtraCols = ['Typ']

    # create workbook and cell formats
    oOutput = StringIO.StringIO()
    oWorkbook = xlsxwriter.Workbook(oOutput)
    oBoldFormat = oWorkbook.add_format({'bold': True})
    qsChildren = Child.objects.filter(facility_id=request.POST['facility'])
    oWorksheet = oWorkbook.add_worksheet('%s bis %s' % (
        oDateFrom.strftime('%d.%m.%Y'), oDateTo.strftime('%d.%m.%Y')))
    # Set different widths to columns
    oWorksheet.set_column(0, len(sorted_meals)+2, 15)
    oWorksheet.write_row(
        0, 0,
        ['Name', 'Vorname'] + lExtraCols + [meal.name for meal in sorted_meals],
        oBoldFormat)
    iRow = 1
    for oChild in qsChildren:
        oWorksheet.write(iRow, 0, oChild.name)
        oWorksheet.write(iRow, 1, oChild.surname)
        if OVERLOAD_SITE == 'sonnenblick':
            if oChild.customer.pay_type.id == PayType.DEBIT:
                sEmployeeType = 'Mitarbeiter'
            else:
                sEmployeeType = 'Praktikant'
            oWorksheet.write(iRow, 2, sEmployeeType)

        iCol = iMealStartCol
        for meal in sorted_meals:
            lMealCounter = (Order.objects.values('menu__meal').filter(
                child=oChild, menu__meal=meal, menu__date__gte=oDateFrom,
                menu__date__lte=oDateTo).annotate(meal_sum=Sum('quantity'))
            )
            iMealSum = 0
            if len(lMealCounter) > 0:
                iMealSum = lMealCounter[0]['meal_sum']
            oWorksheet.write_number(iRow, iCol, iMealSum)
            iCol += 1
        iRow += 1

    # write SUM fields
    iCol = iMealStartCol
    for oMealtime in sorted_meals:
        oWorksheet.write_formula(
            iRow, iCol,
            'SUM(%s:%s)' % (xl_rowcol_to_cell(1, iCol),
                            xl_rowcol_to_cell(iRow-1, iCol)),
            oBoldFormat)
        iCol += 1
    oWorkbook.close()
    oOutput.seek(0)

    response = HttpResponse(
        oOutput.read(), content_type="application/octet-stream")
    response['Content-Disposition'] = \
        'attachment; filename=Abrechnung_%s_bis_%s.xlsx' % (
            oDateFrom.strftime('%Y-%m-%d'), oDateTo.strftime('%Y-%m-%d'))
    return response


@staff_member_required
@permission_required('kg.create_reports')
def diamant3_export_buch(request):
    """ Returns invoices as CSV file in DIAMANT/3 format """

    if request.method == 'POST':
        oFibuForm = FibuBuchForm(request.POST)

        if oFibuForm.is_valid():
            iFacilityId = int(oFibuForm.cleaned_data['facility'])
            iYear = int(oFibuForm.cleaned_data['year'])
            iMonth = int(oFibuForm.cleaned_data['month'])

            oDateFrom = date(iYear, iMonth, 1)
            oDateTo = DateHelper.getLastDayOfMonth(oDateFrom)

        oFacility = Facility.objects.get(pk=iFacilityId)
        sFilename = 'ER2_Schulmenueplaner_FK_%d-%02d_%s.csv' % (iYear, iMonth, oFacility.name_short.replace(' ', '-').replace('"', ''). replace("'", ''))

        oOutput = generate_diamant3_export_buch(oDateFrom, oDateTo, iFacilityId)
        oResponse = HttpResponse(oOutput.read(), content_type="application/octet-stream")
        oResponse['Content-Disposition'] = 'attachment; filename="%s"' % sFilename

        return oResponse

    else:
        raise Http404


@staff_member_required
@permission_required('kg.create_reports')
def diamant3_export_stamm(request):
    """ Returns customer and bank account data as CSV file in DIAMANT/3 format """

    oFibuForm = FibuStammForm(request.POST)

    if oFibuForm.is_valid():
        iFacilityId = int(oFibuForm.cleaned_data['facility'])

        oFacility = Facility.objects.get(pk=iFacilityId)

        oToday = date.today()
        sFilename = 'ER2_Schulmenueplaner_SPM_%d-%02d-%02d_%s.csv' % (oToday.year, oToday.month, oToday.day, oFacility.name_short.replace(' ', '-').replace('"', ''). replace("'", ''))

        oOutput = generate_diamant3_export_stamm(iFacilityId)
        oResponse = HttpResponse(oOutput.read(), content_type="application/octet-stream")
        oResponse['Content-Disposition'] = 'attachment; filename="%s"' % sFilename

        return oResponse

    else:
        raise Http404
    
    
@staff_member_required
@permission_required('kg.create_reports')
def datev_export_buch(request):
    """ Returns invoices as CSV file in DATEV format """

    if request.method == 'POST':
        oFibuForm = FibuBuchForm(request.POST)

        if oFibuForm.is_valid():
            iFacilityId = int(oFibuForm.cleaned_data['facility'])
            iYear = int(oFibuForm.cleaned_data['year'])
            iMonth = int(oFibuForm.cleaned_data['month'])

            oDateFrom = date(iYear, iMonth, 1)
            oDateTo = DateHelper.getLastDayOfMonth(oDateFrom)

        oFacility = Facility.objects.get(pk=iFacilityId)
        sFilename = 'EXTF_Schulmenueplaner_Buchungsstapel_%d-%02d_%s.csv' % (iYear, iMonth, oFacility.name_short.replace(' ', '-').replace('"', ''). replace("'", ''))

        oOutput = generate_datev_export_buch(oDateFrom, oDateTo, iFacilityId)
        oResponse = HttpResponse(oOutput.read(), content_type="application/octet-stream")
        oResponse['Content-Disposition'] = 'attachment; filename="%s"' % sFilename

        return oResponse

    else:
        raise Http404


@staff_member_required
@permission_required('kg.create_reports')
def datev_export_stamm(request):
    """ Returns customer and bank account data as CSV file in DATEVformat """

    oFibuForm = FibuStammForm(request.POST)

    if oFibuForm.is_valid():
        iFacilityId = int(oFibuForm.cleaned_data['facility'])

        oFacility = Facility.objects.get(pk=iFacilityId)

        oToday = date.today()
        sFilename = 'EXTF_Schulmenueplaner_Debitoren_%d-%02d-%02d_%s.csv' % (oToday.year, oToday.month, oToday.day, oFacility.name_short.replace(' ', '-').replace('"', ''). replace("'", ''))

        oOutput = generate_datev_export_stamm(iFacilityId)
        oResponse = HttpResponse(oOutput.read(), content_type="application/octet-stream")
        oResponse['Content-Disposition'] = 'attachment; filename="%s"' % sFilename

        return oResponse

    else:
        raise Http404