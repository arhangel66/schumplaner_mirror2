# -*- coding: utf-8 -*-
from datetime import date, datetime
from decimal import Decimal
import xlsxwriter
import StringIO

from django.db.models import Sum, Count
from django.template.loader import render_to_string

from kg.models import Order, Child, Customer
from menus.models import Meal, Menu, Mealtime, Price, PriceGroup


def generate_hytera_fibu_xlsx(oDateFrom=date.today(), oDateTo=date.today(), sDivision=''):
    """ Generate Excel Sheet with employee meal totals for Hytera """

    # create workbook and cell formats
    oOutput = StringIO.StringIO()
    oWorkbook = xlsxwriter.Workbook(oOutput)
    oNumberFormat = oWorkbook.add_format({'num_format': '0.00'})
    oBoldFormat = oWorkbook.add_format({'bold': True})
    oBoldNumberFormat = oWorkbook.add_format({'bold': True, 'num_format': '0.00'})

    oWorksheet = oWorkbook.add_worksheet('%s bis %s' % (oDateFrom.strftime('%d.%m.%Y'), oDateTo.strftime('%d.%m.%Y')))
    oWorksheet.write_row(0, 0, ['Personal-Nr.', 'Nachname', 'Vorname', 'Menge', 'Brutto', 'Zuschuss', 'Betrag'], oBoldFormat)
    iRow = 1
    qsChildren = Child.objects.select_related('customer').order_by('customer__name', 'customer__surname')
    for oChild in qsChildren:
        oTotalAmount = Decimal('0.00')
        iTotalQuantity = 0
        iRowForFormula = iRow + 1
        qsOrders = Order.objects.filter(child=oChild,
                                        menu__date__gte=oDateFrom,
                                        menu__date__lte=oDateTo)
        for oOrder in qsOrders:
            oPrice = Price.objects.get(menu=oOrder.menu,
                                       price_group=oChild.facility_subunit.price_group)
            iTotalQuantity += oOrder.quantity
            oTotalAmount = oTotalAmount + oOrder.quantity * oPrice.price
        if iTotalQuantity > 0:
            oWorksheet.write(iRow, 0, oChild.customer.extra_attribute)
            oWorksheet.write(iRow, 1, oChild.customer.name)
            oWorksheet.write(iRow, 2, oChild.customer.surname)
            oWorksheet.write(iRow, 3, iTotalQuantity)
            oWorksheet.write_number(iRow, 4, oTotalAmount, oNumberFormat)
            if oChild.customer.extra_attribute2 == 'Intern':
                oWorksheet.write_formula(iRow, 5, '=0.2*E%d' % iRowForFormula, oNumberFormat)  # 20% discount for in-house employees
            else:
                oWorksheet.write(iRow, 5, 0, oNumberFormat)  # no discount for the others
            oWorksheet.write_formula(iRow, 6, '=E%d-F%d' % (iRowForFormula, iRowForFormula), oNumberFormat)
            iRow += 1

    # add SUM row
    oWorksheet.write(iRow, 0, 'Summen', oBoldFormat)
    oWorksheet.write_blank(iRow, 1, None, oBoldFormat)
    oWorksheet.write_blank(iRow, 2, None, oBoldFormat)
    oWorksheet.write_formula(iRow, 3, '=SUM(D2:D%d)' % iRowForFormula, oBoldNumberFormat)
    oWorksheet.write_formula(iRow, 4, '=SUM(E2:E%d)' % iRowForFormula, oBoldNumberFormat)
    oWorksheet.write_formula(iRow, 5, '=SUM(F2:F%d)' % iRowForFormula, oBoldNumberFormat)
    oWorksheet.write_formula(iRow, 6, '=SUM(G2:G%d)' % iRowForFormula, oBoldNumberFormat)

    oWorkbook.close()
    oOutput.seek(0)

    return oOutput


def generate_hytera_caterer_xlsx(oDateFrom=date.today(), oDateTo=date.today(), sDivision=''):
    """ Generate Excel Sheet with meal totals for Hytera Caterer """

    # create workbook and cell formats
    oOutput = StringIO.StringIO()
    oWorkbook = xlsxwriter.Workbook(oOutput)
    oNumberFormat = oWorkbook.add_format({'num_format': '0.00'})
    oBoldFormat = oWorkbook.add_format({'bold': True})
    oBoldNumberFormat = oWorkbook.add_format({'bold': True, 'num_format': '0.00'})
    oDateFormat = oWorkbook.add_format({'num_format': 'dd.mm.yyyy'})

    oWorksheet = oWorkbook.add_worksheet('%s bis %s' % (oDateFrom.strftime('%d.%m.%Y'), oDateTo.strftime('%d.%m.%Y')))
    oWorksheet.set_column(0, 1, 11)  # set width to 11 for first two columns
    oWorksheet.write_row(0, 0, ['Datum', u'Menü', 'Menge', 'Einzelpreis', 'Summe'], oBoldFormat)

    qsPriceGroups = PriceGroup.objects.all()
    oPriceGroup = qsPriceGroups[0]  # there's only one pricegroup for Hytera
    qsMenus = (Menu.objects.select_related('meal').filter(date__gte=oDateFrom,
                                                          date__lte=oDateTo)
                                                  .order_by('date', 'meal__sortKey'))
    iRow = 1
    for oMenu in qsMenus:
        iRowForFormula = iRow + 1

        oPrice = Price.objects.get(menu=oMenu, price_group=oPriceGroup)
        dOrders = Order.objects.filter(menu=oMenu).aggregate(sum=Sum('quantity'))

        oWorksheet.write_datetime(iRow, 0, oMenu.date, oDateFormat)
        oWorksheet.write(iRow, 1, oMenu.meal.name)
        oWorksheet.write_number(iRow, 2, dOrders['sum'] or 0)
        oWorksheet.write_number(iRow, 3, oPrice.price, oNumberFormat)
        oWorksheet.write_formula(iRow, 4, '=C%d*D%d' % (iRowForFormula, iRowForFormula), oNumberFormat)
        iRow += 1

    # add SUM row
    oWorksheet.write(iRow, 0, 'Summen', oBoldFormat)
    oWorksheet.write_blank(iRow, 1, None, oBoldFormat)
    oWorksheet.write_formula(iRow, 2, '=SUM(C2:C%d)' % iRowForFormula, oBoldFormat)
    #oWorksheet.write_formula(iRow, 3, '=SUM(D2:D%d)' % iRowForFormula, oBoldFormat)
    oWorksheet.write_formula(iRow, 4, '=SUM(E2:E%d)' % iRowForFormula, oBoldNumberFormat)

    oWorkbook.close()
    oOutput.seek(0)

    return oOutput