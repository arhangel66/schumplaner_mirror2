# -*- coding: utf-8 -*-
from datetime import date, datetime
from decimal import Decimal
import StringIO


from kg_utils import ucsv as csv
from kg.models import Facility, InvoiceLog, Customer, Child, PayType
from settings import OVERLOAD_SITE


def generate_diamant3_export_buch(oDateFrom, oDateTo, iFacilityId):
    """ Generate CSV with invoices data in DIAMANT/3 ER2 format V3.8 """

    oOutput = StringIO.StringIO()
    oCsvWriter = csv.writer(oOutput, delimiter=';', encoding='iso-8859-1')

    oFacility = Facility.objects.get(pk=iFacilityId)

    # TODO: set Steuerschlüssel according to the tax (save tax with invoice)

    # default
    sFirmennummer = ''
    sSachkonto = ''
    sKostenstelle = ''
    sKostentraeger = ''
    sSteuerschluessel = ''
    sKontonummerSource = 'oInvoice.child.customer.extra_attribute'

    # Augustinuswerk
    if OVERLOAD_SITE == 'awessen':
        sFirmennummer = '010'
        sSachkonto = '4300'
        if iFacilityId == 6:
            sKostenstelle = '121000'  # Rosa-Luxemburg-Schule
        else:
            sKostenstelle = '126000'  # von Thom bekocht
        sKostentraeger = '92003'
        sSteuerschluessel = '8'  # 7%

    # Rudolstadt
    if OVERLOAD_SITE == 'rudolstadt':
        sFirmennummer = '100'
        sSachkonto = '48420'
        sKostenstelle = '29118'
        sKostentraeger = ''
        sSteuerschluessel = '10'  # 0%


    qsInvoices = InvoiceLog.objects.filter(invoice_date__gte=oDateFrom, invoice_date__lte=oDateTo)
    if iFacilityId > 0:
        qsInvoices = qsInvoices.filter(facility_id=iFacilityId)

    for oInvoice in qsInvoices:
        sBuchungstext = (u'%s, %s %02d/%d' % ( oInvoice.child.name,
                                               oInvoice.child.surname,
                                               oInvoice.invoice_date.month,
                                               oInvoice.invoice_date.year))

        lRow = [u'F',  # 1. Satzart(*) - "F" für FiBu-Satz
                u'0', # 2. internVKZ(*) - Dieses Feld ist mit einer Null zu füllen (wird intern benutzt, um bereits abgearbeitete Sätze zu markieren).
                u'%s' % sFirmennummer,  # 3. Firmennummer(*)
                u'',  # 4. Filialenkennzeichen
                u'',  # 5. Geschäftskennzeichen
                u'AR',  # 6. Belegart(*) - AR = Ausgangsrechnung
                u'%s' % oInvoice.invoice_date.strftime('%d%m%Y'),  # 7. Belegdatum(*) - TTMMJJJJ
                u'',  # 8. Buchungsperiode
                u'%s' % oInvoice.invoice_number,  # 9. Belegnummer (intern)
                u'',  # 10. Belegnummer
                u'%s' % eval(sKontonummerSource),  # 11. Kontonummer(*)
                u'%s' % sSachkonto,  # 12. Gegenkontonummer(*)
                u'%s' % sSteuerschluessel,  # 13. Steuerschlüssel(**) - individuell pro Klient; nur zu füllen, wenn Gegenkontonummer nicht leer
                u'',  # 14. Steuersatz(**)
                u'%s' % oInvoice.invoice_total,  # 15. Buchungsbetrag(*)
                u'%s' % (sBuchungstext.replace(u'"', u'""'))[:30],  # 16. Buchungstext
                u'',  # 17. freies Feld
                u'',  # 18. freies Feld
                u'EUR',  # 19. Währung(*)
                u'',  # 20. Länderkennzeichen (leer bedeutet Inland)
                u'',  # 21. Steueridentnummer
                u'',  # 22. Warenbewegung / Werkleistung
                u'',  # 23. Fälligkeit
        ]
        # add empty list entries up to 79.
        lRow += [u''] * 56
        oCsvWriter.writerow(lRow)

        # create K-Satz (Kostenstellen- und Kostenträgersatz) for every F-Satz
        lKSatz = [u'K',  # 1. Satzart(*) - "K" für Kostenstellen- oder Kostenträgersatz
                  u'0',  # 2. internVKZ(*) - Dieses Feld ist mit einer Null zu füllen (wird intern benutzt, um bereits abgearbeitete Sätze zu markieren).
                  u'%s' % sKostenstelle,  # 3. Kostenstelle
                  u'%s' % sKostentraeger,  # 4. Kostenträger
                  u'',  # 5. Kostencharakter
                  u'',  # 6. variabler Kostenanteil
                  u'',  # 7. Mengen
                  u'',  # 8. Aufteilung (Monate)
                  u'%s' % oInvoice.invoice_total,  # 9. Betrag(*)
                  u'',  # 10. freies Feld
                  u'',  # 11. Kostenart
                  u'',  # 12. Projekt
                  u'',  # 13. reserviert
                  u'',  # 14. Geschäftseinheit
                  u'',  # 15. Geschäftsfeld
                  u'',  # 16. reserviert
                  u'',  # 17. reserviert
        ]
        oCsvWriter.writerow(lKSatz)

    oOutput.seek(0)
    return oOutput


def generate_diamant3_export_stamm(iFacilityId):
    """ Generate CSV with customer/debtor data in DIAMANT/3 ER2 format V3.8 """

    oOutput = StringIO.StringIO()
    oCsvWriter = csv.writer(oOutput, delimiter=';', encoding='iso-8859-1')

    oFacility = Facility.objects.get(pk=iFacilityId)

    # default
    sExportType = 'CUSTOMER'  # the type of export: 'CUSTOMER' or 'CHILD'

    if sExportType == 'CHILD':
        qsExportItems = Child.objects.distinct().filter(facility__id=iFacilityId).order_by('cust_nbr')
    else:
        qsExportItems = Customer.objects.distinct().filter(child__facility__id=iFacilityId).order_by('extra_attribute')

    for oItem in qsExportItems:
        if sExportType == 'CHILD':
            oCustomer = Customer.objects.get(child=oItem)
            sKontonummer = oItem.cust_nbr
            sAnrede = ''
        else:
            oCustomer = oItem
            sKontonummer = oCustomer.extra_attribute
            sAnrede = oCustomer.title[:10]

        sZahlungsart = ''
        if oCustomer.pay_type.id == PayType.DEBIT:
            sZahlungsart = 'E'  # E = Einzugsermächtigung

        lSSatz = [u'S',  # 1. Satzart(*) - "S" für Stammdatensatz
                    u'0',  # 2. internVKZ(*) - Dieses Feld ist mit einer Null zu füllen (wird intern benutzt, um bereits abgearbeitete Sätze zu markieren).
                    u'D',  # 3. Kontoklasse(*) - enthält ein "D" für Debitorenkonten und ein "K" für Kreditorenkonten
                    u'%s' % sKontonummer,   # 4. Kontonummer(**) - Hier Debitorennummer (Kunde) oder Kundennummer (Kind)
                    u'',  # 5. Adressnummer(**)
                    u'%s, %s' % (oCustomer.name, oCustomer.surname),  # 6. Bezeichnung - Bezeichnung des Kontos
                    u'' ,  # 7. Bezeichnung2 - 2. Bezeichnung des Kontos
                    u'',  # 8. USTIdentnummer
                    u'',  # 9. Sammelkonto
                    u'',  # 10. ZugKredDeb - zugehörige Debitoren-, Kreditorenkontonummer für diesen Debitor/Kreditor
                    u'',  # 11. Bankleitzahl
                    u'',  # 12. Bankkontonummer
                    u'',  # 13. DiversesKonto
                    u'',  # 14. Mahnart
                    u'',  # 15. Mahnsprachkz
                    u'',  # 16. Zahlungskondition
                    u'%s' % sZahlungsart,  # 17. Zahlungsart - E = Einzugsermächtigung; sonst leer lassen
                    u'',  # 18. Zahlkonto
                    u'',  # 19. Zahlungsavis
                    u'',  # 20. Zahlungsauschluss
                    u'',  # 21. Karenztag
                    u'',  # 22. Kd.Nr. bei Kreditor
                    u'',  # 23. Freies Feld
                    u'',  # 24. Branchenschlüssel
                    u'',  # 25. Branche
                    u'%s' % oItem.surname[:20].strip(),  # 26. Vorname
                    u'%s' % oItem.name[:40].strip(),  # 27. Name1
                    u'',  # 28. Name2
                    u'',  # 29. Name3
                    u'%s' % oCustomer.street[:40],  # 30. Straße
                    u'%s' % oCustomer.zip_code[:40],  # 31. Postleitzahl
                    u'%s' % oCustomer.city[:40],  # 32. Ort
                    u'',  # 33. Postfach
                    u'',  # 34. PLZ für Postfach
                    u'',  # 35. Ort für Postfach
                    u'%s' % oCustomer.phone[:25],  # 36. Telefon
                    u'',  # 37. Telefax
                    u'',  # 38. Selektion
                    u'',  # 39. Hinweis
                    u'',  # 40. Gespr. Anrede
                    u'',  # 41. Gespr. Titel
                    u'',  # 42. Gespr. Vorname
                    u'',  # 43. Gespr. Name
                    u'',  # 44. Gespr. Funktion
                    u'',  # 45. Gespr. Durchwahl
                    u'',  # 46. Verband
                    u'',  # 47. Verbandskonto
                    u'',  # 48. Mahnauschluss
                    u'%s' % oCustomer.email[:50],  # 49. EMail
                    u'',  # 50. Ort2
                    u'',  # 51. Land
                    u'',  # 52. Bonität/Rating
                    u'',  # 53. Kreditlimit
                    u'',  # 54. Steuernummer
                    u'',  # 55. Kontoinhaber
                    u'',  # 56. LandFuerZ5A
                    u'',  # 57. BuchenGesperrt
                    u'',  # 58. Verbundenes Unternehmen
                    u'',  # 59. Adressart
                    u'',  # 60. Homepage
                    u'',  # 61. Rechtsform
                    u'',  # 62. Berechtigung
                    u'',  # 63. Konzernobjekt
                    u'',  # 64. Individuelle Abweichungen erlaubt
                    u'',  # 65. Ausschluss ZentralClearing
                    u'',  # 66. Gültig von
                    u'',  # 67. Gültig bis
                    u'',  # 68. Kontokorrent-Auszifferung
                    u'',  # 69. OPs einzeln zahlen/einziehen
                    u'',  # 70. Kontokorrent-Referenzkonto
                    u'',  # 71. BIC
                    u'',  # 72. IBAN
                    u'',  # 73. Geschäftseinheit
                    u'',  # 74. Geschäftsfeld
                    u'',  # 75. Buchen mit Geschäftseinheit
                    u'',  # 76. Adressnummer
                    u'',  # 77. Abweichende Mahnadresse
                    u'',  # 78. Inkassoanschrift
                    u'',  # 79. Lastschriftenavis
                    u'',  # 80. Mahnsperrgrund
                    u'',  # 81. Zahlsperrgrund
                    u'',  # 82. Norm zum Länderkennzeichen
                    u'',  # 83. Vorschlag Währung
                ]
        oCsvWriter.writerow(lSSatz)

        # Create a "PM-Satz" for every SEPA mandate of the customer
        if sExportType == 'CUSTOMER':
            for oMandate in oCustomer.sepamandate_set.all():
                lPMSatz = [u'PM',  # 1. Satzart(*) - "PM" für Mandate eines Personenkontos
                           u'0',  # 2. internVKZ(*) - Dieses Feld ist mit einer Null zu füllen (wird intern benutzt, um bereits abgearbeitete Sätze zu markieren).
                           u'%s' % oCustomer.extra_attribute,  # 3. Kontonummer(*) - Kontonummer des Debitors
                           u'%s' % oMandate.name[:40].strip(),  # 4. Mandatsbezeichnung(*) - Bezeichnung des Mandates
                           u'%s' % oMandate.mandate_id,  # 5. Mandatsnummer(*)
                           u'%s' % oMandate.bic,  # 6. BIC(*)
                           u'%s' % oMandate.iban,  # 7. IBAN Nummer(*)
                           u'%s' % oMandate.created_at.strftime('%d%m%Y'),  # 8. Unterschrift am
                           u'%s' % 'A' if oMandate.is_valid else 'B',  # 9. Status - O,leer = Offen; A = Aktiv; B = Beendet
                           u'CORE',  # 10. Mandatstyp
                           u'N',  # 11. Einmalmandat - J = Ja; N = Nein
                           u'%s' % 'J' if oMandate.is_valid else 'N',  # 12. Standardmandat - J = Ja; N = Nein
                           u'J',  # 13. BankImDebAnlegen - Kennzeichen, ob eine Bank zu dem Debitor angelegt werden soll
                           u'',  # 14. Letzte Verwendung am
                           u'',  # 15. Gläubiger-ID
                           u'',  # 16.
                           ]
                oCsvWriter.writerow(lPMSatz)

    oOutput.seek(0)
    return oOutput