function get_menu_order_forms(data_id) {
    // Finds and returns menu order forms of both normal and small screens
    return $('#mof-'+data_id+',#sv-mof-'+data_id);
}

function update_dependencies() {
    // Check for depending menus order status and enable/disable correspondingly.
    var $dep_menus = $('*[data-dependent-menu=1]');
    for (var i=0; i<$dep_menus.length; i++) {
        var $related_menu = $('#'+$($dep_menus[i]).data('required-menu'));
        if (!$related_menu.hasClass('menu-ordered')) {
            var $dep_menu = $($dep_menus[i]);
            $dep_menu.addClass('not-available');
            // Mark dependent menus as not ordered
            $dep_menu.removeClass('menu-ordered');
            $dep_menu.parents('tr').removeClass('menu-ordered');
            $dep_menu.find('form input[name=quantity]').val(0);
        } else {
            $($dep_menus[i]).removeClass('not-available');
        }
    }
}

function do_ajax(form){
    $.ajax({
        type: "GET",
        dataType:  'json',
        cache: false,
        data: {
            'order': form.find('input[name=order]').val(),
            'quantity': form.find('input[name=quantity]').val()
        },
        success: function(data) {
            if ( data['error'] == false ) {
                var $forms = get_menu_order_forms(data['id']);
                // Update form, input, label datas
                var quantity = isNaN(data.quantity)?0:data.quantity;
                if (quantity > 0) {
                    $( '#mo-'+data['id']+',#smo-'+data['id']).addClass("menu-ordered");
                } else {
                    $( '#mo-'+data['id']+',#smo-'+data['id']).removeClass("menu-ordered");
                }
                $( '#moi-' +  data['id'] ).val(data['id']);
                $forms.find('.order-quantity').text(quantity);
                $forms.find('input[name=current-quantity],input[name=quantity]').val(quantity).attr('value', quantity);

                // Update for small view only
                var $panel_heading = $( '#smo-'+data['id']).closest('.panel-collapse').prev();
                $panel_heading.children('.brief-order-status').replaceWith(data['order_brief_html']);

                var lunch = $('#child-meal-'+data['id']);
                var panel = $(lunch).closest('.panel-collapse');

                switch($forms.find('input[name=mealtime_type]').val()){
                    case 'breakfast':
                        var breakfast_holder = $(panel).prev('a.day-meal').find('.breakfast');
                        $(breakfast_holder).addClass('ordered');
                        break;
                    case 'lunch':
                        var lunch_holder = $(panel).prev('a.day-meal').find('.lunch');
                        $(lunch_holder).html($(lunch).html());
                        break;
                    case 'snack':
                        var snack_holder = $(panel).prev('a.day-meal').find('.snack');
                        $(snack_holder).addClass('ordered');
                        break;
                }

                var $input = $forms.find('input[name=quantity]');

                // Restore disabled controls after receiving _any_ reponse,
                // and remain disabled with minus button when quantity at 0
                $forms.find('.quantity-step').removeClass('disabled');
                if (!quantity) {
                    $input.attr('value', '0');
                    $forms.find('.quantity-step[data-method=minus]').addClass('disabled');
                }
                if (quantity >= $input.data('max') || !data.order_possible) {
                    $forms.find('.quantity-step[data-method=plus]').addClass('disabled');
                }

                // More description here pls...
                if (data['account_balance'] != '-1') {
                    $('#header_account_balance_value').text(data['account_balance']);
                }

                // NOT BEING USED
                // update_dependencies();
            } else {
                var modal = bootbox.alert(data['message']);
                force_modal_reposition(modal);
            }
        },
        error: function() {
            var modal = bootbox.alert('Es ist ein Fehler aufgetreten!');
            force_modal_reposition(modal);
        },
        complete: function(data) {
            // Verify all quantity inputs to ensure the value are valid
            var $inputs = $('form input[name=quantity]');
            for (var i=0; i<$inputs.length; i++) {
                var $input = $($inputs[i]);
                var $source = $input.siblings('input[name=current-quantity]');
                if ($input.val() != $source.val()) {
                    $input.val($source.val());
                }
            }
        }
    });
}

$(document).ready(function() {
    $('#anleitung').show();

    $('.menu_additives_toggle_link').show();
    $('.menu_additives_toggle_link_text1').show();
    $('.menu_additives_div').hide();
    $('.menu_additives_toggle_link').click(function(event) {
        event.preventDefault();
        outer_div = $( event.target ).closest("div");
        outer_div.find(".menu_additives_toggle_link_text1").toggle();
        outer_div.find(".menu_additives_toggle_link_text2").toggle();
        outer_div.next(".menu_additives_div").toggle();
    });


    // Increase or decrease the quantity value correspondingly.
    // Not effective with disabled controls.
    $('.menu-actions .quantity-step').click(function(event) {
      var $method = $(event.currentTarget);
      if ($method.hasClass('disabled')) { return; }
      var $input_quantity = $method.closest('.menu-actions').find('input[name=quantity]');
      var quantity = Number($input_quantity.val());
      if (isNaN(quantity))
        quantity = 0;
      if ($method.data('method') == 'minus') {
        if (quantity == 0) return;
        quantity--;
      } else {
        quantity++;
      }
      $input_quantity.val(quantity).change();
    });

    $('.menu-actions input[name=quantity]').change(function(e){
      var $form = $(this).closest('form');
      var $input = $(e.currentTarget);
      // Show confirmation before cancelling without beable to reorder.
      if (Number($form.data('order-status')) == 1 && Number($form.data('order-possible')) == 0){
        // if it is not possible to order other food - tell it to user and ask for confirmation
        bootbox.confirm('Möchten Sie das Essen wirklich abbestellen? Da die Bestellfrist für dieses Essen abgelaufen ist, können Sie kein Alternativessen bestellen.',
            function(result){
                if (result) {
                    do_ajax($input.parents('form'));
                } else {
                    $input.val(e.target.defaultValue);
                }
            });
      } else {
        do_ajax($input.parents('form'));
      }
    });

    // Show and hide the control button of each order action box respectively
    // to mouse's hovering event.
    $('.big_view .menu-actions').hover(
      function() {
        $(this).children('button').css('opacity', 1).css('filter', 'alpha(opacity=100)');
      },
      function() {
        $(this).children('button').css('opacity', 0).css('filter', 'alpha(opacity=0)');;
      }
    );

    // NOT BEING USED
    // update_dependencies();
});
