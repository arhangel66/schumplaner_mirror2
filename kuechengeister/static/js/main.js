var confirm_config = {
  message: "Are you sure to cancel the menu? You can't order an alternative!",
  title: "",
  show: false,
  buttons: {
    success: {
      label: "Yes",
      className: "btn-success",
      callback: function() {
        alert("Yes");
      }
    },
    danger: {
      label: "No",
      className: "btn-danger",
      callback: function() {
        alert("No");
      }
    }
  }
};

var alert_config = {
  message: "",
  title: "",
  buttons: {
    success: {
      label: "OK",
      className: "btn-success",
      callback: function() {
      }
    }
  }
};


var toggle_login = function(e){
    e.preventDefault();

    $('.auth-box').toggleClass('show');
    $('#overlay').toggleClass('show');
}

var toggle_nav = function(e){
    e.preventDefault();

    var toggle = this;
    if($('.navigation').hasClass('show')){
        $('.navigation').slideUp(500, function(){
            $('.navigation').toggleClass('show');
            $('.navigation').css('display', '');
            $(toggle).toggleClass('close');
        });
    }
    else{
        $('.navigation').slideDown(500, function(){
            $('.navigation').toggleClass('show');
            $('.navigation').css('display', '');
            $(toggle).toggleClass('close');
        });
    }


}

var show_loading = function(){
    $('.loading').addClass('show-block');
}
var hide_loading = function(){
    $('.loading').removeClass('show-block');
}

var next_child = function(e){
    e.preventDefault();
    //show_loading();

    // get currently selected child element
    var child_elem = $('.child-switch .child').not('.hidden');
    // get next element
    var next_child = $(child_elem).next('.child');

    if(next_child.length > 0){
        $(child_elem).addClass('hidden');
        $(next_child).removeClass('hidden');

        // if next child is last then disable right switch
        var even_next_child = $(next_child).next('.child');
        if(even_next_child.length == 0){
             $('.child-switch .right').addClass('disabled');
        }

        // and of course enable left switch
        $('.child-switch .left').removeClass('disabled');

        // Now hide menu object for current child
        var child_id = '#child_menu_' + child_elem.attr('id').replace('child_', '');
        $(child_id).addClass('hidden');

        // and now get id for next child menu
        var next_child_id = '#child_menu_' + next_child.attr('id').replace('child_', '');
        $(next_child_id).removeClass('hidden');
    }

    //setTimeout(hide_loading, 1000);
}
var prev_child = function(e){
    e.preventDefault();
    //show_loading();

    // get currently selected child element
    var child_elem = $('.child-switch .child').not('.hidden');
    // get prev element
    var prev_child = $(child_elem).prev('.child');

    if(prev_child.length > 0){
        $(child_elem).addClass('hidden');
        $(prev_child).removeClass('hidden');

        // if prev child is last then disable right switch
        var even_prev_child = $(prev_child).prev('.child');
        if(even_prev_child.length == 0){
             $('.child-switch .left').addClass('disabled');
        }

        // and of course enable left switch
        $('.child-switch .right').removeClass('disabled');

        // Now hide menu object for current child
        var child_id = '#child_menu_' + child_elem.attr('id').replace('child_', '');
        $(child_id).addClass('hidden');

        // and now get id for prev child menu
        var prev_child_id = '#child_menu_' + prev_child.attr('id').replace('child_', '');
        $(prev_child_id).removeClass('hidden');
    }

    //setTimeout(hide_loading, 1000);
}

var next_week = function(e){
    e.preventDefault();
    show_loading();
    setTimeout(hide_loading, 1000);
}
var prev_week = function(e){
    e.preventDefault();
    show_loading();
    setTimeout(hide_loading, 1000);
}


function modal_reposition() {
    var modal = $(this), dialog = modal.find('.modal-dialog');
    modal.css('display', 'block');

    // Dividing by two centers the modal exactly, but dividing by three
    // or four works better for larger screens.
    dialog.css("margin-top", Math.max(0, ($(window).height() - dialog.height()) / 2));
}

function force_modal_reposition(modal) {
    var dialog = modal.find('.modal-dialog');
    modal.css('display', 'block');

    // Dividing by two centers the modal exactly, but dividing by three
    // or four works better for larger screens.
    dialog.css("margin-top", Math.max(0, ($(window).height() - dialog.height()) / 2));
}


$(function(){
    // Reposition when a modal is shown
    $('.modal').on('show.bs.modal', modal_reposition);
    // Reposition when the window is resized
    $(window).on('resize', function() {
        $('.modal:visible').each(modal_reposition);
    });


    $('.toggle.for-auth').on('click', toggle_login);
    $('.modal_close').on('click', toggle_login);

    $('.toggle.for-navigation').on('click', toggle_nav);


    // How much child we have
    // if we have one child disable left switch and right switch
    if($(".child-switch .child").length <= 1){
        $('.child-switch .left').addClass('disabled');
        $('.child-switch .right').addClass('disabled');
    }
    else if($(".child-switch .child").length > 1){
        // But if we have more than one child disable only left switch
        $('.child-switch .left').addClass('disabled');
    }

    $('.child-switch .right').on('click', next_child);
    $('.child-switch .left').on('click', prev_child);

    //$('.week-switch .right').on('click', next_week);
    //$('.week-switch .left').on('click', prev_week);

    $('button.confirm').on('click', function(e){
        e.preventDefault();

        bootbox.dialog(confirm_config);
    });

    $('.datepicker').datepicker({
        format: 'dd.mm.yyyy',
        weekStart: 1,
        language: 'de',
        autoclose: true
    });
});
