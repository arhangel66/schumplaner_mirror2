# -*- coding: utf-8 -*-
from __future__ import with_statement
from datetime import date

import time

from django.db import transaction
from django.template.loader import render_to_string
from django.contrib.auth.models import User
from django.utils.log import getLogger

from kg.models import Customer, Child, Order
from settings import OVERLOAD_SITE


logger = getLogger(__name__)


def create_filter_list(request, param_names=[], param_values=[],
                       preselected=[], all_field=False, exclude_params=[]):
    
    base_link = '?'
    links = list()
    selected_all = True
    
    
    # create base link    
    if request.GET:
        for param, p_value in request.GET.items(): 
            if param not in param_names and param not in exclude_params:
                base_link = base_link + param + '=' + p_value + '&'
            else:
                selected_all = False
                
    # add all link
    if all_field:
        links.append({'name': 'Alle', 'selected': selected_all, 'link': base_link})

    # start creating links
    for item_index, dummy in enumerate(param_values[0]):
        for param_index, param_name in enumerate(param_names):
            # check GET params and preselected paramter to decide if item is selected  
            if request.GET.has_key(param_name) and str(param_values[param_index][item_index][0]) == str(request.GET[param_name]):
                selected = True
            elif len(preselected) > param_index and str(str(param_values[param_index][item_index][0])) == str(preselected[param_index]):
                selected = True
            else:
                selected = False
                
            if param_index == 0:
                link = base_link + param_name + '=' + str(param_values[param_index][item_index][0])
            else:
                link = link + '&' +  param_name + '=' + str(param_values[param_index][item_index][0])
            
        links.append({'name': param_values[param_index][item_index][1],
                      'selected': selected,
                      'link': link})
    return links


def get_voucher_html(request, oChild, iYear, iMonth):
    oCustomer = Customer.objects.get(child=oChild)

    # change customer number to old one for tommys
    if OVERLOAD_SITE == 'tommys':
        if 10000 <= oChild.cust_nbr < 20000:
            sCustNumber = 'A-%d' % (oChild.cust_nbr - 10000)
        elif 20000 <= oChild.cust_nbr < 30000:
            sCustNumber = 'B-%d' % (oChild.cust_nbr - 20000)
        elif 30000 <= oChild.cust_nbr < 40000:
            sCustNumber = 'C-%d' % (oChild.cust_nbr - 30000)
        elif 40000 <= oChild.cust_nbr < 50000:
            sCustNumber = 'D-%d' % (oChild.cust_nbr - 40000)
        else:
            sCustNumber = oChild.cust_nbr
    else:
        sCustNumber = oChild.cust_nbr

    lVouchers = []
    qsOrders = Order.objects.select_related().filter(child=oChild, menu__date__year=iYear, menu__date__month=iMonth).order_by('menu__date')
    if not qsOrders:
        return ''

    for oOrder in qsOrders:
        lVouchers.append({'date': oOrder.menu.date,
                          'cust_nbr': sCustNumber,
                          'menu_name': oOrder.menu.meal.name_short,
                          'child_name': '%s %s' % (oChild.surname, oChild.name)})

    # fill up the table
    iCellsToFill = 4 - len(lVouchers) % 4
    if iCellsToFill < 4:
        for i in range(0, iCellsToFill):
            lVouchers.append({'date': '   ',
                              'cust_nbr': '   ',
                              'menu_name': '   ',
                              'child_name': '   '})

    return render_to_string('kg/utils_voucher.html', {'month_date': date(iYear, iMonth, 1),
                                                      'voucher_list': lVouchers,
                                                      'customer': oCustomer,
                                                      'child': oChild})


def update_children_from_pos_data(data):
    if 'updated_children' in data:
        nfc_tag_updated = []
        chip_number_updated = []
        chip_deposit_updated = []
        with transaction.commit_manually():
            try:
                for child in data['updated_children']:
                    if child['nfc_tag_updated'] == 1:
                        (Child.objects.filter(id=child['id'],
                                              rfid_tag_update_time__lte=child['nfc_tag_update_time'])
                         .update(rfid_tag=child['nfc_tag'],
                                 rfid_tag_update_time=child['nfc_tag_update_time']))
                        nfc_tag_updated.append(child['id'])
                    if child['chip_number_updated'] == 1:
                        (Child.objects.filter(id=child['id'],
                                              chip_number_update_time__lte=child['chip_number_update_time'])
                         .update(chip_number=child['chip_number'],
                                 chip_number_update_time=child['chip_number_update_time']))
                        chip_number_updated.append(child['id'])
                    if child['chip_deposit_updated'] == 1:
                        (Child.objects.filter(id=child['id'],
                                              chip_deposit_update_time__lte=child['chip_deposit_update_time'])
                         .update(chip_deposit=child['chip_deposit'],
                                 chip_deposit_update_time=child['chip_deposit_update_time']))
                        chip_deposit_updated.append(child['id'])
                transaction.commit()

                updated_children = {'nfc_tag_updated': nfc_tag_updated,
                                    'chip_number_updated': chip_number_updated,
                                    'chip_deposit_updated': chip_deposit_updated}

                return updated_children

            except Exception:
                transaction.rollback()
                raise


def update_orders_from_pos_data(data):
    """ This method is used for old Push API that gets order_id back from terminal """
    updated_orders = []
    if 'updated_orders' in data:
        with transaction.commit_manually():
            try:
                for order in data['updated_orders']:
                    Order.objects.filter(id=order['id']).update(pickup_time=order['pickup_time'])
                    updated_orders.append(order['id'])
                transaction.commit()

                return updated_orders

            except Exception:
                transaction.rollback()
                raise

def update_orders_from_pos_data_v2(data):
    """ This method is used for new Push API that gets (child_id, menu_id) tuple back from terminal
        Also this new method is able to process orders that were created on the terminal """
    updated_orders = []
    if 'updated_orders' in data:
        with transaction.commit_manually():
            try:
                for order in data['updated_orders']:
                    oOrder, bCreated = Order.all_objects.get_or_create(child_id=order['child_id'], menu_id=order['menu_id'])
                    if not bCreated:
                        # use update method to prevent that last_modified datetime gets overwritten
                        Order.all_objects.filter(child_id=order['child_id'], menu_id=order['menu_id']).update(quantity=order['quantity'],
                                                                                                              pickup_time=order['pickup_time'])
                    else:
                        oOrder.quantity = order['quantity']
                        oOrder.pickup_time = order['pickup_time']
                        oOrder.save()

                    updated_orders.append((order['child_id'], order['menu_id']))
                transaction.commit()

                return updated_orders

            except Exception as exc:
                transaction.rollback()
                raise


def get_cat_standard_view_url_params(sOverloadSite):
    """
    Returns the URL params for the display of the CustomerAccountTransactions standard view
    """
    if sOverloadSite in ['leibundseele']:
        return ''
    else:
        return '&status__exact=0'


def deactivate_user(username):
    """Perform routines for deactivating a Django user, not deleting, to ensure
    no conflict will happen."""
    try:
        oOldUser = User.objects.get(username=username)
        oOldUser.username = '%s_old_%s' % (oOldUser.username, str(time.time()))
        oOldUser.is_active = False
        oOldUser.save()
    except Exception as exc:
        logger.exception(exc)
