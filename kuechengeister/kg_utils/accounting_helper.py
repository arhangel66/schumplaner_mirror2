# -*- coding: utf-8 -*-

from __future__ import with_statement
import re
import chardet
import StringIO
from decimal import Decimal
from datetime import datetime

from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from django.db import transaction
from django.utils.log import getLogger

from kg.models import BankAccountTransaction, Customer, CustomerAccountTransaction
from kg_utils import ucsv as csv
from kg_utils.helpers import encode_uploaded_file_to_utf8
from settings import ACCOUNTING_SETTINGS, SEPA, OVERLOAD_SITE, MODULE_PREPAID

logger = getLogger(__name__)


def match_bank_account_transactions():
    """
    Match the BankAccountTransactions with the open CustomerAccountTransactions

    @return: integer Number of successful matched BankAccountTransactions
    """

    iMatchCounter = 0

    # IMPORTANT: do not change order of matching methods

    # match the debit type BankAccountTransactions
    iMatchCounter += match_type_debit()

    # match the debit return type BankAccountTransactions
    iMatchCounter += match_type_debit_return()

    # match the banktransfer type BankAccountTransactions
    if OVERLOAD_SITE == 'bodelessen':
        iMatchCounter += match_type_banktransfer_bodelschwinghhaus()
    elif OVERLOAD_SITE == 'rudolstadt':
        iMatchCounter += match_type_banktransfer_rudolstadt()
    elif MODULE_PREPAID:
        iMatchCounter += match_type_banktransfer_prepaid()
    else:
        iMatchCounter += match_type_banktransfer()

    return iMatchCounter


def match_type_debit():
    """
    Matches BankAccountTransaction of type debit with the already created debit Customer Account Transactions

    @return: integer Number of successful matched BankAccountTransactions
    """
    iCounter = 0

    oBATs = BankAccountTransaction.objects.open().filter(type=BankAccountTransaction.TYPE_DEBIT)
    for oBAT in oBATs:

        # extract SEPA ID from subject
        sSepaEndToEndRef = ''
        sHaystack = oBAT.subject.replace(' ', '')
        iStart = sHaystack.find(SEPA['SEPA_EREF_PREFIX'])
        if iStart:
            sSepaEndToEndRef = sHaystack[iStart:iStart+32]  # EREF is 32 charakters long
            if sSepaEndToEndRef == '':
                continue
        else:
            continue

        # retrieve corresponding CAT: transaction_id of the debit CAT is the debit EndToEndRef (EREF+)
        try:
            oMatchingCAT = CustomerAccountTransaction.objects.get(type=CustomerAccountTransaction.TYPE_DEBIT,
                                                                  transaction_id__iexact=sSepaEndToEndRef)
        except (ObjectDoesNotExist, MultipleObjectsReturned):
            continue

        with transaction.commit_manually():
            try:
                # set BAT hash, subject and amount to CAT
                oMatchingCAT.bat_hash = oBAT.t_hash
                oMatchingCAT.description = oBAT.subject
                oMatchingCAT.amount = oBAT.amount
                oMatchingCAT.save()

                # set status of BAT
                oBAT.setStatusBooked()
                oBAT.save()

            except Exception, message:
                transaction.rollback()
                continue
            else:
                transaction.commit()
                iCounter += 1

    return iCounter


def match_type_debit_return():
    """
    Matches BankAccountTransaction of type debit return with CustomerAccountTransactions of type debit

    Creates a debit return CAT and a debit return fee CAT out of the BankAccountTransaction.
    The debit return fee is calculated from the amount difference or a fixed amount is set (settings parameter).

    @return: integer Number of successful matched BankAccountTransactions
    """

    iCounter = 0

    oBATs = BankAccountTransaction.objects.open().filter(type=BankAccountTransaction.TYPE_DEBIT_RETURN)
    for oBAT in oBATs:

        # extract SEPA ID from subject
        sSepaEndToEndRef = ''
        sHaystack = oBAT.subject.replace(' ', '')
        iStart = sHaystack.find(SEPA['SEPA_EREF_PREFIX'])
        if iStart:
            sSepaEndToEndRef = sHaystack[iStart:iStart+32]  # EREF is 32 charakters long
            if sSepaEndToEndRef == '':
                continue
        else:
            continue

        # retrieve corresponding debit CAT: transaction_id of the debit CAT is the debit EndToEndRef (EREF+)
        try:
            oMatchCAT = CustomerAccountTransaction.objects.get(transaction_id__iexact=sSepaEndToEndRef)
        except (ObjectDoesNotExist, MultipleObjectsReturned):
            continue

        with transaction.commit_manually():
            try:
                # create new CustomerAccountTransaction
                oNewCAT = createCustAccTransOutOfBankAccTrans(oMatchCAT.customer.id, oBAT)

                # mark connected invoice as open and disconnect from debit
                if oMatchCAT.connected_to:
                    CustomerAccountTransaction.objects.filter(id=oMatchCAT.connected_to, customer=oMatchCAT.customer).update(connected_to='', status=CustomerAccountTransaction.STATUS_OPEN)

                # connect debit CAT and debit return CAT
                connectCustAccTrans(oMatchCAT, oNewCAT)

                # calculate debit return fees
                oFees = oNewCAT.amount.copy_abs() - oMatchCAT.amount.copy_abs()

                # adjust amount of returned debit CAT to amount without fees
                oNewCAT.amount = oMatchCAT.amount.copy_negate()

                # add values for original amount  (OAMT -> OAMTX+) and compensation amount (COAM -> COAMX+) to description
                oNewCAT.description += "  OAMTX+%s COAMX+%s" % (('%.2f' % oMatchCAT.amount).replace('.', ','), ('%.2f' % oFees).replace('.', ','))

                oNewCAT.save()

                # create new CustomerAccountTransaction for the debit return fee
                oReturnFeeCAT = CustomerAccountTransaction(customer=oNewCAT.customer)
                oReturnFeeCAT.type = CustomerAccountTransaction.TYPE_DEBIT_RETURN_FEE
                oReturnFeeCAT.transaction_id = oNewCAT.transaction_id
                oReturnFeeCAT.transaction_date = oNewCAT.transaction_date
                if ACCOUNTING_SETTINGS['DEBIT_RETURN_FEE'] == 'ORIGINAL':
                    oReturnFeeCAT.amount = oFees.copy_negate()
                else:
                    oReturnFeeCAT.amount = Decimal(str(ACCOUNTING_SETTINGS['DEBIT_RETURN_FEE'])).copy_negate()
                oReturnFeeCAT.description = u"Gebühren der Lastschriftrückgabe vom %s" % oNewCAT.transaction_date.strftime('%d.%m.%Y')
                # save the CAT only if the amount is not zero
                if not oReturnFeeCAT.amount.is_zero():
                    oReturnFeeCAT.save()

                iCounter += 1

            except Exception, message:
                transaction.rollback()
                continue
            else:
                transaction.commit()

    return iCounter


def match_type_banktransfer():
    """
    Matches BankAccountTransaction of type bank transfer with open CustomerAccountTransactions of type invoice

    @return: integer Number of successful matched BankAccountTransactions
    """

    iCounter = 0

    # match all open invoice CustomerAccountTransactions with all open bank transfer BankAccountTransactions
    oCATs = CustomerAccountTransaction.objects.open().filter(type=CustomerAccountTransaction.TYPE_INVOICE)
    for oCAT in oCATs:
        oBATs = BankAccountTransaction.objects.open().filter(type=BankAccountTransaction.TYPE_BANK_TRANSFER_IN,
                                                             subject__contains=oCAT.transaction_id,
                                                             amount=-oCAT.amount)

        if len(oBATs) == 1:
            oBAT = oBATs[0]
            # create new CustomerAccountTransaction
            oNewCustomerAccountTransaction = createCustAccTransOutOfBankAccTrans(oCAT.customer.id, oBAT)
            if oNewCustomerAccountTransaction:
                # connect CATs
                connectCustAccTrans(oCAT, oNewCustomerAccountTransaction)
                iCounter += 1

    return iCounter


def match_type_banktransfer_prepaid():
    """
    Creates CustomerAccountTransactions out of BankAccountTransaction of type bank transfer

    @return: integer Number of successful matched BankAccountTransactions
    """

    iCounter = 0

    if 'CSV_REGEX' in ACCOUNTING_SETTINGS and ACCOUNTING_SETTINGS['CSV_REGEX'] != '':
        p = re.compile(ACCOUNTING_SETTINGS['CSV_REGEX'])
    else:
        p = re.compile('^([0-9]{5}) ')  # default

    for oBAT in BankAccountTransaction.objects.open().filter(type=BankAccountTransaction.TYPE_BANK_TRANSFER_IN):
        oMatchResult = p.findall(oBAT.subject)
        if oMatchResult:
            iCustNbr = int(oMatchResult[0])
            try:
                oMatchCustomer =  Customer.objects.get(child__cust_nbr=iCustNbr)
                oNewCustomerAccountTransaction = createCustAccTransOutOfBankAccTrans(oMatchCustomer.id, oBAT)
                if oNewCustomerAccountTransaction:
                    oNewCustomerAccountTransaction.description = u"Überweisungseingang vom %s" % oBAT.t_date.strftime('%d.%m.%Y')
                    oNewCustomerAccountTransaction.save()
                    iCounter += 1

            except (ObjectDoesNotExist, MultipleObjectsReturned) as exc:
                pass
            except Exception as exc:
                logger.exception('Error in BankAccountTransaction matching.')

    return iCounter


def match_type_banktransfer_bodelschwinghhaus():
    """
    Matches BankAccountTransaction of type bank transfer to customers

    @return: integer Number of successful matched BankAccountTransactions
    """
    iCounter = 0

    for oBAT in BankAccountTransaction.objects.open().filter(type=BankAccountTransaction.TYPE_BANK_TRANSFER_IN):
        # search customer which has a child with the given customer number
        qsCustomers = Customer.objects.filter(child__cust_nbr=oBAT.bank_account_owner)
        if len(qsCustomers) == 1:
            oNewCustomerAccountTransaction = createCustAccTransOutOfBankAccTrans(qsCustomers[0].id, oBAT)
            if oNewCustomerAccountTransaction:
                oNewCustomerAccountTransaction.description = "Guthabeneinzahlung"
                oNewCustomerAccountTransaction.save()
                iCounter += 1

    return iCounter

def match_type_banktransfer_rudolstadt():
    """
    Matches BankAccountTransaction of type bank transfer to customers

    @return: integer Number of successful matched BankAccountTransactions
    """
    iCounter = 0

    for oBAT in BankAccountTransaction.objects.open().filter(type=BankAccountTransaction.TYPE_BANK_TRANSFER_IN):
        # search customer with the given customer.extra_attribute
        qsCustomers = Customer.objects.filter(extra_attribute=oBAT.bank_account_owner)
        if len(qsCustomers) == 1:
            oNewCustomerAccountTransaction = createCustAccTransOutOfBankAccTrans(qsCustomers[0].id, oBAT)
            if oNewCustomerAccountTransaction:
                oNewCustomerAccountTransaction.description = "Guthabeneinzahlung"
                oNewCustomerAccountTransaction.save()
                iCounter += 1

    return iCounter

def createCustAccTransOutOfBankAccTrans(iCustomerId, oBankAccountTransaction):
    """
    Creates CustomerAccountTransaction out of Customer and BankAccountTransaction and saves it
    Mark BankAccountTransaction as booked

    @param: CustomerId The Customer the CustomerAccountTransaction is created for
    @param: BankAccountTransaction The BankAccountTransaction the CustomerAccountTransaction is created from

    @return: CustomerAccountTransaction The new created CustomerAccountTransaction
    """

    # retrieve Customer
    try:
        oCustomer = Customer.objects.get(id=iCustomerId)
    except ObjectDoesNotExist:
        return None

    # create new CustomerAccountTransaction
    oNewCustomerAccountTransaction = CustomerAccountTransaction(customer=oCustomer)

    oNewCustomerAccountTransaction.transaction_id = oBankAccountTransaction.t_hash
    oNewCustomerAccountTransaction.bat_hash = oBankAccountTransaction.t_hash
    oNewCustomerAccountTransaction.transaction_date = oBankAccountTransaction.t_date
    oNewCustomerAccountTransaction.amount = oBankAccountTransaction.amount
    oNewCustomerAccountTransaction.description = oBankAccountTransaction.subject
    oNewCustomerAccountTransaction.type = oBankAccountTransaction.type
    oNewCustomerAccountTransaction.save()

    # set status of BankAccountTransaction
    oBankAccountTransaction.setStatusBooked()
    oBankAccountTransaction.save()

    return oNewCustomerAccountTransaction


def connectCustAccTrans(oCustAccTrans1, oCustAccTrans2):
    """
    Connects two CustomerAccountTransactions and sets status of both to booked

    @param: oCustAccTrans1 The first CustomerAccountTransaction
    @param: oCustAccTrans2 The second CustomerAccountTransaction
    """

    oCustAccTrans1.connected_to = str(oCustAccTrans2.id)
    oCustAccTrans1.setStatusBooked()
    oCustAccTrans1.save()

    oCustAccTrans2.connected_to = str(oCustAccTrans1.id)
    oCustAccTrans2.setStatusBooked()
    oCustAccTrans2.save()


def parse_and_save_bank_csv_upload(django_uploaded_file):
    """
    Parse the rawfile input with the csv dictreader (use site specific csv configuration)
    and save the parsed input as BankAccountTransaction objects

    @param: InMemoryUploadedFile The uploaded file
    @return: dict with 'success': boolean, 'message': string
    """

    iInsertCounter = 0
    iDuplicateCounter = 0
    sMessage = ''

    with transaction.commit_manually():

        # use CSV DictReader to parse the data
        try:
            file = encode_uploaded_file_to_utf8(django_uploaded_file)

            if 'CSV_FIELDNAMES' in ACCOUNTING_SETTINGS and ACCOUNTING_SETTINGS['CSV_FIELDNAMES'] != '':
                oReader = csv.DictReader(file,
                                         fieldnames=ACCOUNTING_SETTINGS['CSV_FIELDNAMES'],
                                         delimiter=ACCOUNTING_SETTINGS['CSV_DELIMITER'],
                                         restkey='RESTKEY', restval='RESTVAL')
            else:
                oReader = csv.DictReader(file,
                                         delimiter=ACCOUNTING_SETTINGS['CSV_DELIMITER'],
                                         restkey='RESTKEY', restval='RESTVAL')

            iRowCounter = 0
            for dRow in oReader:
                iRowCounter += 1

                # check data integrity
                if 'RESTKEY' in dRow or 'RESTVAL' in dRow:
                    sMessage = u'Fehler: CSV-Datei fehlerhaft. Anzahl der Kopffelder stimmt nicht mit Anzahl der Felder in Reihe %s überein.' % iRowCounter
                    raise StandardError

                oTmpTrans = BankAccountTransaction()

                # set transaction type if in known types
                if ACCOUNTING_SETTINGS['CSV_TYPE'] in dRow:
                    for sAccType, lCsvType in ACCOUNTING_SETTINGS['CSV_TYPE_CHOICES'].iteritems():
                        if dRow[ACCOUNTING_SETTINGS['CSV_TYPE']].strip() in lCsvType:
                            oTmpTrans.type = getattr(BankAccountTransaction, sAccType)
                            break

                # set data for hash
                sHashData = dRow[ACCOUNTING_SETTINGS['CSV_CONTRAACCOUNT_NUMBER']].strip()
                sHashData += dRow[ACCOUNTING_SETTINGS['CSV_CONTRAACCOUNT_ICODE']].strip()
                sHashData += dRow[ACCOUNTING_SETTINGS['CSV_CONTRAACCOUNT_OWNER']].strip()
                sHashData += dRow[ACCOUNTING_SETTINGS['CSV_DATE']].strip()
                for subject_column_name in ACCOUNTING_SETTINGS['CSV_SUBJECT_FIELDS']:
                    sHashData += dRow[subject_column_name].strip()
                sHashData += dRow[ACCOUNTING_SETTINGS['CSV_AMOUNT']].strip()
                oTmpTrans.setHash(sHashData.encode('utf-8'))

                # set subject
                oTmpTrans.subject = ''
                for subject_column_name in ACCOUNTING_SETTINGS['CSV_SUBJECT_FIELDS']:
                    oTmpTrans.subject += dRow[subject_column_name].strip()
                # clean some SFIRM specific text
                if oTmpTrans.subject.startswith('SVWZ+'):
                    oTmpTrans.subject = oTmpTrans.subject[5:]
                if 'CSV_EREF' in ACCOUNTING_SETTINGS and ACCOUNTING_SETTINGS['CSV_EREF'] in dRow:
                    oTmpTrans.subject += ' EREF+ %s MREF' % dRow[ACCOUNTING_SETTINGS['CSV_EREF']].strip()

                # clean mandate id and invoice number in subject for later use
                oTmpTrans.subject = re.sub('(RG|MA)[ 0-9]+',
                                           lambda x: str(x.group()).replace(' ', '') + ' ',
                                           oTmpTrans.subject)

                # other properties
                if ACCOUNTING_SETTINGS['CSV_TYPE'] in dRow:
                    oTmpTrans.booking_type = dRow[ACCOUNTING_SETTINGS['CSV_TYPE']].strip()
                else:
                    oTmpTrans.booking_type = 'Unbekannt'
                oTmpTrans.t_date = datetime.strptime(dRow[ACCOUNTING_SETTINGS['CSV_DATE']],
                                                     ACCOUNTING_SETTINGS['CSV_DATE_FORMAT']).date()
                # clean the amount so that also numbers in format 8.000,00 and 8000,00 are possible
                sCleanedAmount = dRow[ACCOUNTING_SETTINGS['CSV_AMOUNT']].replace(',', '.')
                sCleanedAmount = sCleanedAmount.replace('.', '', sCleanedAmount.count('.') -1)
                oTmpTrans.amount = Decimal(sCleanedAmount)
                oTmpTrans.bank_account_owner = dRow[ACCOUNTING_SETTINGS['CSV_CONTRAACCOUNT_OWNER']].strip()
                oTmpTrans.bank_account_number = dRow[ACCOUNTING_SETTINGS['CSV_CONTRAACCOUNT_NUMBER']]
                oTmpTrans.bank_icode = dRow[ACCOUNTING_SETTINGS['CSV_CONTRAACCOUNT_ICODE']]

                # check for duplicate
                iDupTest = BankAccountTransaction.objects.filter(t_hash=oTmpTrans.t_hash).count()
                if iDupTest == 0:
                    oTmpTrans.save()
                    iInsertCounter += 1
                else:
                    iDuplicateCounter += 1
                    continue

        except Exception as exc:
            transaction.rollback()
            template = "Es ist ein Fehler vom Typ {0} aufgetreten! Hinweis: {1!r}"
            sMessage = template.format(type(exc).__name__, exc.args)
            return {'success': False,
                    'message': sMessage}

        else:
            transaction.commit()
            file.close()
            sMessage = 'Es wurden %s Buchungen importiert.' % iInsertCounter
            if iDuplicateCounter > 0:
                sMessage += ' %s Buchungen waren schon vorhanden.' % iDuplicateCounter
            return {'success': True,
                    'message': sMessage}


def parse_and_save_bank_csv_upload_bodelschwinghhaus(django_uploaded_file):
    """
    Parse the Bodelschwingh-Haus special rawfile input with the csv dictreader
    and save the parsed input as BankAccountTransaction objects

    @param: InMemoryUploadedFile The uploaded file
    @return: dict with 'success': boolean, 'message': string
    """
    iInsertCounter = 0
    iDuplicateCounter = 0
    sMessage = ''

    with transaction.commit_manually():

        # use CSV DictReader to parse the data
        try:
            file = encode_uploaded_file_to_utf8(django_uploaded_file)
            oReader = csv.DictReader(file,
                                     fieldnames=['Datum', 'Betrag', 'Betreff', 'Kundennummer'],
                                     delimiter=';',
                                     restkey='RESTKEY', restval='RESTVAL')

            iRowCounter = 0
            for dRow in oReader:
                iRowCounter += 1

                # check data integrity
                if 'RESTKEY' in dRow or 'RESTVAL' in dRow:
                    sMessage = u'Fehler: CSV-Datei fehlerhaft. Anzahl der Kopffelder stimmt nicht mit Anzahl der Felder in Reihe %s überein.' % iRowCounter
                    raise StandardError

                oTmpTrans = BankAccountTransaction()
                oTmpTrans.type = BankAccountTransaction.TYPE_BANK_TRANSFER_IN

                # set data for hash
                sHashData = dRow['Datum'].strip()
                sHashData += dRow['Betrag'].strip()
                sHashData += dRow['Betreff'].strip()
                sHashData += dRow['Kundennummer'].strip()
                oTmpTrans.setHash(sHashData.encode('utf-8'))

                # set subject
                oTmpTrans.subject = dRow['Betreff'].strip()

                # other properties
                oTmpTrans.booking_type = u'Überweisung'
                oTmpTrans.t_date = datetime.strptime(dRow['Datum'].strip(), '%d.%m.%Y').date()
                oTmpTrans.amount = Decimal(dRow['Betrag'].replace(',', '.')).copy_negate()
                oTmpTrans.bank_account_owner = dRow['Kundennummer'].strip()
                oTmpTrans.bank_account_number = ''
                oTmpTrans.bank_icode = ''

                # check for duplicate
                iDupTest = BankAccountTransaction.objects.filter(t_hash=oTmpTrans.t_hash).count()
                if iDupTest == 0:
                    oTmpTrans.save()
                    iInsertCounter += 1
                else:
                    iDuplicateCounter += 1

        except Exception as exc:
            transaction.rollback()
            template = "Es ist ein Fehler vom Typ {0} aufgetreten! Hinweis: {1!r}"
            sMessage = template.format(type(exc).__name__, exc.args)
            return {'success': False,
                    'message': sMessage}

        else:
            transaction.commit()
            file.close()
            sMessage = 'Es wurden %s Buchungen importiert.' % iInsertCounter
            if iDuplicateCounter > 0:
                sMessage += ' %s Buchungen waren schon vorhanden.' % iDuplicateCounter
            return {'success': True,
                    'message': sMessage}

def parse_and_save_bank_xlsx_upload_rudolstadt(django_uploaded_file):
    """
    Parse the AWO Rudolstadt special rawfile input with openpyxl
    and save the parsed input as BankAccountTransaction objects

    @param: InMemoryUploadedFile The uploaded file
    @return: dict with 'success': boolean, 'message': string
    """
    from openpyxl import load_workbook

    iInsertCounter = 0
    iDuplicateCounter = 0
    sMessage = ''

    with transaction.commit_manually():

        # use openpyxl to parse the data
        try:
            oWorkbook = load_workbook(django_uploaded_file)
            oSheet = oWorkbook.active

            iRowCounter = 0
            for oRow in oSheet.iter_rows():
                sDate = str(oRow[0].value)  # date
                sInternalNumber = str(oRow[4].value)  # internal number
                sDebitorNumber = str(oRow[5].value)  # debitor number
                sAmount = str(oRow[9].value)  # amount
                sSubject = str(oRow[11].value)  # subject

                # leave out header and the sum line at the end
                if sDate == 'Belegdatum' or sDate == 'None':
                    continue

                iRowCounter += 1

                oTmpTrans = BankAccountTransaction()
                oTmpTrans.type = BankAccountTransaction.TYPE_BANK_TRANSFER_IN

                # set data for hash
                sHashData = sDate.strip()
                sHashData += sInternalNumber.strip()
                sHashData += sDebitorNumber.strip()
                sHashData += sSubject.strip()
                sHashData += sAmount.strip()
                oTmpTrans.setHash(sHashData.encode('utf-8'))

                # set subject
                oTmpTrans.subject = sSubject.strip()

                # other properties
                oTmpTrans.booking_type = u'Überweisung'
                oTmpTrans.t_date = datetime.strptime(sDate.strip(), '%d.%m.%Y').date()
                oTmpTrans.amount = Decimal(sAmount)
                oTmpTrans.bank_account_owner = sDebitorNumber.strip()
                oTmpTrans.bank_account_number = ''
                oTmpTrans.bank_icode = ''

                # check for duplicate
                iDupTest = BankAccountTransaction.objects.filter(t_hash=oTmpTrans.t_hash).count()
                if iDupTest == 0:
                    oTmpTrans.save()
                    iInsertCounter += 1
                else:
                    iDuplicateCounter += 1

        except Exception as exc:
            transaction.rollback()
            template = "Es ist ein Fehler vom Typ {0} aufgetreten! Hinweis: {1!r}"
            sMessage = template.format(type(exc).__name__, exc.args)
            return {'success': False,
                    'message': sMessage}

        else:
            transaction.commit()
            sMessage = 'Es wurden %s Buchungen importiert.' % iInsertCounter
            if iDuplicateCounter > 0:
                sMessage += ' %s Buchungen waren schon vorhanden.' % iDuplicateCounter
            return {'success': True,
                    'message': sMessage}