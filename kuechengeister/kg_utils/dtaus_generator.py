# -*- coding: utf-8 -*- 

import string
from settings import DTAUS
from kg.models import InvoiceLog, Child
from kg_utils.date_helper import DateHelper
from kg_utils.invoice_generator import InvoiceGenerator
from datetime import date


class Dtaus:
    def __init__(self, year, month, facility=None):
        '''
         Erstelle eine DTAUS-Datei.
         Typ ist 'LK' (Lastschrift Kunde)
        '''
        self.typ = 'LK'
        self.zahlungsart = '05000' # LK (Lastschrift des Einzugsermächtigungsverfahrens)
        
        self.year = year
        self.month = month
        self.date = date.today()
        self.from_date = date(year, month, 1)
        self.to_date = DateHelper.getLastDayOfMonth(self.from_date)
        self.facility = facility
        
        self.sum   = { 'konto': 0, 'blz': 0, 'euro': 0 } #Prüfsummen
        self.anzBuchungen = 0
        self.data = ''
            
        self.generateData()
    
    
    
    def generateData(self):
        
        self.data = self.dataA()
        
        childs = Child.objects.filter(customer__pay_type__id=1,
                                      customer__bank_icode__isnull=False,
                                      customer__bank_account__isnull=False,
                                      customer__bank_account_owner__isnull=False,).exclude(
                                      customer__bank_icode__exact='',
                                      customer__bank_account__exact='',
                                      customer__bank_account_owner__exact='',)
        if self.facility:
            childs = childs.filter(facility=self.facility)
            
        invoices = InvoiceLog.objects.filter(invoice_date__year=self.year, invoice_date__month=self.month, child__in=childs)
            
        for invoice in invoices:
            
            if invoice.invoice_total > 0:

                buchung = {'kontoinhaber' : invoice.child.customer.bank_account_owner,
                           'kto'          : long(''.join((invoice.child.customer.bank_account).split())),
                           'blz'          : long(''.join((invoice.child.customer.bank_icode).split())),
                           'betrag'       : invoice.invoice_total,
                           'zweck1'       : 'Essengeldabrechnung ' + '%02d' % self.month + '\x2F' + '%04d' % self.year,
                           'zweck2'       : invoice.child.surname + ' ' + invoice.child.name,
                           'zweck3'       : invoice.invoice_number }
                self.data += self.dataC(buchung)  
                
        self.data += self.dataE()
        
    
    
    def convert_text(self, text):
        # Zeichen umsetzen gemäß DTA-Norm
        text = text.lower()
        text = text.replace(u'é', 'e')
        text = text.replace(u'è', 'e')
        text = text.replace(u'á', 'a')
        text = text.replace(u'à', 'a')
        text = text.replace(u'í', 'i')
        text = text.replace(u'ä', 'ae')
        text = text.replace(u'ö', 'oe')
        text = text.replace(u'ü', 'ue')
        text = text.replace(u'Ä', 'Ae')
        text = text.replace(u'Ö', 'Oe')
        text = text.replace(u'Ü', 'Ue')
        text = text.replace(u'ß', 'ss')
        text = text.replace(u'č', 'c')
        text = string.upper(text)
        text.encode("cp437", 'ignore')
        text.encode("utf-8", 'ignore')
        return text
    
        
    def dataA(self):
        '''
        Erstellen A-Segment der DTAUS-Datei
        Aufbau des Segments:
        Nr.   Start  Länge         Beschreibung 
        1     0       4 Zeichen      Länge des Datensatzes, immer 128 Bytes, also immer "0128" 
        2     4       1 Zeichen      Datensatz-Typ, immer 'A' 
        3     5       2 Zeichen      Art der Transaktionen 
                                        "LB" für Lastschriften Bankseitig 
                                        "LK" für Lastschriften Kundenseitig 
                                        "GB" für Gutschriften Bankseitig 
                                        "GK" für Gutschriften Kundenseitig  
        4     7       8 Zeichen      Bankleitzahl des Auftraggebers 
        5     15      8 Zeichen      CST, "00000000", nur belegt, wenn Diskettenabsender Kreditinstitut 
        6     23      27 Zeichen     Name des Auftraggebers 
        7     50      6 Zeichen      aktuelles Datum im Format DDMMJJ 
        8     56      4 Zeichen      CST, "    " (Blanks) 
        9     60      10 Zeichen     Kontonummer des Auftraggebers 
        10    70      10 Zeichen     Optionale Referenznummer 
        11a   80      15 Zeichen     Reserviert, 15 Blanks 
        11b   95      8 Zeichen      Ausführungsdatum im Format DDMMJJJJ. Nicht jünger als Erstellungsdatum (A7), jedoch höchstens 15 Kalendertage später. Sonst Blanks. 
        11c  103     24 Zeichen      Reserviert, 24 Blanks 
        12   127      1 Zeichen      Währungskennzeichen 
                                        " " = DM 
                                        "1" = Euro  
        Insgesamt 128 Zeichen 
        '''
        
        data = '0128'
        data = data + 'A'
        data = data + self.typ
        data = data + '%8i' % int(DTAUS['BANK_ICODE']) #BLZ
        data = data + '%08i' % 0                 #belegt, wenn Bank
        data = data + '%-27.27s' % self.convert_text(DTAUS['BANK_ACCOUNT_OWNER'])
        data = data + self.date.strftime('%d%m%y')    #aktuelles Datum im Format DDMMJJ 
        data = data + 4*'\x20'  #bankinternes Feld
        data = data + '%010i' % int(DTAUS['BANK_ACCOUNT'])
        data = data + '%010i' % 0 #Referenznummer
        data = data + 15 * '\x20'   #Reserve
        data = data + '%8s' % ' '   #Ausführungsdatum (ja hier 8 Stellen, Erzeugungsdat. hat 6 Stellen)
        data = data + 24 * '\x20'   #Reserve
        data = data + '1'   #Kennzeichen Euro
        if len(data) <> 128:
            raise Exception('DTAUS: Längenfehler A')
        
        return data.encode("cp437", 'ignore')
    
        
        
                
    def dataC(self, buchung):
        '''
        Erstellen C-Segmente (Buchungen mit Texten) der DTAUS-Datei
        Aufbau:        
        Nr.    St    Länge        Beschreibung 
        1     0      4 Zeichen      Länge des Datensatzes, 187 + x * 29 (x..Anzahl Erweiterungsteile) 
        2     4      1 Zeichen      Datensatz-Typ, immer 'C' 
        3     5      8 Zeichen      Bankleitzahl des Auftraggebers (optional) 
        4     13  8 Zeichen      Bankleitzahl des Kunden 
        5     21  10 Zeichen  Kontonummer des Kunden 
        6     31  13 Zeichen  Verschiedenes 
                    1. Zeichen: "0" 
                    2. - 12. Zeichen: interne Kundennummer oder Nullen 
                    13. Zeichen: "0" 
                    Die interne Nummer wird vom erstbeauftragten Institut zum endbegünstigten Institut weitergeleitet. Die Weitergabe der internenen Nummer an den Überweisungsempfänger ist der Zahlstelle freigestellt.  
        7     44  5 Zeichen  Art der Transaktion (7a: 2 Zeichen, 7b: 3 Zeichen) 
                    "04000" Lastschrift des Abbuchungsauftragsverfahren 
                    "05000" Lastschrift des Einzugsermächtigungsverfahren 
                    "05005" Lastschrift aus Verfügung im elektronischen Cash-System 
                    "05006" Wie 05005 mit ausländischen Karten 
                    "51000" Überweisungs-Gutschrift 
                    "53000" Überweisung Lohn/Gehalt/Rente 
                    "5400J" Vermögenswirksame Leistung (VL) ohne Sparzulage 
                    "5400J" Vermögenswirksame Leistung (VL) mit Sparzulage 
                    "56000" Überweisung öffentlicher Kassen 
                    Die im Textschlüssel mit J bezeichnete Stelle, wird bei Übernahme in eine Zahlung automatisch mit der jeweils aktuellen Jahresendziffer (7, wenn 97) ersetzt.  
        8     49  1 Zeichen  Reserviert, " " (Blank) 
        9     50  11 Zeichen  Betrag 
        10     61  8 Zeichen  Bankleitzahl des Auftraggebers 
        11     69  10 Zeichen  Kontonummer des Auftraggebers 
        12     79  11 Zeichen  Betrag in Euro einschließlich Nachkommastellen, nur belegt, wenn Euro als Währung angegeben wurde (A12, C17a), sonst Nullen 
        13     90  3 Zeichen  Reserviert, 3 Blanks 
        14a 93  27 Zeichen  Name des Kunden 
        14b 120  8 Zeichen  Reserviert, 8 Blanks 
            Insgesamt 128 Zeichen
            
        15 128  27 Zeichen  Name des Auftraggebers 
        16 155  27 Zeichen  Verwendungszweck 
        17a 182  1 Zeichen  Währungskennzeichen 
                    " " = DM 
                    "1" = Euro  
        17b 183  2 Zeichen  Reserviert, 2 Blanks 
        18 185  2 Zeichen  Anzahl der Erweiterungsdatensätze, "00" bis "15" 
        19 187  2 Zeichen  Typ (1. Erweiterungsdatensatz) 
                    "01" Name des Kunden 
                    "02" Verwendungszweck 
                    "03" Name des Auftraggebers  
        20 189  27 Zeichen  Beschreibung gemäß Typ 
        21 216  2 Zeichen  wie C19, oder Blanks (2. Erweiterungsdatensatz) 
        22 218  27 Zeichen  wie C20, oder Blanks 
        23 245  11 Zeichen  11 Blanks 
            Insgesamt 256 Zeichen, kann wiederholt werden (max 3 mal)
        '''
        #Erweiterungsteile für lange Namen...
        erweiterungen = []  #('xx', 'inhalt') xx: 01=Name 02=Verwendung 03=Name
        # 1. Satzabschnitt
        #data1 = '%4i' % ?? #Satzlänge kommt später
        data1 = 'C'
        data1 = data1 + '%08i' % 0  #freigestellt 
        data1 = data1 + '%08i' % buchung['blz'] # BLZ Kunde
        data1 = data1 + '%010i' % buchung['kto'] # Kontonummer Kunde
        data1 = data1 + '%013i' % 0   #interne Kundennummer
        data1 = data1 + self.zahlungsart 
        data1 = data1 + '\x20' #bankintern
        data1 = data1 + '%011i' % 0 #Reserve
        data1 = data1 + '%08i' % int(DTAUS['BANK_ICODE'])
        data1 = data1 + '%010i' % int(DTAUS['BANK_ACCOUNT'])
        data1 = data1 + '%011i' % int(buchung['betrag'] * 100 ) #Betrag in Euroeinschl. Nachkomme
        data1 = data1 + 3 * '\x20'
        # Kunde/Zahlungspflichtiger
        mitglied = self.convert_text(buchung['kontoinhaber'])
        data1 = data1 + '%-27.27s' % mitglied
        #if len(mitglied) > 27:
        #    erweiterungen.append( ('01', mitglied[27:]) )
        data1 = data1 + 8 * '\x20'

        
        self.sum['konto']   = self.sum['konto'] + int('%010i' % buchung['kto'])
        self.sum['blz']     = self.sum['blz'] + int('%08i' % buchung['blz'])
        self.sum['euro']    = self.sum['euro'] + int(buchung['betrag'] * 100 )
        
        
        # 2. Satzabschnitt
        wir = self.convert_text(DTAUS['BANK_ACCOUNT_OWNER'])
        data2 = '%-27.27s' % wir
        #if len(wir) > 27:
        #    erweiterungen.append( ('03', wir[27:]) )
        #Zweck
        zweck1 = self.convert_text(buchung['zweck1'])
        data2 = data2 + '%-27.27s' % zweck1
        zweck2 = self.convert_text(buchung['zweck2'])
        erweiterungen.append( ('02', '%-27.27s' % zweck2) )
        zweck3 = self.convert_text(buchung['zweck3'])
        erweiterungen.append( ('02', '%-27.27s' % zweck3) )
        data2 = data2 + '1'     #Währungskennzeichen
        data2 = data2 + 2 * '\x20'
        # Gesamte Satzlänge ermitteln ( data1(+4) + data2 + Erweiterungen )
        data1 = '%04i' % (len(data1)+4 + len(data2)+2 + len(erweiterungen) * 29 ) + data1
        if len(data1) <> 128:
            raise Exception(u'DTAUS: Längenfehler C / data1: len(' + str(len(data1)) + '), data1(' + data1 + ')')

        #Anzahl Erweiterungen anfügen
        data2 = data2 + '%02i' % len(erweiterungen)  #Anzahl Erweiterungsteile
        #Die ersten zwei Erweiterungen gehen in data2,
        data2 = data2 + '%2.2s%27.27s' % erweiterungen[0]
        data2 = data2 + '%2.2s%27.27s' % erweiterungen[1]
        
        data2 = data2 + 11 * '\x20'
        if len(data2) <> 128:
            raise Exception(u'DTAUS: Längenfehler in C / data2: len(' + str(len(data2)) + '), data2(' + data2 + ')')

        self.anzBuchungen += 1

        return (data1 + data2).encode("cp437", 'ignore')
    
    
    

    def dataE(self):
        '''
        Erstellen E-Segment (Prüfsummen) der DTAUS-Datei
        Aufbau:
        Nr.    Start Länge     Beschreibung 
        1     0      4 Zeichen      Länge des Datensatzes, immer 128 Bytes, also immer "0128" 
        2     4      1 Zeichen      Datensatz-Typ, immer 'E' 
        3     5      5 Zeichen      "     " (Blanks) 
        4     10  7 Zeichen      Anzahl der Datensätze vom Typ C 
        5     17  13 Zeichen  Kontrollsumme Beträge 
        6     30  17 Zeichen  Kontrollsumme Kontonummern 
        7     47  17 Zeichen  Kontrollsumme Bankleitzahlen 
        8     64  13 Zeichen  Kontrollsumme Euro, nur belegt, wenn Euro als Währung angegeben wurde (A12, C17a) 
        9     77  51 Zeichen  51 Blanks 
        Insgesamt 128 Zeichen 
        '''
        data = '0128'
        data = data + 'E'
        data = data + 5 * '\x20'
        data = data + '%07i' % self.anzBuchungen
        data = data + 13 * '0'  #Reserve
        data = data + '%017i' % self.sum['konto']
        data = data + '%017i' % self.sum['blz']
        data = data + '%013i' % self.sum['euro']
        data = data + 51 * '\x20'   #Abgrenzung Datensatz
        if len(data) <> 128:
            raise Exception('DTAUS: Längenfehler E')
        
        return data
