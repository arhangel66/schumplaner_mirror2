# -*- coding: utf-8 -*-
import StringIO
from chardet.universaldetector import UniversalDetector


def clean_not_ascii_chars(text):
    
    dic = {u'Ä':'AE', u'ä':'ae',
           u'Ö':'OE', u'ö':'oe',
           u'Ü':'UE', u'ü':'ue',
           u'ß':'ss',
           u'é':'e', u'à':'a'}
    
    for i, j in dic.iteritems():
        text = text.replace(i, j)
    return text


def encode_uploaded_file_to_utf8(django_uploaded_file):
    """
    Detect file encoding of an uploaded text file and recode it if necessary

    @param InMemoryUploadedFile The uploaded file

    @return StringIO file
    """
    oTempFile = StringIO.StringIO()
    oTempFile.write(django_uploaded_file.read())
    oTempFile.seek(0)

    # check for most used encodings
    lStandardEncodings = ['utf-8', 'windows-1252', 'latin-1']
    sFoundEncoding = ''
    for sEncoding in lStandardEncodings:
        if '' == sFoundEncoding:
            try:
                for line in oTempFile.readlines():
                    line.decode(sEncoding)
                sFoundEncoding = sEncoding
                oTempFile.seek(0)
            except UnicodeDecodeError:
                oTempFile.seek(0)

    # if encoding does not match one out of most used ones: use chardet to detect encoding
    if '' == sFoundEncoding:
        detector = UniversalDetector()
        for line in oTempFile.readlines():
            #line.decode('ascii')
            detector.feed(line)
            if detector.done: break
        detector.close()
        oTempFile.seek(0)
        sFoundEncoding = detector.result['encoding']

    if sFoundEncoding != 'utf-8':
        oRecodedFile = StringIO.StringIO()
        for line in oTempFile.readlines():
            oRecodedFile.write(line.decode(sFoundEncoding).encode('utf-8'))
        oTempFile.close()
        oRecodedFile.seek(0)
        return oRecodedFile
    else:
        return oTempFile

