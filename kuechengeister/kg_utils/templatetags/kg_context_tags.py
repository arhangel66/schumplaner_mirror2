from django.template import Library
from django.conf import settings

register = Library()


@register.assignment_tag
def get_site_context():
    """Makes site context variables available in templates."""
    return getattr(settings, 'SITE_CONTEXT', None)

@register.assignment_tag
def get_site_overload_content_dir():
    """Makes site context variables available in templates."""
    return getattr(settings, 'SITE_OVERLOAD_CONTENT_DIR', None)