# -*- coding: utf-8 -*-
from django import template
from django.template.defaultfilters import stringfilter

register = template.Library()  
  
@register.filter(name='field_type')
def field_type(value):  
    return value.field.__class__.__name__  


@register.filter(name='widget_type')  
def widget_type(value):  
    return value.field.widget.__class__.__name__


@register.filter(name='keyvalue')
def keyvalue(dict, key):
    return dict[key]

@register.filter
@stringfilter
def slicestring(value, arg):
    """usage: "mylongstring"|slicestring:"2:4" """
    els = map(int, arg.split(':'))
    return value[els[0]:els[1]]

@register.filter
def selected_week(all_weeks, selected_week):
    """
    Use to get selected week out of all week list.

    usage: weeks|selected_week:selected """

    week = None

    for item in all_weeks:
        if item['week'] == selected_week:
            week = item

    return week

@register.filter
def next_week(all_weeks, selected_week):
    # get selected week index
    current_week_index = 0

    for idx, item in enumerate(all_weeks):
        if item['week'] == selected_week:
            current_week_index = idx
            break

    # if current week is not last in list return next week
    if current_week_index < len(all_weeks)-1:
        return all_weeks[current_week_index+1]
    else:
        return None

@register.filter
def prev_week(all_weeks, selected_week):
    # get selected week index
    current_week_index = 0

    for idx, item in enumerate(all_weeks):
        if item['week'] == selected_week:
            current_week_index = idx
            break

    # if current week is not last in list return prev week
    if current_week_index > 0:
        return all_weeks[current_week_index-1]
    else:
        return None

@register.filter
def get_day_mealtime(mealtimes, day):
    mealtime = None

    for item in mealtimes:
        if 'menu' in item and item['menu'].date == day:
            mealtime = item
            break

    return mealtime
