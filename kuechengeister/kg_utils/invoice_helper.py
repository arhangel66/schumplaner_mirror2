# -*- coding: utf-8 -*-

from datetime import date

from django.db import transaction
from django.utils.log import getLogger

from kg.models import Order, Reduction, Holiday, Child, Customer,\
                      Facility, FacilityType, InvoiceLog
from menus.models import Menu
from kg_utils.date_helper import DateHelper

logger = getLogger(__name__)


class InvoiceHelper():
    
    @staticmethod
    def calculate_and_update_prices(iChildId, oDateStart=None, oDateEnd=None):
        """ Calculates and updates the prices for the orders of the given child for the given period """
        
        # check input data
        if ( int(iChildId) < 1 or not oDateStart or not oDateEnd ):
            raise Exception

        oChild = Child.objects.get(pk=iChildId)

        for oOrder in Order.objects.filter(child=oChild, menu__date__range=(oDateStart, oDateEnd)):
            try:
                dMenuPrice = oOrder.menu.get_price(child=oChild,
                                                   price_group=oChild.facility_subunit.price_group)
                oOrder.price = dMenuPrice['price']
                oOrder.reduction = None
                if dMenuPrice['reduction_id'] > 0:
                    oOrder.reduction = Reduction.objects.get(pk=dMenuPrice['reduction_id'])
                oOrder.save()
            except Exception as exc:
                logger.exception('Error in setting order prices before creating invoice')
                raise

        return True


    @staticmethod
    def getTaxConsultantCsvHeader():
        return "Status;Rechnungsnummer;Kundennummer;Nachname;NAME AUS ABRECHNUNG;Summe brutto"



    @staticmethod
    def getTaxConsultantCsvContent(invoice_date, invoice_number, invoice_total, child):
        if invoice_total > 0:
            row = invoice_date.strftime("%d.%m.%Y") + ";" + invoice_number + ";" + str(child.cust_nbr) + ";8400;" + child.name + ", " + child.surname + ";" + ("%2.2f" % invoice_total).replace('.', ',')
            return row.encode("latin-1", 'ignore')
        else:
            return ''

