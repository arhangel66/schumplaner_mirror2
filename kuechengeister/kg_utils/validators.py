# -*- coding: utf-8 -*-
import re
from django.core.exceptions import ValidationError
from django.contrib.auth.models import User


def EmailUniqueValidator(value):
    """ Checks that email address is not already used as username """
    from kg.models import Customer  # import here because otherwise validators are not usable in models.py

    if User.objects.filter(username__iexact=value) or Customer.objects.filter(email__iexact=value):
        raise ValidationError(u'Unter dieser E-Mailadresse ist bereits ein Kunde registriert')


def IbanValidator(value):
    """ Validates per Regex for German and Austrian IBANs und official IBAN checksum """

    # TODO: make check for different countries more generic
    from settings import OVERLOAD_SITE

    if OVERLOAD_SITE == 'wimpassing':
        # regex for Austrian IBAN
        if not re.match('^AT[0-9]{2}[A-Z0-9]{16}$', value):
            raise ValidationError(u'IBAN nicht gültig')
    else:
        # regex for German IBAN
        if not re.match('^DE[0-9]{2}[A-Z0-9]{18}$', value):
            raise ValidationError(u'IBAN nicht gültig')

    # official checksum check

    # 1. Move the four initial characters to the end of the string.
    value = value[4:] + value[:4]

    # 2. Replace each letter in the string with two digits, thereby expanding the string, where
    #    A = 10, B = 11, ..., Z = 35.
    value_digits = ""
    for x in value:
        # Check if we can use ord() before doing the official check. This protects against bad character encoding.
        if len(x) > 1:
            raise ValidationError(u'IBAN nicht gültig')

        # The official check.
        ord_value = ord(x)
        if 48 <= ord_value <= 57:  # 0 - 9
            value_digits += x
        elif 65 <= ord_value <= 90:  # A - Z
            value_digits += str(ord_value - 55)
        else:
            raise ValidationError(u'IBAN nicht gültig')

    # 3. Interpret the string as a decimal integer and compute the remainder of that number on division by 97.
    if int(value_digits) % 97 != 1:
        raise ValidationError(u'IBAN nicht gültig')


def BicValidator(value):
    """ Validates BIC per regex according to SFIRM software """
    if not re.match('^[A-Z]{6}[A-Z2-9][A-NP-Z0-9]([A-Z0-9]{3})?$', value):
        raise ValidationError(u'BIC nicht gültig')


# TODO: Use EmailField if upgraded to newer Django version
# Use the EmailValidator from Django 1.9 branch because of new top level domains
class EmailValidator(object):
    message = ('Bitte eine gültige E-Mail-Adresse eingeben.')
    code = 'invalid'
    user_regex = re.compile(
        r"(^[-!#$%&'*+/=?^_`{}|~0-9A-Z]+(\.[-!#$%&'*+/=?^_`{}|~0-9A-Z]+)*\Z"  # dot-atom
        r'|^"([\001-\010\013\014\016-\037!#-\[\]-\177]|\\[\001-\011\013\014\016-\177])*"\Z)',  # quoted-string
        re.IGNORECASE)
    domain_regex = re.compile(
        # max length for domain name labels is 63 characters per RFC 1034
        r'((?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+)(?:[A-Z0-9-]{2,63}(?<!-))\Z',
        re.IGNORECASE)
    literal_regex = re.compile(
        # literal form, ipv4 or ipv6 address (SMTP 4.1.3)
        r'\[([A-f0-9:\.]+)\]\Z',
        re.IGNORECASE)
    domain_whitelist = ['localhost']

    def __init__(self, message=None, code=None, whitelist=None):
        if message is not None:
            self.message = message
        if code is not None:
            self.code = code
        if whitelist is not None:
            self.domain_whitelist = whitelist

    def __call__(self, value):
        #value = force_text(value)

        if not value or '@' not in value:
            raise ValidationError(self.message, code=self.code)

        user_part, domain_part = value.rsplit('@', 1)

        if not self.user_regex.match(user_part):
            raise ValidationError(self.message, code=self.code)

        if (domain_part not in self.domain_whitelist and
                not self.validate_domain_part(domain_part)):
            # Try for possible IDN domain-part
            try:
                domain_part = domain_part.encode('idna').decode('ascii')
                if self.validate_domain_part(domain_part):
                    return
            except UnicodeError:
                pass
            raise ValidationError(self.message, code=self.code)

    def validate_domain_part(self, domain_part):
        if self.domain_regex.match(domain_part):
            return True

        return False

    def __eq__(self, other):
        return (
            isinstance(other, EmailValidator) and
            (self.domain_whitelist == other.domain_whitelist) and
            (self.message == other.message) and
            (self.code == other.code)
        )

validate_email = EmailValidator()
