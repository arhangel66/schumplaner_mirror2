# -*- coding: utf-8 -*-
import os
import random
import hashlib
import math

from datetime import date, timedelta
from decimal import Decimal

from django.template import Context
from django.template.loader import render_to_string
from django.db.models import Count, Sum, Avg
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned

from reports.helpers import generate_pdf
from kg.models import (Customer, CustomerAccountTransaction, Order, Reduction, InvoiceLog,
                       PayType, SepaMandate, FacilityType)
from kg_utils.invoice_helper import InvoiceHelper
from kg_utils.date_helper import DateHelper
from settings import (INVOICE_ROOT, MODULE_ACCOUNTING, MODULE_PREPAID, MODULE_PAPER_INVOICE,
                      PAPER_INVOICE_SETTINGS, SITE_CONTEXT, SEPA, SITE_OVERLOAD_STATICFILES_DIR, OVERLOAD_SITE)


class InvoiceGenerator:
    COL_ORDERS = 45

    def __init__(self, child, year, month, context):
        self.child = child
        self.customer = Customer.objects.get(child=self.child)
        self.facility = self.child.facility.name + u' - ' + self.child.facility_subunit.name
        self.facility_name_short = self.child.facility.name_short
        self.month = int(month)
        self.year = int(year)
        self.from_date = date(year, month, 1)
        self.to_date = DateHelper.getLastDayOfMonth(self.from_date)
        self.creation_date = self.to_date
        self.orders = []
        self.invoice_items = []
        self.additionals = []
        self.subtotal = 0
        self.subtotal_netto = 0
        self.total = 0
        self.tax_rate = self.child.facility_subunit.tax_rate
        self.tax = 0
        self.pay_type_id = 0
        self.pay_type = ''
        self.pay_until = ''
        self.invoice_number = ''
        self.bank_account_iban = ''
        self.sepa_mandate = None
        self.sepa_mandate_id = ''
        self.sepa_transaction_id = ''
        self.context = context
        self.is_new_invoice = True  # set to False if invoice already exists and is already released

        # check if invoice already exists
        # load some values in that case, set new values otherwise
        try:
            existing_invoice = InvoiceLog.objects.get(child=self.child, invoice_date=self.to_date)

            # Set values for EXISTING invoice
            self.invoice_number = existing_invoice.invoice_number
            self.sepa_transaction_id = existing_invoice.sepa_transaction_id

            # if invoice was already released take existing pay_type and pay_until date
            # otherwise calculate them new and set sepa mandate
            if existing_invoice.is_released():
                self.is_new_invoice = False
                self.__set_paytype(existing_invoice.pay_type_id)
                self.pay_until = existing_invoice.pay_until
                self.bank_account_iban = existing_invoice.bank_account_iban
                self.sepa_mandate_id = existing_invoice.sepa_mandate_reference
            else:
                # IMPORTANT: do not change order of function calls
                self.__set_paytype(self.customer.pay_type_id)
                self.__set_sepa_mandate()
                self.__set_pay_until(self.pay_type_id)
        except ObjectDoesNotExist:
            # Set values for NEW invoice
            # IMPORTANT: do not change order of function calls
            self.__create_invoicenumber()
            self.__create_sepa_transaction_id()
            self.__set_paytype(self.customer.pay_type_id)
            self.__set_sepa_mandate()
            self.__set_pay_until(self.pay_type_id)

        InvoiceHelper.calculate_and_update_prices(iChildId=self.child.id,
                                                  oDateStart=self.from_date,
                                                  oDateEnd=self.to_date)

        self.__populate_invoice()
        self.__calculate_total()
        self.__calculate_tax()

    def __populate_invoice(self):

        qsOrders = (
            Order.objects.filter(
                child=self.child,
                menu__date__range=(self.from_date, self.to_date)
            ).values('menu__meal__name', 'menu__date',
                     'price', 'quantity', 'reduction')
            .order_by('menu__date', 'menu__meal__sortKey'))

        oOrdersTotal = 0
        for dOrder in qsOrders:
            dRow = {'text': dOrder['menu__meal__name'],
                    'reduction': dOrder['reduction'] is not None,
                    'quantity': dOrder['quantity'],
                    'price': dOrder['price'],
                    'date': dOrder['menu__date'],
                    }
            self.orders.append(dRow)
            oOrdersTotal += dOrder['quantity'] * dOrder['price']

        self.invoice_items.append({'text': 'Essenversorgung lt. Einzelauflistung',
                                   'price': oOrdersTotal})

        # additional fee for Muehlenmenue Hort children
        if OVERLOAD_SITE == 'muehlenmenue':
            if self.child.extra_attribute == 'Hort':
                if self.year == 2017 and self.month == 10:
                    self.invoice_items.append(
                        {'text': u'Obst-, Gemüse-, Getränkepauschale Hort (September 2017)',
                         'price': Decimal('3.00')})
                self.invoice_items.append({'text': u'Obst-, Gemüse-, Getränkepauschale Hort',
                                           'price': Decimal('3.00')})

        # manually correct some invoices
        if OVERLOAD_SITE == 'sonnenblick':
            if self.year == 2017 and self.month == 7:
                if self.child.id == 12:
                    self.invoice_items.append({'text': 'Gutschrift	3xBuchung am 08.06.2017',
                                               'price': Decimal('-9.51')})
            if self.year == 2017 and self.month == 8:
                if self.child.id == 28:
                    self.invoice_items.append({'text': 'Gutschrift Doppelbuchung 17.07.2017',
                                               'price': Decimal('-3.17')})
        if OVERLOAD_SITE == 'oschatz':
            if self.child.id == 128:
                self.invoice_items.append({'text': 'Servicepauschale',
                                           'price': Decimal('5.00')})

        self.__add_paper_fee()
        self.__populate_additionals()

    def __add_paper_fee(self):
        """add fee for paper invoice if MODULE_PAPER_INVOICE is activated
        and customer has no online invoice
        """
        if MODULE_PAPER_INVOICE:
            if self.customer.paper_invoice:
                data = {'text': PAPER_INVOICE_SETTINGS['INVOICE_TEXT'],
                        'price': Decimal(PAPER_INVOICE_SETTINGS['INVOICE_FEE'])}
                self.invoice_items.append(data)

                # make a corresponding booking into the customer account if MODULE_PREPAID is active
                if MODULE_PREPAID:
                    oPaperFeeCAT = CustomerAccountTransaction(
                                       customer=self.customer,
                                       amount=Decimal(PAPER_INVOICE_SETTINGS['INVOICE_FEE']).copy_negate(),
                                       description=u'Rechnung %s in Papierform' % self.invoice_number,
                                       type=CustomerAccountTransaction.TYPE_CHARGE,
                                       transaction_date=self.to_date,
                                       status=CustomerAccountTransaction.STATUS_OPEN,
                                       invoice_id=self.invoice_number
                                   )
                    oPaperFeeCAT.save()

    def __populate_additionals(self):
        """use to add certain additional positions to the invoice"""

        if OVERLOAD_SITE == 'leibundseele':
            # add chip deposit for children with unpaid chip deposit
            if self.child.rfid_tag != '' and not self.child.chip_deposit:
                self.additionals.append({'text': 'Pfand Chip', 'price': Decimal('5.00')})
                self.child.chip_deposit = True
                self.child.save()

            if self.year == 2017 and self.month == 5:
                if self.child.id == 345:
                    self.additionals.append({'text': 'Erstattung Chippfand', 'price': Decimal('-5.00')})
                if self.child.id == 129:
                    self.additionals.append({'text': 'Erstattung Chippfand', 'price': Decimal('-5.00')})
                if self.child.id == 1136:
                    self.additionals.append({'text': 'Erstattung Chippfand', 'price': Decimal('-5.00')})
                if self.child.id == 1130:
                    self.additionals.append({'text': 'Erstattung Chippfand', 'price': Decimal('-5.00')})
                if self.child.id == 1486:
                    self.additionals.append({'text': 'Erstattung Chippfand', 'price': Decimal('-5.00')})

            if self.year == 2017 and self.month == 6:
                # Andersen
                if self.child.cust_nbr in [11163, 10710, 10943, 10601, 10622, 10849, 10680, 10691, 10620, 10686, 10653, 10652, 10881, 10599, 10636, 10586, 10610, 10580, 10843, 10608, 10585, 10655, 10661, 10895]:
                    self.additionals.append({'text': 'Erstattung Chippfand', 'price': Decimal('-5.00')})
                # LBZ Halberstadt
                if self.child.cust_nbr in [10818, 10671, 10505, 10496, 10508, 10776, 10507, 10503, 10618, 10526, 10777, 10600]:
                    self.additionals.append({'text': 'Erstattung Chippfand', 'price': Decimal('-5.00')})
                # GS Neumarkt
                if self.child.cust_nbr in [10150, 10121, 10223, 10010, 10148, 10011, 11162, 10109, 10336, 10278, 10210, 10263, 10068, 10185, 10152, 10402, 10327, 11412, 10041, 10097, 10302, 10260, 10081, 10362, 10031, 10959, 10125, 10023, 10224, 10219, 10092, 10071, 10067, 10332, 10312, 10369, 10389, 10346, 11461, 10051, 10316, 10366, 10079, 10337, 10373, 10276, 10216, 10066, 11170, 10093, 11451, 10145, 10204, 10059, 10119, 10401, 10018]:
                    self.additionals.append({'text': 'Erstattung Chippfand', 'price': Decimal('-5.00')})
                # doppelte
                if self.child.cust_nbr in [10585, 10600, ]:
                    self.additionals.append({'text': 'Erstattung Chippfand', 'price': Decimal('-5.00')})

            if self.year == 2017 and self.month == 7:
                # GS Neumarkt
                if self.child.cust_nbr in [11132, 10179, 10125]:
                    self.additionals.append({'text': 'Erstattung Chippfand', 'price': Decimal('-5.00')})
                if self.child.cust_nbr in [10125,]:
                    self.additionals.append({'text': 'Erstattung Chippfand', 'price': Decimal('-5.00')})


    def __set_paytype(self, pay_type_id):
        """
        Set paytype id and paytype for use in template
        """
        self.pay_type_id = pay_type_id

        if MODULE_PREPAID:
                self.pay_type_id = PayType.CASH
                self.pay_type = 'PREPAID'
        else:
            if pay_type_id is PayType.DEBIT:
                self.pay_type = 'DEBIT'

            if pay_type_id is PayType.INVOICE:
                self.pay_type = 'INVOICE'

    def __set_pay_until(self, pay_type_id):
        """
        Checks if it is first or recurrent invoice and set sequence type and pay until date according
        """
        if MODULE_PREPAID:
            # set today for prepaid: invoice is already paid
            self.pay_until = date.today()

        elif pay_type_id is PayType.DEBIT:

            # calculate debit date
            if str(SEPA['SEPA_DEBIT_FRST_DAYS']).endswith('.'):
                frst_debit_date = date(date.today().year, date.today().month, int(SEPA['SEPA_DEBIT_FRST_DAYS'][0:-1]))
            else:
                frst_debit_date = DateHelper.getDateByAddingBusinessdays(date.today(), int(SEPA['SEPA_DEBIT_FRST_DAYS']))
            if str(SEPA['SEPA_DEBIT_RCUR_DAYS']).endswith('.'):
                rcur_debit_date = date(date.today().year, date.today().month, int(SEPA['SEPA_DEBIT_RCUR_DAYS'][0:-1]))
            else:
                rcur_debit_date = DateHelper.getDateByAddingBusinessdays(date.today(), int(SEPA['SEPA_DEBIT_RCUR_DAYS']))

            if self.sepa_mandate.last_debit_sequence == '':
                # first debit
                self.pay_until = frst_debit_date
                self.sepa_seq_type = 'FRST'
            elif self.sepa_mandate.last_debit_sequence == 'RCUR':
                # recurrent debit
                self.pay_until = rcur_debit_date
                self.sepa_seq_type = 'RCUR'
            elif self.sepa_mandate.last_debit_sequence == 'FRST':
                if rcur_debit_date > self.sepa_mandate.last_debit_date:
                    # recurrent debit
                    self.pay_until = rcur_debit_date
                    self.sepa_seq_type = 'RCUR'
                else:
                    # set as first debit
                    self.pay_until = frst_debit_date
                    self.sepa_seq_type = 'FRST'

        else:
            self.pay_until = date.today() + timedelta(days=int(SITE_CONTEXT['INVOICE_PAYMENT_PERIOD_IN_DAYS']))

    def __set_sepa_mandate(self):
        """
        Retrieves and sets actual valid sepa mandate
        Raise Exception if no valid sepa mandate was found
        """
        # TODO: Fehler in message queue schreiben und an Nutzer ausgeben
        if self.pay_type_id == PayType.DEBIT:
            try:
                self.sepa_mandate = SepaMandate.objects.get(customer=self.customer, is_valid=True)
                self.bank_account_iban = self.sepa_mandate.iban  # set IBAN for use in template
                self.sepa_mandate_id = self.sepa_mandate.mandate_id  # set SEPA mandate id for use in template
            except ObjectDoesNotExist:
                message = u"Kein gültiges SEPA Mandat gefunden. Kunde: %s" % self.customer
                raise Exception(message)
            except MultipleObjectsReturned:
                message = u"Mehr als ein gültiges SEPA Mandat gefunden. Kunde: %s" % self.customer
                raise Exception(message)

    def __create_sepa_transaction_id(self):
        """
        Create the EndToEndID for the sepa payment
        The ID is always 32 charakters long

        @return the Id.
        """
        sRnd = str(random.random())
        sRnd = hashlib.md5(sRnd).hexdigest()

        sSepaName = SEPA['SEPA_EREF_PREFIX']
        if len(sSepaName) > 15:
            sSepaName = sSepaName[0:15]

        self.sepa_transaction_id = sSepaName + sRnd[0:32-len(sSepaName)]
        return True

    def __calculate_total(self):
        for row in self.invoice_items:
            self.subtotal = self.subtotal + row['price']
        self.total = self.subtotal
        for row in self.additionals:
            self.total = self.total + row['price']
        return True

    def __calculate_tax(self):
        self.tax = self.subtotal - (self.subtotal / (Decimal(100 + self.tax_rate) / Decimal(100)))
        self.subtotal_netto = self.subtotal - self.tax
        return True

    def __create_invoicenumber(self):
        # if invoice number was already created use existing invoice number, otherwise create a new number
        if self.invoice_number == '':
            self.invoice_number = self.to_date.strftime("%y%m") + str(self.child.cust_nbr)
            self.invoice_number = self.invoice_number.upper()
        return True

    def get_invoice_html(self):
        
        #if self.subtotal == 0:
        #    return ''

        # add site context
        dContext = Context(self.context)
        dContext.update(SITE_CONTEXT)
        dContext.update(SEPA)
        dContext['TAX_RATE'] = self.tax_rate

        orders_per_page = self.COL_ORDERS*2
        order_pages = range(
            int(math.ceil(float(len(self.orders)) / orders_per_page))
        )
        order_ranges = []
        for p in order_pages:
            page_start = p*orders_per_page
            if len(self.orders) - page_start <= orders_per_page:
                iColOrders = int(math.ceil(float((len(self.orders) - page_start)) / 2))
            else:
                iColOrders = self.COL_ORDERS
            col_1 = '%d:%d' % (page_start, page_start+iColOrders)
            col_2 = '%d:%d' % (
                page_start+iColOrders,
                page_start+orders_per_page)
            order_ranges.append((col_1, col_2))

        return render_to_string(
            'kg/utils_invoice.html',
            {
                'invoice': self,
                'order_pages': range(len(self.invoice_items) / (2 * self.COL_ORDERS)),
                'order_ranges': order_ranges,
            },
            context_instance=dContext
        )

    def save_invoice(self):
        """
        Writes the invoice to filesystem and create accordings entry in the invoice_log table
        """

        # save invoice only if subtotal is greater than zero
        #if self.subtotal <= 0:
        #return False
        
        # create directory and filename
        rel_dir = str(self.year) + "/" + "%02d" % self.month
        abs_dir = INVOICE_ROOT + "/" + rel_dir
        
        # check if invoice_dir exists, if not create it
        if not os.path.exists(abs_dir):
            os.makedirs(abs_dir)

        # prepare context
        dContext = {
            'invoice_html': self.get_invoice_html(),
            'invoice': self,
            'STATICFILES_DIR': SITE_OVERLOAD_STATICFILES_DIR
        }
        # load extra context
        dContext.update(SITE_CONTEXT)
        dContext.update(SEPA)

        # save invoice
        filename = rel_dir + u'/Rechnung_' + str(self.year) + u'_' + "%02d" % self.month + "_" + self.invoice_number + u'.pdf'
        filename = filename.encode("ascii", 'ignore') 
        fileObj = open(INVOICE_ROOT + "/" + filename, "w")
        fileObj = generate_pdf(
            'kg/pdf/invoice.html', file_object=fileObj, context=dContext)
        fileObj.close()
        
        # create/update InvoiceLog
        oLog, bCreated = InvoiceLog.objects.get_or_create(child=self.child, invoice_date=self.to_date)

        oLog.invoice_file = filename
        oLog.invoice_total = self.total
        if bCreated or self.is_new_invoice:
            oLog.invoice_number = self.invoice_number
            oLog.pay_type_id = self.pay_type_id
            oLog.pay_until = self.pay_until
            oLog.facility = self.child.facility
            if self.pay_type_id == PayType.DEBIT:
                oLog.bank_account_owner = self.sepa_mandate.name
                oLog.bank_account_iban = self.sepa_mandate.iban
                oLog.bank_account_bic = self.sepa_mandate.bic
                oLog.sepa_mandate_reference = self.sepa_mandate.mandate_id
                oLog.sepa_transaction_id = self.sepa_transaction_id
                oLog.sepa_mandate_date = self.sepa_mandate.created_at
                oLog.sepa_description = ("Essengeldabrechnung %02d/%04d RG %s %s %s" %
                                         (self.month, self.year, self.invoice_number,
                                          self.child.surname, self.child.name))[:140]
                oLog.sepa_seq_type = self.sepa_seq_type
        oLog.save()

        # set last debit date to sepa mandate if new invoice
        if self.is_new_invoice and self.pay_type_id == PayType.DEBIT:
            self.sepa_mandate.last_debit_date = self.pay_until
            self.sepa_mandate.last_debit_sequence = self.sepa_seq_type
            self.sepa_mandate.save()

        # create/update CustomerAccountTransaction
        if MODULE_ACCOUNTING and not MODULE_PREPAID:
            try:
                oCustomerAccountTransaction = CustomerAccountTransaction.objects.get(customer=self.customer,
                                                                                     transaction_id=self.invoice_number)
            except CustomerAccountTransaction.DoesNotExist:
                oCustomerAccountTransaction = CustomerAccountTransaction(customer=self.customer,
                                                                         transaction_id=self.invoice_number)
            oCustomerAccountTransaction.amount = -self.total
            oCustomerAccountTransaction.transaction_date = date.today()
            oCustomerAccountTransaction.description = (self.invoice_number + ' vom ' +
                                                       self.to_date.strftime("%d.%m.%Y") +
                                                       ' - %s, %s' % (self.child.name, self.child.surname))
            oCustomerAccountTransaction.type = CustomerAccountTransaction.TYPE_INVOICE
            oCustomerAccountTransaction.save()

        return True

