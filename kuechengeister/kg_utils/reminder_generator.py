# -*- coding: utf-8 -*-

from __future__ import with_statement
import os
from datetime import date, timedelta
from decimal import Decimal

from django.db import transaction
from django.template.loader import render_to_string
from django.db.models import Sum
from django.template import RequestContext

from reports.helpers import generate_pdf
from kg.models import CustomerAccountTransaction, Reminder, InvoiceLog
from settings import SITE_CONTEXT, SEPA, ACCOUNTING_SETTINGS, REMINDER_ROOT, SITE_OVERLOAD_STATICFILES_DIR


class ReminderGenerator:

    def __init__(self, customer, request, reminder=None):
        self.oCustomer = customer
        self.oReminder = None
        self.oLastReminder = reminder
        self.lData = []
        self.oSubTotal = None
        self.oReminderFee = Decimal('0.00')
        self.qsCustomerAccountTransactions = None
        self.oRequest = request

    def createReminder(self):
        """
        Creates the reminder and saves it

        @return boolean True if reminder is created, False otherwise
        """
        # no action if last reminder is from type 3
        if self.oLastReminder and self.oLastReminder.type == Reminder.TYPE_THIRD:
            return False

        # no action if last reminder pay until date is not reached
        if self.oLastReminder and self.oLastReminder.pay_until > date.today():
            return False

        # no action if reminder has children
        if self.oLastReminder and self.oLastReminder.children.exists():
            return False

        with transaction.commit_manually():
            try:
                self._prepareReminder()
                self._saveReminder()
            except Exception as exc:
                transaction.rollback()
                #print exc.message
                return False
            else:
                transaction.commit()
                return True


    def _prepareReminder(self):

        if not self.oLastReminder:
            # prepare query to retrieve all open CustomerAccountTransactions in reminder date range
            qsReminderCATs = (CustomerAccountTransaction.objects.open().filter(customer=self.oCustomer, reminder_id=0)
                                                                       .order_by('transaction_date'))
            # exclude invoice CATs that have not reached the payment until date
            for oCat in qsReminderCATs:
                if oCat.type == CustomerAccountTransaction.TYPE_INVOICE:
                    qsInvoices = InvoiceLog.objects.filter(invoice_number=oCat.transaction_id)
                    if qsInvoices and qsInvoices[0].pay_until >= date.today():
                        qsReminderCATs = qsReminderCATs.exclude(pk=oCat.pk)

            self.qsCustomerAccountTransactions = qsReminderCATs

            oPayUntilDate = date.today() + timedelta(days=int(ACCOUNTING_SETTINGS['REMINDER_PAYMENT_PERIOD']))
            iReminderType = Reminder.TYPE_FIRST

        else:
            # retrieve reminder CATs without reminder fee
            qsReminderCATs = (CustomerAccountTransaction.objects.filter(customer=self.oCustomer,
                                                                        reminder_id=self.oLastReminder.id)
                                                                .exclude(type=CustomerAccountTransaction.TYPE_REMINDER_FEE)
                                                                .order_by('-transaction_date'))
            self.qsCustomerAccountTransactions = qsReminderCATs

            if self.oLastReminder.type == Reminder.TYPE_FIRST:
                oPayUntilDate = date.today() + timedelta(days=int(ACCOUNTING_SETTINGS['REMINDER_PAYMENT_PERIOD_2']))
                iReminderType = Reminder.TYPE_SECOND
            if self.oLastReminder.type == Reminder.TYPE_SECOND:
                oPayUntilDate = date.today() + timedelta(days=int(ACCOUNTING_SETTINGS['REMINDER_PAYMENT_PERIOD_3']))
                iReminderType = Reminder.TYPE_THIRD

        # calculate total
        dAggrResult = qsReminderCATs.aggregate(total=Sum('amount'))
        if dAggrResult['total'] and dAggrResult['total'] < 0:
            self.oSubTotal = -dAggrResult['total']
        else:
            # no reminder creation if total is not positive
            raise Exception('The total of the reminder has to be greater than zero.')

        # create new Reminder and save it to get the new id
        self.oReminder = Reminder.objects.create(customer=self.oCustomer,
                                                 parent=self.oLastReminder,
                                                 create_date=date.today(),
                                                 pay_until=oPayUntilDate,
                                                 type=iReminderType)

        # create data for reminder
        for oCAT in qsReminderCATs:
            if oCAT.type is CustomerAccountTransaction.TYPE_INVOICE:
                self.lData.append({'description': 'Rechnung ' + oCAT.description,
                                   'amount': -oCAT.amount})
            elif oCAT.type is CustomerAccountTransaction.TYPE_BANK_TRANSFER_IN:
                self.lData.append({'description': 'Überweisungseingang vom %s' % oCAT.transaction_date.strftime('%d.%m.%Y'),
                                   'amount': -oCAT.amount})
            elif oCAT.type is CustomerAccountTransaction.TYPE_CASH:
                self.lData.append({'description': 'Bareinzahlung vom %s' % oCAT.transaction_date.strftime('%d.%m.%Y'),
                                   'amount': -oCAT.amount})
            else:
                self.lData.append({'description': oCAT.description,
                                  'amount': -oCAT.amount})

        # add reminder fee
        if self.oLastReminder:
            if self.oLastReminder.type == Reminder.TYPE_FIRST:
                self.oReminderFee = Decimal(str(ACCOUNTING_SETTINGS['REMINDER_FEE_2']))
            if self.oLastReminder.type == Reminder.TYPE_SECOND:
                self.oReminderFee = Decimal(str(ACCOUNTING_SETTINGS['REMINDER_FEE_3']))
        else:
            self.oReminderFee = Decimal(str(ACCOUNTING_SETTINGS['REMINDER_FEE']))

        if self.oReminderFee > 0:
            self.oReminder.total = self.oSubTotal + self.oReminderFee

            if not self.oLastReminder:
                # create CustomerAccountTransaction for reminder fee if it not still exists
                CustomerAccountTransaction.objects.create(customer=self.oCustomer,
                                                          amount=self.oReminderFee.copy_negate(),
                                                          description=u'Mahnkosten der Mahnung vom %s' % date.today().strftime('%d.%m.%Y'),
                                                          type=CustomerAccountTransaction.TYPE_REMINDER_FEE,
                                                          transaction_date=date.today(),
                                                          reminder_id=self.oReminder.id)
            else:
                # retrieve existing reminder fee CAT and adjust date, description, amount
                oNewReminderFeeCAT, bCreated = CustomerAccountTransaction.objects.get_or_create(customer=self.oCustomer,
                                                                                                type=CustomerAccountTransaction.TYPE_REMINDER_FEE,
                                                                                                reminder_id=self.oLastReminder.id)
                oNewReminderFeeCAT.amount = self.oReminderFee.copy_negate()
                oNewReminderFeeCAT.description = u'Mahnkosten der Mahnung vom %s' % date.today().strftime('%d.%m.%Y')
                oNewReminderFeeCAT.transaction_date = date.today()
                oNewReminderFeeCAT.reminder_id = self.oReminder.id
                oNewReminderFeeCAT.save()

        else:
            self.oReminder.total = self.oSubTotal

        # set reminder number
        self.oReminder.number = 'M' + str(100000 + self.oReminder.id)

        # set content
        self.oReminder.content = self._getReminderHtml()

    def _getReminderHtml(self):

        # add site context
        dContext = RequestContext(self.oRequest)
        dContext.update(SITE_CONTEXT)
    
        return render_to_string('kg/utils_reminder.html', {'reminder': self.oReminder,
                                                           'last_reminder': self.oLastReminder,
                                                           'subtotal': self.oSubTotal,
                                                           'reminder_fee': self.oReminderFee,
                                                           'customer': self.oCustomer,
                                                           'data': self.lData}, context_instance= dContext)

    def _saveReminder(self):
        
        # save reminder only if total is greater than zero
        if self.oReminder.total <= 0:
            raise Exception('The total of the reminder has to be greater than zero.')

        # check if reminder_dir exists, if not create it
        if not os.path.exists(REMINDER_ROOT):
            os.makedirs(REMINDER_ROOT)

        # prepare context
        dContext = {'reminder': self.oReminder,
                    'customer': self.oCustomer,
                    'STATICFILES_DIR': SITE_OVERLOAD_STATICFILES_DIR}
        # load site site specific context
        dContext.update(SITE_CONTEXT)
        dContext.update(SEPA)

        # save invoice
        filename = u'Mahnung_' + str(self.oReminder.number) + u'.pdf'
        filename = filename.encode("ascii", 'ignore')
        fileObj = open(REMINDER_ROOT + "/" + filename, "w")
        fileObj = generate_pdf(
            'kg/pdf/reminder.html', file_object=fileObj, context=dContext)
        fileObj.close()

        # update Reminder
        self.oReminder.filename = filename
        self.oReminder.save()

        # update CustomerAccountTransactions
        self.qsCustomerAccountTransactions.update(reminder_id=self.oReminder.id)
