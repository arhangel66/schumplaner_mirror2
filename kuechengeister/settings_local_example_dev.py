# -*- coding: utf-8 -*-

"""
 The confidential site specific settings

 Overrides the site wide settings
 Sets all the confidential site specific settings
 The settings that are not confidential are in /site_overloads/OVERLOAD_SITE/settings.py
"""

import os
from settings import *

# The name of the directory the site overloads are in

OVERLOAD_SITE = 'demo'


# E-Mail settings
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'  # writes email to stdout instead of sending
#EMAIL_BACKEND = 'django.core.mail.backends.dummy.EmailBackend'  # dummy email backend; does nothing


# Database settings
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': OVERLOAD_SITE,
        'USER': 'root',
        'PASSWORD': 'root',
        'HOST': 'localhost',
        'PORT': '5432'
    }
}


#####  Settings for local development  #####


# Debugging
DEBUG = True
TEMPLATE_DEBUG = True

# Disable Raven
RAVEN_CONFIG = {}

# Customize INVOICE_ROOT for local development
INVOICE_ROOT = os.path.join(PROJECT_ROOT, '../usercontent/invoices')

# Customize REMINDER_ROOT for local development
REMINDER_ROOT = os.path.join(PROJECT_ROOT, '../usercontent/reminders')

# Debug Toolbar
DEBUG_APPS =  ('debug_toolbar',)
