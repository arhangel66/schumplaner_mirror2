from django.contrib import admin
from django import forms
from django.forms.util import ErrorList

from django.utils.translation import ugettext_lazy as _

from models import Absence


class AbsenceForm(forms.ModelForm):
    """Custom admin form for the Absence model."""
    def clean(self):
        data = self.cleaned_data
        # Make sure that the date_to is equal or greater than the date_from.
        dates_present = data.get('date_from') and data.get('date_to')
        if dates_present and data['date_from'] > data['date_to']:
            msg = _('date_to must be equal or greater than date_from')
            self._errors["date_to"] = ErrorList([msg])
        return self.cleaned_data


class AbsenceAdmin(admin.ModelAdmin):
    """Admin class for Absence with custom validation."""
    model = Absence
    form = AbsenceForm
    list_display = ('date_from', 'date_to', 'child', 'notice')
    ordering = ['-date_from']


admin.site.register(Absence, AbsenceAdmin)
