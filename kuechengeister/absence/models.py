# -*- coding: utf-8 -*-
from django.db import models
from django.utils.timezone import now


class Absence(models.Model):
    """Model for the single absence of a child. It could for a day or more."""
    child = models.ForeignKey('kg.child', verbose_name=u'Kind')
    date_from = models.DateField(verbose_name=u'Datum von')
    date_to = models.DateField(
        verbose_name=u'Datum bis', blank=True, null=True)
    notice = models.TextField(verbose_name=u'Mitteilung')

    class Meta:
        ordering = ['-date_from', 'child__name', 'child__surname']
        verbose_name = "Abwesenheit"
        verbose_name_plural = "Abwesenheit"

    def __unicode__(self):
        return '%s (%s - %s) ' % (self.child, self.date_from, self.date_to)

    def save(self, *args, **kwargs):
        """If date_to is missing, it will take the date_from."""
        if not self.date_to and self.date_from is not None:
            self.date_to = self.date_from
        return super(Absence, self).save(*args, **kwargs)

    @property
    def editable(self):
        return now().date() < self.date_from
