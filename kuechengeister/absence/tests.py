# -*- coding: utf-8 -*-
from datetime import date, timedelta

from django.test import TestCase
from django.test.utils import override_settings
from django.conf import settings
from django.utils.timezone import now
from django.core.urlresolvers import reverse

from kg.tests import KundenTestCaseMixin

from absence import views
from absence.forms import AbsenceForm

CURRENT_MODULE_ABSENCE = settings.MODULE_ABSENCE


@override_settings(MODULE_ABSENCE=True)
class CustomerAbsenceTestCase(KundenTestCaseMixin, TestCase):

    fixtures = [
        'tests/user.json', 'tests/menus.json', 'tests/kg.json',
        'tests/absence.json'
    ]
    view_url = reverse('absence:report')

    def test_normal_view(self):
        self.login()
        res = self.client.get(self.view_url)
        for key in ['form', 'object_list', 'page_obj', 'is_paginated']:
            self.assertIn(key, res.context)
        self.assertNotEqual(res.context['form'], None)
        self.assertEqual(len(res.context['object_list']), 10)
        self.assertEqual(res.context['is_paginated'], True)

    def test_create_absence(self):
        self.login()
        date_from = now().date() + timedelta(days=365)
        data = {
            'child': 312,
            'date_from': date_from,
            'notice': 'Just a test record',
        }
        form = AbsenceForm(data=data)
        self.assertEqual(form.is_valid(), True)

        response = self.client.post(self.view_url, data)
        self.assertEqual(response.status_code, 302)

        # It should be found as it's the 3rd row in the table
        response = self.client.get(self.view_url)
        new_record_found = False
        for record in response.context['object_list']:
            if record.date_from == date_from and \
               record.date_to == date_from and \
               record.notice == 'Just a test record':
                new_record_found = True
                break
        self.assertEqual(new_record_found, True)

    def test_update(self):
        self.login()
        self.client.get(reverse('absence:report', kwargs={'pk': 11}))
        date_from = now().date() + timedelta(days=365)
        data = {
            'child': 312,
            'date_from': date_from,
            'notice': 'Just a test record',
        }
        form = AbsenceForm(data=data)
        self.assertEqual(form.is_valid(), True)

        response = self.client.post(self.view_url, data)
        self.assertEqual(response.status_code, 302)

        # It should be found as it's the 3rd row in the table
        response = self.client.get(self.view_url)
        new_record_found = False
        for record in response.context['object_list']:
            if record.date_from == date_from and \
               record.date_to == date_from and \
               record.notice == 'Just a test record':
                new_record_found = True
                break
        self.assertEqual(new_record_found, True)

    def test_overlaping_absence(self):
        self.login()
        # Overlapping range
        data = {
            'child': 312,
            'date_from': date(2116, 10, 11),
            'date_to': date(2116, 10, 16),
            'notice': 'Just a test overlapping record',
        }
        form = AbsenceForm(data=data)
        self.assertEqual(form.is_valid(), False)
        response = self.client.post(self.view_url, data)
        self.assertNotEqual(response.context['form'].errors, None)

    def test_submit_past_date(self):
        self.login()
        # Overlapping range
        data = {
            'child': 312,
            'date_from': date(2011, 10, 13),
            'date_to': date(2011, 10, 16),
            'notice': 'Test past date',
        }
        form = AbsenceForm(data=data)
        self.assertEqual(form.is_valid(), False)
        response = self.client.post(self.view_url, data)
        self.assertIn('date_from', response.context['form'].errors)

    def test_empty_notice(self):
        self.login()
        # Overlapping range
        data = {
            'child': 312,
            'date_from': date(2020, 10, 13),
            'date_to': date(2020, 10, 16),
            'notice': '',
        }
        form = AbsenceForm(data=data)
        self.assertEqual(form.is_valid(), False)
        response = self.client.post(self.view_url, data)
        self.assertIn('notice', response.context['form'].errors)


class DisabledAbsenceTestCase(KundenTestCaseMixin, TestCase):
    view_url = reverse('absence:report')
    fixtures = [
        'tests/user.json', 'tests/menus.json', 'tests/kg.json',
        'tests/absence.json'
    ]

    def test_anonymous_access(self):
        pass

    def setUp(self):
        self.current_state = views.MODULE_ABSENCE
        views.MODULE_ABSENCE = False

    def tearDown(self):
        views.MODULE_ABSENCE = self.current_state

    def test_customer_access(self):
        self.login()
        res = self.client.get(self.view_url)
        self.assertEqual(res.status_code, 404)
