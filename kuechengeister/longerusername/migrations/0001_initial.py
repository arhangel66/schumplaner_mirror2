# encoding: utf-8
from south.db import db
from south.v2 import SchemaMigration
from django.db import models
from longerusername import USERNAME_MAXLENGTH

class Migration(SchemaMigration):

    def forwards(self, orm):
        # Changing field 'User.username'
        db.alter_column('auth_user', 'username', models.CharField(max_length=USERNAME_MAXLENGTH))


    def backwards(self, orm):

        # Changing field 'User.username'
        db.alter_column('auth_user', 'username', models.CharField(max_length=30))


    models = {
        
    }

    complete_apps = ['longerusername']