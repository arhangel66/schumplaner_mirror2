# -*- coding: utf-8 -*-

from datetime import date, datetime

from django.core.management.base import BaseCommand

from kg_utils import ucsv as csv
from kg.models import User, Customer, Child, Facility, FacilitySubunit, SepaMandate, PayType



class Command(BaseCommand):
    help = "Datenimport für Augustinuswerk:\n"

    def handle(self, *args, **options):

        with open('Import_Augustinuswerk.csv', 'rb') as csvfile:
            oReader = csv.DictReader(csvfile, delimiter=';')

            facility = Facility.objects.get(pk=1)
            paytype_debit = PayType.objects.get(pk=PayType.DEBIT)
            paytype_invoice = PayType.objects.get(pk=PayType.INVOICE)

            iRowCounter = 0
            iUserCounter = 0
            iCustomerCounter = 0
            iOnlineInvoiceCounter = 0
            iSepaCounter = 0
            iChildrenCounter = 0
            sLastId = ''
            oLastCustomer = None
            for dRow in oReader:
                iRowCounter += 1
                bCreateSepa = True

                try:
                    # create new Customer
                    if sLastId == dRow['Kunde_Id']:
                        customer = oLastCustomer
                        bCreateSepa = False
                    else:
                        customer = Customer()
                        customer.title = dRow['Kunde_Anrede']
                        customer.name = dRow['Kunde_Nachname']
                        customer.email = dRow['Kunde_Email']
                        customer.surname = dRow['Kunde_Vorname']
                        customer.street = "%s %s" % (dRow['Kunde_Strasse'], dRow['Kunde_Hnr.'])
                        customer.zip_code = dRow['Kunde_Postleitzahl']
                        customer.city = dRow['Kunde_Ort']
                        customer.phone = dRow['Kunde_Telefon']
                        if dRow['Kunde_Zahlungsart'] == 'Lastschrift':
                            customer.pay_type = paytype_debit
                        else:
                            customer.pay_type = paytype_invoice
                        customer.save()
                        iCustomerCounter += 1

                    # SEPA Mandat eintragen -> NUR EIN MANDAT PRO ELTERN
                    if bCreateSepa and customer.pay_type.id == PayType.DEBIT:
                        existing_sepa_mandate = SepaMandate.objects.filter(customer=customer)
                        if len(existing_sepa_mandate) > 1:
                            self.stdout.write("Doppeltes SEPA Mandat bei Kunde %s, %s" % (customer.name, customer.surname))
                        if not existing_sepa_mandate:
                            sepa_mandate = SepaMandate(customer=customer)
                            sepa_mandate.name = "%s %s" % (dRow['SEPA_Kontoinhaber_Vorname'], dRow['SEPA_Kontoinhaber_Name'])
                            sepa_mandate.street = customer.street
                            sepa_mandate.city = "%s %s" % (customer.zip_code, customer.city)
                            sepa_mandate.mandate_id = dRow['SEPA_Mandatsreferenz']
                            sepa_mandate.iban = dRow['SEPA_IBAN']
                            sepa_mandate.bic = dRow['SEPA_BIC']
                            sepa_mandate.is_valid = True
                            sepa_mandate.on_paper = True
                            sepa_mandate.created_at = datetime.strptime(dRow['SEPA_Datum'], "%d.%m.%Y")
                            sepa_mandate.last_debit_date = date(2016,1,1)
                            sepa_mandate.last_debit_sequence = 'RCUR'
                            sepa_mandate.save(force_insert=True)
                            iSepaCounter += 1

                    # Kind anlegen und in richtige Einrichtung/Klasse speichern
                    facility_subunit = FacilitySubunit.objects.filter(facility=facility,
                                                                      name_short=dRow['Kind_Klasse'])
                    if not facility_subunit or len(facility_subunit) != 1:
                        print "Facility Subunit nicht gefunden oder nicht eindeutig: %s, %s" % (dRow['Kind_Nachname'], dRow['Kind_Vorname'])
                    else:
                        child, was_created = Child.objects.get_or_create(customer=customer,
                                                                         name=dRow['Kind_Nachname'],
                                                                         surname=dRow['Kind_Vorname'],
                                                                         facility=facility, facility_subunit=facility_subunit[0])
                        if was_created:
                            iChildrenCounter += 1

                    sLastId = dRow['Kunde_Id']
                    oLastCustomer = customer

                except Exception as e:
                    self.stdout.write("Exception: %s\n" % e)
                    self.stdout.write("%s, %s\n" % (dRow['Kind_Nachname'], dRow['Kind_Vorname']))

        self.stdout.write("Anzahl Zeilen: %d\n" % iRowCounter)
        self.stdout.write("Angelegte Customer: %d\n" % iCustomerCounter)
        self.stdout.write("Angelegte SepaMandate: %d\n" % iSepaCounter)
        self.stdout.write("Angelegte Children: %d\n" % iChildrenCounter)