from datetime import date, datetime
from decimal import Decimal

from django.core.management.base import BaseCommand, CommandError
from django.db.models import Q

from kg.models import SepaDebit, CustomerAccountTransaction


class Command(BaseCommand):
    help = "Prueft, ob alle SEPA Lastschriften als CATs gebucht wurden, korrigiert Fehler.\n"

    def handle(self, *args, **options):
        self.stdout.write("%s" % self.help)
        choice = None
        while not choice:
            val = raw_input("Enter YES if you want to proceed: ")
            choice = str(val)

        if choice != "YES":
            self.stdout.write("Aborted by user!\n")
        else:
            iCounter = 0
            iRepairCounter = 0
            qsSepaDebits = SepaDebit.objects.filter(submission_time__gte=datetime(2015,12,14)) # ignore all before 14.12.15
            self.stdout.write("Process %d SepaDebits..\n" % len(qsSepaDebits))

            try:
                for oSepaDebit in qsSepaDebits:
                    oCustomer = oSepaDebit.customer

                    # check existing CAT, correct it if necessary
                    qsCAT = CustomerAccountTransaction.objects.filter(customer=oCustomer,
                                                                      transaction_id="sepadebit_%d" % oSepaDebit.id)

                    bCreateNewCAT = False
                    if qsCAT:
                        if len(qsCAT) > 1:
                            qsCAT.delete()
                            bCreateNewCAT = True
                            self.stdout.write("Mehrere CATs: %s\n" % (oSepaDebit.customer))
                        elif qsCAT[0].amount != oSepaDebit.amount:
                            qsCAT.update(amount=oSepaDebit.amount)
                            iRepairCounter += 1
                            self.stdout.write("Korrigierte CAT: %s\n" % (oSepaDebit.customer))
                    else:
                        bCreateNewCAT = True

                    if bCreateNewCAT:
                        oNewCAT = CustomerAccountTransaction(customer=oCustomer,
                                                             transaction_id="sepadebit_%d" % oSepaDebit.id,
                                                             type=CustomerAccountTransaction.TYPE_DEBIT,
                                                             status=CustomerAccountTransaction.STATUS_OPEN,
                                                             amount = oSepaDebit.amount,
                                                             description = u"Lastschrifteinzug",
                                                             transaction_date=oSepaDebit.submission_time)
                        oNewCAT.save()
                        iCounter += 1

                self.stdout.write("Orders repaired: %d\n" % iRepairCounter)
                self.stdout.write("CATs created: %d\n" % iCounter)

            except Exception as e:
                raise CommandError("Error\n%s\n" % e.message)


