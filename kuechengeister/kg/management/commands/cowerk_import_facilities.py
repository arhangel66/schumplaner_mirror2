# -*- coding: utf-8 -*-

from datetime import date, datetime

from django.core.management.base import BaseCommand

from kg_utils import ucsv as csv
from kg.models import Facility, FacilitySubunit, FacilityType
from menus.models import MealPlan, PriceGroup


class Command(BaseCommand):
    help = "Datenimport für Cowerk:\n"

    def handle(self, *args, **options):

        with open('Cowerk_Daten.csv', 'rb') as csvfile:
            oReader = csv.DictReader(csvfile, delimiter=';')

            iRowCounter = 0
            iFacilityCounter = 0
            iSubfacilityCounter = 0
            lFacilities = []
            dSubfacilities = {}
            for dRow in oReader:
                iRowCounter += 1
                #if dRow['SCHULE'] == 'NULL' or dRow['KLASSE'] == 'NULL':
                #    continue
                if dRow['SCHULE'] not in lFacilities:
                    lFacilities.append(dRow['SCHULE'])
                    iFacilityCounter += 1
                if dRow['SCHULE'] not in dSubfacilities:
                    dSubfacilities[dRow['SCHULE']] = [dRow['KLASSE'],]
                    iSubfacilityCounter += 1
                else:
                    if not dRow['KLASSE'] in dSubfacilities[dRow['SCHULE']]:
                        dSubfacilities[dRow['SCHULE']].append(dRow['KLASSE'])
                        iSubfacilityCounter += 1

            #print lFacilities
            #print dSubfacilities

            try:
                oFacilityType = FacilityType.objects.get(pk=1)
                oPriceGroup = PriceGroup.objects.get(pk=1)
                oMealPlan = MealPlan.objects.get(pk=1)
                for sFacility in lFacilities:
                    qsFacilities = Facility.objects.filter(name=sFacility)
                    if not qsFacilities:
                        oFacility = Facility(name=sFacility,
                                             name_short='',
                                             is_active=True,
                                             type=oFacilityType,
                                             flatrate_customizable=False,
                                             tax_rate=7)
                        oFacility.save()
                    else:
                        oFacility = qsFacilities[0]

                    for sSubfacility in dSubfacilities[sFacility]:
                        qsFacilitySubunits = FacilitySubunit.objects.filter(facility=oFacility,
                                                                            name=sSubfacility)
                        if not qsFacilitySubunits:
                            oFacilitySubunit = FacilitySubunit(facility=oFacility,
                                                               name=sSubfacility,
                                                               name_short='',
                                                               mealplan=oMealPlan,
                                                               price_group=oPriceGroup,
                                                               tax_rate=7
                                                               )
                            oFacilitySubunit.save()

            except Exception as e:
                self.stdout.write("Exception: %s\n" % e)

        self.stdout.write("Anzahl Zeilen: %d\n" % iRowCounter)
        self.stdout.write("Angelegte Schulen: %d\n" % iFacilityCounter)
        self.stdout.write("Angelegte Klassen: %d\n" % iSubfacilityCounter)