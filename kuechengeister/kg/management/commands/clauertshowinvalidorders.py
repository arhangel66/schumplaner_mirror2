from django.core.management.base import BaseCommand, CommandError
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned

from kg.models import Order, Menu, Flatrate
from kg_utils.validators import IbanValidator, BicValidator


class Command(BaseCommand):
    help = "Gibt alle Nachtisch Bestellungen und Abonnements aus.\n"

    def handle(self, *args, **options):

        orders = Order.objects.filter(menu__mealtime__id=7).order_by("child__facility__name", "child__name")
        self.stdout.write("Anzahl fehlerhafter Bestellungen: %d\n" % len(orders))
        for order in orders:
            self.stdout.write("Order: %s - %s\n" % (order.child.facility.name, order))

        flatrates = Flatrate.objects.filter(mealtime__id=7, active=True).order_by("child__facility__name", "child__name")
        self.stdout.write("Anzahl fehlerhafter Abos normal: %d\n" % len(flatrates))
        for flatrate in flatrates:
            self.stdout.write("Abo normal: %s - %s\n" % (flatrate.child.facility.name, flatrate))

        flatrates = Flatrate.objects.filter(mealtime__id=7, active_on_holidays=True).order_by("child__facility__name", "child__name")
        self.stdout.write("Anzahl fehlerhafter Abos Ferien: %d\n" % len(flatrates))
        for flatrate in flatrates:
            self.stdout.write("Abo Ferien: %s - %s\n" % (flatrate.child.facility.name, flatrate))

