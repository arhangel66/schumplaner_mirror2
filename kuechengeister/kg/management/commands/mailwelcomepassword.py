# -*- coding: utf-8 -*-

from smtplib import SMTPException

from django.core.management.base import BaseCommand, CommandError
from django.db.models import Max
from django.core.mail import send_mass_mail

from kg.models import Customer, User


class Command(BaseCommand):
    help = "Send welcome message with login data for all customers\n"

    from_email = "kinderkueche@q-linar.com"
    subject = u"Vollwert Kinderküche Dresden - Neues Online-Bestellsystem"
    mailtext_raw=u"""Liebe Eltern, liebe Lehrer, liebe Erzieher und liebe Kinder,

es ist endlich soweit:

Unser neues Online-Bestellsystem ist ab sofort einsatzbereit!
Alle Bestellungen ab 02.03.15 machen Sie bitte im neuen Online-Bestellsystem.

Hier finden Sie den neuen Zugang: http://www.vollwert-kinderkueche.de

Ihr Benutzername: ___benutzername___
Ihr Passwort: ___passwort___


Hier die wichtigsten Neuerungen:
- 24 Stunden Rufannahme-Sprachsystem für Bestellungsänderungen unter der Telefonnummer: 0351/89693749 zum ortsüblichen Tarif bzw. Handytarif.
- Neue Bestellzeiten – Abbestellungen bzw. Zubestellungen am Tag jetzt bis 08:00 Uhr möglich.
- Monatliche Rechnungsstellung.
- Einfache Bestellübersicht.
- Abo-Bestellungen möglich.
- Vereinfachte BuT Abrechnung.
- Vereinfachter Passwortzugang zum System.
- Automatische Rechnungsablage ins Postfach.

Ihr bestehendes Guthaben vom 28.02.15 wird in das neue System übernommen und mit Ihrer März-Rechnung verrechnet.

Hat Ihr Kind einen Chip, so wird dieser am 02.03.15 in der Schule gegen einen modernen NFC Chip getauscht.

Viele Grüße
Ihr Vollwert Kinderküche Dresden Team

Vollwert Kinderküche Dresden by Q-LINAR
Güntzplatz 5
01307 Dresden
Tel: 0351 455 95930
kinderkueche@q-linar.com"""


    def handle(self, *args, **options):
        self.stdout.write("%s" % self.help)
        choice = None
        while not choice:
            val = raw_input("Enter YES if you want to proceed: ")
            choice = str(val)

        if choice != "YES":
            self.stdout.write("Aborted by user!\n")
        else:
            mails = []
            try:
                for customer in Customer.objects.all().order_by('name', 'surname'):

                    if customer.email != '':
                        if customer.user and 'patrick-weitz.de' in customer.email:
                            new_passwd = User.objects.make_random_password(length=8)
                            customer.user.set_password(new_passwd)
                            customer.user.save()

                            mail_text = self.mailtext_raw.replace('___benutzername___', customer.user.username)
                            mail_text = mail_text.replace('___passwort___', new_passwd)

                            mails.append((self.subject, mail_text, self.from_email, [customer.email]))

                try:
                    self.stdout.write("Try to send %d emails as bulk email...\n" % len(mails))
                    send_mass_mail(tuple(mails), fail_silently=False)
                    for mail in mails:
                        self.stdout.write("%s\n" % mail[3][0])
                    self.stdout.write("Emails sent: %d\n" % len(mails))

                except SMTPException as exc:
                    self.stdout.write("An exception occured: %s\n" % exc)

            except Exception as e:
                raise CommandError("Error\n%s\n" % e.message)
