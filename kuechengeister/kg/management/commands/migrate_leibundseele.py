# -*- coding: utf-8 -*-

import traceback
from datetime import date
from decimal import Decimal

from django.core.management.base import BaseCommand, CommandError
from django.contrib.flatpages.models import FlatPage
from django.db.models import connection
from django.core.exceptions import ObjectDoesNotExist

from kg.models import Flatrate, Order
from menus.models import Price, PriceGroup, Menu, Meal, MealPlan, MealDefaultPrice, MealPeriod, Allergen, Additional


class Command(BaseCommand):
    help = ("Migration script for Leibundseele.\n"
            "ATTENTION: Run after all migrations! Delete after usage!\n\n")

    def handle(self, *args, **options):
        self.stdout.write("%s" % self.help)
        choice = None
        while not choice:
            val = raw_input("Enter YES if you want to proceed: ")
            choice = str(val)

        if choice != "YES":
            self.stdout.write("Aborted by user!\n")
        else:
            try:
                self.stdout.write("Update flatpages..\n")
                flat_home = """<h2>Willkommen bei Speisenbildung Leib &amp; Seele</h2>
                <h3>Allgemeines</h3>
                <p>Die Firma Leib &amp; Seele Dienst am Gast GmbH ist ein mittelst&auml;ndisches Cateringunternehmen und versorgt in 15 Einrichtungen t&auml;glich ca. 6000 G&auml;ste mit Speisen und Imbissartikeln. Unser Bet&auml;tigungsfeld umfasst nahezu alles was sich unter dem Thema Essen und Trinken zusammenfassen l&auml;sst. Wir erm&ouml;glichen "rund um die Uhr" Versorgung von Industrieanlagen und Beh&ouml;rden ebenso zuverl&auml;ssig, wie wir die ausgewogene Verk&ouml;stigung von Sch&uuml;lern sicherstellen k&ouml;nnen. Generell kochen wir frisch vor Ort und k&ouml;nnen daher auf die Besonderheiten Ihres Standortes eingehen.</p>
                <h3>ACHTUNG</h3>
                <p><strong>Anmeldung in Kurzform:</strong></p>
                <p>-bei der Registrierung f&uuml;r Neukunden alle Angaben machen, Zahlungsmethode ist Lastschrift, -nach erfolgter Zusendung des Passwortes als Kunde anmelden, -auf das Feld Kind hinzuf&uuml;gen klicken und sich oder das Kind eintragen (den "Essenempf&auml;nger"), -richtige Einrichtung, Klasse w&auml;hlen, -Reiter Essenbestellung anklicken und Essen ausw&auml;hlen.</p>
                <p>Das Essen f&uuml;r Montag und Dienstag kann nur bis zum Freitag 08:00 Uhr (der Vorwoche) bestellt werden, da nur so die Zubereitung von Speisen mit frischen Zutaten in der n&ouml;tigen Anzahl m&ouml;glich ist! Wir danken f&uuml;r Ihr Verst&auml;ndnis.</p>
                <p>&nbsp;</p>
                <p><strong>Kontakt:</strong></p>
                <p><strong>Unsere Servicenummer: 034605-14 99 99</strong></p>
                <p><strong>Telefax: 034605-14 99 84</strong></p>
                <p><strong>E-Mailadresse: info@speisenbildung-leib-und-seele.de</strong></p>
                <p><strong>Postanschrift: Marktplatz 3, 06184 Kabelsketal OT Gro&szlig;kugel</strong></p>"""
                FlatPage.objects.filter(url='/').update(content=flat_home)

                self.stdout.write("Update MealPeriode..\n")
                MealPeriod.objects.all().update(from_date=date(2000,1,1))

                self.stdout.write("Delete all prices..\n")
                Price.objects.all().delete()

                self.stdout.write("Delete all MealDefaultPrices..\n")
                MealDefaultPrice.objects.all().delete()

                self.stdout.write("Update PriceGroups..\n")
                PriceGroup.objects.filter(pk=1).update(mealplan=MealPlan.objects.get(pk=1))
                PriceGroup.objects.filter(pk=2).update(mealplan=MealPlan.objects.get(pk=2))
                PriceGroup.objects.filter(pk=3).update(mealplan=MealPlan.objects.get(pk=3))
                PriceGroup.objects.filter(pk=4).update(mealplan=MealPlan.objects.get(pk=4))
                PriceGroup.objects.filter(pk=5).update(mealplan=MealPlan.objects.get(pk=5))
                PriceGroup.objects.filter(pk=6).update(mealplan=MealPlan.objects.get(pk=6))
                PriceGroup.objects.filter(pk=7).update(mealplan=MealPlan.objects.get(pk=7))
                PriceGroup.objects.filter(pk=8).update(mealplan=MealPlan.objects.get(pk=8))
                PriceGroup.objects.filter(pk=9).update(mealplan=MealPlan.objects.get(pk=9))

                self.stdout.write("Create MealDefaultPrices..\n")
                # meal_id, price_group_id, price, price_is_variable, create_pricegroup_for_element
                lMealDefaultPrices = [
                                      # Bergmannstrost
                                      (29, 7, Decimal('0.00'), True, True),
                                      (30, 7, Decimal('0.00'), True, True),
                                      (31, 7, Decimal('0.00'), True, True),

                                      # Lebensweg
                                      (6, 2, Decimal('2.70'), False, True),
                                      (7, 2, Decimal('2.70'), False, True),

                                      # Neumarkt
                                      (1, 1, Decimal('3.10'), False, True),
                                      (2, 1, Decimal('3.10'), False, True),
                                      (3, 1, Decimal('3.10'), False, True),
                                      (4, 1, Decimal('3.90'), False, True),
                                      (5, 1, Decimal('3.90'), False, True),
                    
                                      # Andersen Schule
                                      (12, 4, Decimal('3.10'), False, True),
                                      (13, 4, Decimal('3.10'), False, True),
                                      (15, 4, Decimal('3.20'), False, True),
                                      (14, 4, Decimal('0.00'), False, False),
                                      (16, 4, Decimal('0.00'), False, False),

                                      # Halberstadt
                                      (8, 3, Decimal('1.75'), False, True),
                                      (9, 3, Decimal('1.75'), False, True),
                                      (10, 3, Decimal('1.75'), False, True),
                                      (11, 3, Decimal('0.50'), False, True),
                                      (17, 3, Decimal('0.50'), False, True),

                                      # Internat Halberstadt
                                      (8, 5, Decimal('1.75'), False, True),
                                      (9, 5, Decimal('1.75'), False, True),
                                      (10, 5, Decimal('1.75'), False, True),

                                      # Schneeberg
                                      (44, 9, Decimal('0.00'), True, True),
                                      (45, 9, Decimal('0.00'), True, True),
                                      (46, 9, Decimal('0.00'), True, True),

                                      # Alsleben
                                      (19, 6, Decimal('0.00'), True, True),
                                      (24, 6, Decimal('0.00'), True, True),
                                      (18, 6, Decimal('0.00'), True, False),
                                      (20, 6, Decimal('0.00'), True, False),
                                      (21, 6, Decimal('0.00'), True, False),
                                      (22, 6, Decimal('0.00'), True, False),
                                      (23, 6, Decimal('0.00'), True, False),
                                      (25, 6, Decimal('0.00'), True, False),
                                      (26, 6, Decimal('0.00'), True, False),
                                      (27, 6, Decimal('0.00'), True, False),
                                      (28, 6, Decimal('0.00'), True, False),
                                      ]
                for tMDP in lMealDefaultPrices:
                    if tMDP[4]:
                        oNewMDP, created = MealDefaultPrice.objects.get_or_create(meal_id=tMDP[0],
                                                                                  price_group_id=tMDP[1],
                                                                                  price=tMDP[2],
                                                                                  price_is_variable=tMDP[3])

                self.stdout.write("Set Prices for all Menus..\n")
                cursor = connection.cursor()
                cursor.execute('''SELECT id, mealtime_id, price_group_id FROM kg_menu''')
                for tKgMenu in cursor.fetchall():
                    oNewPrice = Decimal('0.00')
                    if tKgMenu[1] in [18,]:
                        oNewPrice = Decimal('2.64')
                    if tKgMenu[1] in [20, 25]:
                        oNewPrice = Decimal('3.10')
                    if tKgMenu[1] in [21, 26]:
                        oNewPrice = Decimal('3.20')
                    if tKgMenu[1] in [22, 27]:
                        oNewPrice = Decimal('3.30')
                    if tKgMenu[1] in [23, 28]:
                        oNewPrice = Decimal('3.40')
                    if tKgMenu[1] in [1, 2, 3, 12, 13, 15]:
                        if tKgMenu[2] == 2:
                            oNewPrice = Decimal('2.90')
                        if tKgMenu[2] == 53:
                            oNewPrice = Decimal('3.10')
                    if tKgMenu[1] in[19, 24]:
                        oNewPrice = Decimal('2.90')
                    if tKgMenu[1] in [4, 5]:
                        if tKgMenu[2] == 5:
                            oNewPrice = Decimal('3.70')
                        if tKgMenu[2] == 54:
                            oNewPrice = Decimal('3.90')
                    if tKgMenu[1] in [14, 16]:
                        if tKgMenu[2] == 14:
                            oNewPrice = Decimal('3.20')
                        if tKgMenu[2] == 55:
                            oNewPrice = Decimal('3.40')
                    if tKgMenu[1] in [6, 7]:
                        oNewPrice = Decimal('2.70')
                    if tKgMenu[1] in [8, 9, 10]:
                        oNewPrice = Decimal('1.75')
                    if tKgMenu[1] in [11, 17]:
                        oNewPrice = Decimal('0.50')
                    if tKgMenu[1] in [29, 30, 31]:
                        if tKgMenu[2] == 23:
                            oNewPrice = Decimal('2.90')
                        if tKgMenu[2] == 33:
                            oNewPrice = Decimal('2.95')
                        if tKgMenu[2] == 24:
                            oNewPrice = Decimal('3.00')
                        if tKgMenu[2] == 25:
                            oNewPrice = Decimal('3.05')
                        if tKgMenu[2] == 26:
                            oNewPrice = Decimal('3.10')
                        if tKgMenu[2] == 34:
                            oNewPrice = Decimal('3.15')
                        if tKgMenu[2] == 27:
                            oNewPrice = Decimal('3.20')
                        if tKgMenu[2] == 28:
                            oNewPrice = Decimal('3.25')
                        if tKgMenu[2] == 29:
                            oNewPrice = Decimal('3.30')
                        if tKgMenu[2] == 31:
                            oNewPrice = Decimal('3.40')
                        if tKgMenu[2] == 30:
                            oNewPrice = Decimal('3.45')
                        if tKgMenu[2] == 37:
                            oNewPrice = Decimal('3.50')
                        if tKgMenu[2] == 32:
                            oNewPrice = Decimal('3.60')
                        if tKgMenu[2] == 39:
                            oNewPrice = Decimal('3.80')
                        if tKgMenu[2] == 35:
                            oNewPrice = Decimal('4.20')
                    if tKgMenu[1] in [44, 45, 46]:
                        if tKgMenu[2] == 40:
                            oNewPrice = Decimal('3.00')
                        if tKgMenu[2] == 46:
                            oNewPrice = Decimal('3.10')
                        if tKgMenu[2] == 47:
                            oNewPrice = Decimal('3.30')
                        if tKgMenu[2] == 42:
                            oNewPrice = Decimal('3.50')
                        if tKgMenu[2] == 50:
                            oNewPrice = Decimal('3.60')
                        if tKgMenu[2] == 48:
                            oNewPrice = Decimal('3.70')
                        if tKgMenu[2] == 43:
                            oNewPrice = Decimal('4.00')
                        if tKgMenu[2] == 52:
                            oNewPrice = Decimal('4.10')
                        if tKgMenu[2] == 51:
                            oNewPrice = Decimal('4.20')
                        if tKgMenu[2] == 44:
                            oNewPrice = Decimal('4.50')
                        if tKgMenu[2] == 49:
                            oNewPrice = Decimal('4.60')
                        if tKgMenu[2] == 45:
                            oNewPrice = Decimal('5.00')

                    if oNewPrice > 0:
                        for tMDP in lMealDefaultPrices:
                            if tMDP[0] == tKgMenu[1]:
                                oPrice, created = Price.objects.get_or_create(menu=Menu.objects.get(pk=tKgMenu[0]),
                                                                              price_group=PriceGroup.objects.get(pk=tMDP[1]),
                                                                              price=oNewPrice)

                self.stdout.write("Join Meals..\n")
                lJoinMeals = [(14, 13), (16, 15),
                              (18, 19), (20, 19), (21, 19), (22, 19), (23, 19),
                              (25, 24), (26, 24), (27, 24), (28, 24),
                              (39, 0), (40, 0), (41, 0), (42, 0), (47, 0), ]
                for tJM in lJoinMeals:
                    if tJM[1] > 0:
                        Menu.objects.filter(meal__id=tJM[0]).update(meal=Meal.objects.get(pk=tJM[1]))
                    Flatrate.objects.filter(meal__id=tJM[0]).delete()
                    Meal.objects.filter(pk=tJM[0]).delete()

                self.stdout.write("Updated allergens..\n")
                Allergen.objects.filter(pk__in=[1,2,3,4,5,6,7,8,9,10,11,12,13,14]).update(use_for_menu=True)

                self.stdout.write("Create additionals..\n")
                lAdditinals = [(1, 'mit Farbstoff'),
                               (2, 'mit Konservierungsmittel'),
                               (3, 'mit Antioxidationsmittel'),
                               (4, 'mit Geschmacksverstärker'),
                               (5, 'mit Süßungsmittel'),
                               (6, 'mit Phosphat'),
                               (7, 'gewachst'),
                               (8, 'mit Farbstoff'),
                               (9, 'geschwefelt'),
                               (10, 'koffeinhaltig')]
                for tAdditional in lAdditinals:
                    oAdditional, bCreated = Additional.objects.get_or_create(pk=tAdditional[0])
                    if bCreated:
                        oAdditional.name = tAdditional[1]
                        oAdditional.name_short = str(tAdditional[0])
                        oAdditional.sortKey = tAdditional[0]
                        oAdditional.save()

                MealPlan.objects.filter(pk=6).update(weekdays=7)

                self.stdout.write("Done\n")

            except Exception:
                traceback.print_exc()
                raise CommandError("Abortion through Exception\n")
