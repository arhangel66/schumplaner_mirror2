# -*- coding: utf-8 -*-

import traceback
from datetime import datetime, date, timedelta

from django.core.management.base import BaseCommand, CommandError

from kg.models import Meal, Mealtime, Menu, PriceGroup
from kg_utils.date_helper import DateHelper


class Command(BaseCommand):
    help = ("Create placeholder menus for 01/2016\n\n")

    def handle(self, *args, **options):
        self.stdout.write("%s" % self.help)

        choice = None
        while not choice:
            val = raw_input("Enter YES if you want to proceed: ")
            choice = str(val)

        if choice != "YES":
            self.stdout.write("Aborted by user!\n")
        else:
            self.stdout.write("Create placeholder menus..\n")

            iMenuCounter = 0
            try:
                for oMealtime in Mealtime.objects.all():
                    oPricegroup = PriceGroup.objects.get(mealtimes=oMealtime)
                    for iWeek in range(1,5):
                        lDays = DateHelper.getDaysInCalendarWeek(2016, iWeek)[0:5]
                        for oDay in lDays:
                            oMenu = Menu(mealtime=oMealtime,
                                         date=oDay,
                                         description=oMealtime.name_public_menu,
                                         price_group=oPricegroup)
                            oMenu.save()
                            iMenuCounter += 1

                self.stdout.write("Number of created menus: %d\n" % iMenuCounter)

            except Exception:
                traceback.print_exc()
                raise CommandError("Abortion through Exception\n")
