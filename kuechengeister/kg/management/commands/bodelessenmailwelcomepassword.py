# -*- coding: utf-8 -*-

from smtplib import SMTPException

from django.core.management.base import BaseCommand, CommandError
from django.core.mail import send_mail, send_mass_mail

from kg.models import Customer, User


class Command(BaseCommand):
    help = "Send welcome message with login data for all customers with email address\n"

    from_email = "Bodelschwingh-Haus Wolmirstedt WfbM <info@bodel-essen.de>"
    subject = u"Ihr Zugang zum neuen Online-Bestellsystem"
    mailtext_raw=u"""Liebe Eltern, liebe Lehrer, liebe Erzieher und liebe Kinder,

es ist endlich soweit:

Unser neues Online-Bestellsystem ist ab sofort einsatzbereit!

Alle Bestellungen ab 27.08.15 machen Sie bitte im neuen Online-Bestellsystem.
Bitte achten Sie darauf, dass Sie erst Guthaben aufladen müssen, bevor Sie Essen bestellen können. Alle Informationen dazu finden Sie in Ihrem Kundenbereich unter dem Menüpunk "Guthaben".

Hier finden Sie das neue Online-Bestellsystem: http://www.bodel-essen.de/

Ihr Benutzername: ___benutzername___
Ihr Passwort: ___passwort___


Viele Grüße
Ihr Bodelschwingh-Haus Wolmirstedt WfbM Team"""


    def handle(self, *args, **options):
        self.stdout.write("%s" % self.help)
        choice = None
        while not choice:
            val = raw_input("Enter YES if you want to proceed: ")
            choice = str(val)

        if choice != "YES":
            self.stdout.write("Aborted by user!\n")
        else:
            mails = []
            try:
                qsCustomers = Customer.objects.filter(email__icontains="patrick-weitz.de")
                #qsCustomers = Customer.objects.all().order_by('name', 'surname')[70:]

                for customer in qsCustomers:

                    if customer.email != '':

                        new_passwd = User.objects.make_random_password(length=8)

                        if customer.user:
                            user = customer.user
                            user.username = customer.email
                            user.email = customer.email
                            user.set_password(new_passwd)
                            user.save()

                        else:
                            user = User.objects.create_user(customer.email, customer.email, new_passwd)
                            user.save()

                            customer.user = user
                            customer.save()

                        mail_text = self.mailtext_raw.replace('___benutzername___', user.username)
                        mail_text = mail_text.replace('___passwort___', new_passwd)

                        mails.append((self.subject, mail_text, self.from_email, [customer.email]))

                try:
                    self.stdout.write("Try to send %d emails..\n" % len(mails))
                    for mail in mails:
                        self.stdout.write("Try: %s\n" % mail[3][0])
                        send_mail(mail[0], mail[1], mail[2], mail[3], fail_silently=False)
                    self.stdout.write("Emails sent: %d\n" % len(mails))

                except SMTPException as exc:
                    self.stdout.write("An exception occured: %s\n" % exc)

            except Exception as e:
                raise CommandError("Error\n%s\n" % e.message)
