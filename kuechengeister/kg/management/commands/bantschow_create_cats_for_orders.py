from datetime import date, datetime
from decimal import Decimal

from django.core.management.base import BaseCommand, CommandError
from django.db.models import Q

from kg.models import ChildReduction, Order, CustomerAccountTransaction


class Command(BaseCommand):
    help = "Erstellt CATs fuer alle Orders, korrigiert Fehler.\n"

    def handle(self, *args, **options):
        self.stdout.write("%s" % self.help)
        choice = None
        while not choice:
            val = raw_input("Enter YES if you want to proceed: ")
            choice = str(val)

        if choice != "YES":
            self.stdout.write("Aborted by user!\n")
        else:
            iCounter = 0
            iOrderCounter = 0
            iExcludeCounter = 0
            qsOrders = Order.objects.all()
            self.stdout.write("Process %d orders..\n" % len(qsOrders))

            try:
                for oOrder in qsOrders:
                    oChild = oOrder.child
                    oCustomer = oChild.customer
                    oMenu = oOrder.menu
                    oReduction = None
                    oMenuPrice = oMenu.get_price()
                    oLastChangeDate = oOrder.last_modified

                    # check for reduction: if there is one, set reduced price
                    qsReductions = ChildReduction.objects.select_related().filter(child=oChild, date_start__lte=oMenu.date, date_end__gte=oMenu.date)
                    if qsReductions:
                        oReduction = qsReductions[0].reduction
                        oMenuPrice = oReduction.reduction

                    if oOrder.price is None or oOrder.price == Decimal('0.00') or oOrder.reduction != oReduction:
                        oOrder.price = oMenuPrice
                        oOrder.reduction = oReduction
                        oOrder.save()
                        iOrderCounter += 1

                    # skip orders until 14.12.2015 for 2 schools
                    if oChild.facility.id in [3, 4] and oMenu.date < date(2015, 12, 14):
                        iExcludeCounter += 1
                        continue

                    # check existing CAT, correct it if necessary
                    qsCAT = CustomerAccountTransaction.objects.filter(customer=oOrder.child.customer,
                                                                      transaction_id="order_%d" % oOrder.id)
                    bCreateNewCAT = False
                    if qsCAT:
                        if len(qsCAT) > 1:
                            qsCAT.delete()
                            bCreateNewCAT = True
                            self.stdout.write("Mehrere CATs: %s - %s\n" % (oOrder.child, oOrder.child.customer))
                        elif qsCAT[0].amount != oOrder.price.copy_negate():
                            qsCAT.update(amount=oOrder.price.copy_negate())
                            self.stdout.write("Korrigierte CAT: %s - %s\n" % (oOrder.child, oOrder.child.customer))
                    else:
                        bCreateNewCAT = True

                    if bCreateNewCAT:
                        oNewCAT = CustomerAccountTransaction(customer=oCustomer,
                                                             transaction_id="order_%d" % oOrder.id,
                                                             type=CustomerAccountTransaction.TYPE_CHARGE,
                                                             status=CustomerAccountTransaction.STATUS_OPEN,
                                                             amount = oMenuPrice.copy_negate(),
                                                             description = u"%s, %s, %s" % (oMenu.mealtime.name, oMenu.date.strftime('%d.%m.%Y'), oChild.surname),
                                                             transaction_date=oLastChangeDate)
                        oNewCAT.save()
                        iCounter += 1

                self.stdout.write("Orders repaired: %d\n" % iOrderCounter)
                self.stdout.write("CATs created: %d\n" % iCounter)
                self.stdout.write("CATs ignored: %d\n\n" % iExcludeCounter)

            except Exception as e:
                raise CommandError("Error\n%s\n" % e.message)


