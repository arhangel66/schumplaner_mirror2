from decimal import Decimal

from django.core.management.base import BaseCommand, CommandError
from django.db.models import Sum

from kg.models import Customer, CustomerAccountTransaction


class Command(BaseCommand):
    help = "Recalculate and set account balance for every customer.\n"

    def handle(self, *args, **options):
        self.stdout.write("%s" % self.help)
        choice = None
        while not choice:
            val = raw_input("Enter YES if you want to proceed: ")
            choice = str(val)

        if choice != "YES":
            self.stdout.write("Aborted by user!\n")
        else:
            iCounter = 0

            try:
                qsAllCustomers = Customer.objects.all()
                self.stdout.write("Process %d customers..\n" % len(qsAllCustomers))
                for oCustomer in qsAllCustomers:
                    oNewBalance = CustomerAccountTransaction.objects.filter(customer__id=oCustomer.id).aggregate(Sum('amount'))['amount__sum'] or Decimal('0.00')
                    if oCustomer.account_balance != oNewBalance:
                        oCustomer.account_balance = oNewBalance
                        oCustomer.save()
                        iCounter += 1

            except Exception as e:
                raise CommandError("Error\n%s\n" % e.message)

            self.stdout.write("Fixed customers: %d\n" % iCounter)
