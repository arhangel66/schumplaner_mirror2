from decimal import Decimal

from django.core.management.base import BaseCommand, CommandError

from kg.models import Order, ChildReduction


class Command(BaseCommand):
    help = "Set prices and reductions to all orders without prices.\n"

    def handle(self, *args, **options):
        self.stdout.write("%s" % self.help)
        choice = None
        while not choice:
            val = raw_input("Enter YES if you want to proceed: ")
            choice = str(val)

        if choice != "YES":
            self.stdout.write("Aborted by user!\n")
        else:
            iCounter = 0

            try:
                iCounter = 0
                for oOrder in Order.all_objects.filter(price__isnull=True):
                    qsChildReduction = ChildReduction.objects.filter(child__id=oOrder.child_id,
                                                                     date_start__lte=oOrder.menu.date,
                                                                     date_end__gte=oOrder.menu.date)
                    if len(qsChildReduction) > 0:
                        # reduction found
                        if len(qsChildReduction) == 1:
                            oOrder.price = Decimal('1.00')
                            oOrder.reduction__id = qsChildReduction[0].id
                            oOrder.save()
                        else:
                            self.stdout.write('Mehr als eine Ermaessigung gefunden! child_id = %d\n' % oOrder.child__id)
                    else:
                        # no reduction
                        oOrder.price = Decimal('2.70')
                        oOrder.save()


                    iCounter += 1

            except Exception as e:
                raise CommandError("Error\n%s\n" % e.message)

            self.stdout.write("Updated orders: %d\n" % iCounter)


