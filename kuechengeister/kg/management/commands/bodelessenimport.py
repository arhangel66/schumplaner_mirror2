# -*- coding: utf-8 -*-

from datetime import date, datetime

from django.core.management.base import BaseCommand

from kg_utils import ucsv as csv
from kg.models import User, Customer, Child, Facility, FacilitySubunit, SepaMandate, OnlineInvoice, PayType



class Command(BaseCommand):
    help = "Datenimport für Bodelschwinghaus:\n"

    def handle(self, *args, **options):

        with open('BodelKunden37.csv', 'rb') as csvfile:
            oReader = csv.DictReader(csvfile, delimiter=';')

            paytype = PayType.objects.get(pk=PayType.INVOICE)

            iRowCounter = 0
            iUserCounter = 0
            iCustomerCounter = 0
            iOnlineInvoiceCounter = 0
            iSepaCounter = 0
            iChildrenCounter = 0
            for dRow in oReader:

                try:

                    # einen User anlegen, falls noch nicht existiert (E-Mail Abgleich)
                    existing_user = User.objects.filter(username=dRow['Nummer'])
                    if not existing_user:
                        new_user = User.objects.create_user(dRow['Nummer'], '', 'aFyohiJpyiUD')
                        iUserCounter += 1
                    else:
                        new_user = existing_user[0]

                    # einen Customer anlegen, falls noch nicht existiert
                    try:
                        customer = new_user.get_profile()
                    except Exception:
                        # create new Customer
                        customer, was_created = Customer.objects.get_or_create(user=new_user)
                        if was_created:
                            iCustomerCounter += 1

                    if dRow['Anrede'] == 'Fam.':
                        customer.title = 'Familie'
                    else:
                        customer.title = dRow['Anrede']
                    customer.name = dRow['Name']
                    customer.surname = dRow['Vorname']
                    customer.email = dRow['E-Mail']
                    customer.street = dRow['Strasse']
                    if len(str(dRow['PLZ'])) < 5:
                        dRow['PLZ'] = "0%s" % dRow['PLZ']
                    if dRow['PLZ'] == 0:
                        dRow['PLZ'] = ''
                    customer.zip_code = dRow['PLZ']
                    customer.city = dRow['Ort']
                    customer.pay_type = paytype
                    customer.save()

                    # Online Rechnung aktivieren
                    online_invoice, was_created = OnlineInvoice.objects.get_or_create(customer=customer)
                    if was_created:
                        iOnlineInvoiceCounter += 1

                    # Kind anlegen und in richtige Einrichtung/Klasse speichern
                    facility = Facility.objects.get(pk=1)
                    facility_subunit = FacilitySubunit.objects.get(pk=1)

                    if dRow['NameKind'] != '':
                        child_birthday = None
                        child, was_created = Child.objects.get_or_create(customer=customer,
                                                                         cust_nbr=dRow['Nummer'],
                                                                         name=dRow['NameKind'],
                                                                         surname=dRow['VornameKind'],
                                                                         birthday=child_birthday,
                                                                         facility=facility, facility_subunit=facility_subunit)
                        if was_created:
                            iChildrenCounter += 1

                    iRowCounter += 1

                except Exception as e:
                    self.stdout.write("Exception: %s\n" % e)

        self.stdout.write("Anzahl Zeilen: %d\n" % iRowCounter)
        self.stdout.write("Angelegte User: %d\n" % iUserCounter)
        self.stdout.write("Angelegte Customer: %d\n" % iCustomerCounter)
        self.stdout.write("Angelegte OnlineInvoices: %d\n" % iOnlineInvoiceCounter)
        self.stdout.write("Angelegte Children: %d\n" % iChildrenCounter)