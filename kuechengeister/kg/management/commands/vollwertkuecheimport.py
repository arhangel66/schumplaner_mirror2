# -*- coding: utf-8 -*-

from datetime import date, datetime

from django.core.management.base import BaseCommand

from kg_utils import ucsv as csv
from kg.models import User, Customer, Child, Facility, FacilitySubunit, SepaMandate, OnlineInvoice, PayType



class Command(BaseCommand):
    help = "Datenimport für Vollwertkueche:\n"

    def handle(self, *args, **options):

        with open('ExportMensamaxGESAMT.csv', 'rb') as csvfile:
            oReader = csv.DictReader(csvfile, delimiter=';')

            paytype = PayType.objects.get(pk=PayType.DEBIT)

            iRowCounter = 0
            iUserCounter = 0
            iCustomerCounter = 0
            iOnlineInvoiceCounter = 0
            iSepaCounter = 0
            iChildrenCounter = 0
            for dRow in oReader:

                try:

                    # einen User anlegen, falls noch nicht existiert (E-Mail Abgleich)
                    existing_user = User.objects.filter(username=dRow['kontoinhaber_email'])
                    if not existing_user:
                        new_user = User.objects.create_user(dRow['kontoinhaber_email'], dRow['kontoinhaber_email'], 'aFyohiJpyiUD')
                        iUserCounter += 1
                    else:
                        new_user = existing_user[0]

                    # einen Customer anlegen, falls noch nicht existiert
                    try:
                        customer = new_user.get_profile()
                    except Exception:
                        # create new Customer
                        customer, was_created = Customer.objects.get_or_create(user=new_user)
                        if was_created:
                            iCustomerCounter += 1

                    customer.title = 'Familie'
                    customer.name = dRow['kontoinhaber_nachname']
                    customer.surname = dRow['kontoinhaber_vorname']
                    customer.email = dRow['kontoinhaber_email']
                    customer.street = "%s %s" % (dRow['kontoinhaber_strasse'], dRow['kontoinhaber_hausnummer'])
                    if len(str(dRow['kontoinhaber_plz'])) < 5:
                        dRow['kontoinhaber_plz'] = "0%s" % dRow['kontoinhaber_plz']
                    if dRow['kontoinhaber_plz'] == 0:
                        dRow['kontoinhaber_plz'] = ''
                    customer.zip_code = dRow['kontoinhaber_plz']
                    customer.city = dRow['kontoinhaber_ort']
                    customer.pay_type = paytype
                    customer.save()

                    # Online Rechnung aktivieren
                    online_invoice, was_created = OnlineInvoice.objects.get_or_create(customer=customer)
                    if was_created:
                        iOnlineInvoiceCounter += 1

                    # SEPA Mandat eintragen -> NUR EIN MANDAT PRO ELTERN
                    existing_sepa_mandate = SepaMandate.objects.filter(customer=customer)
                    if len(existing_sepa_mandate) > 1:
                        self.stdout.write("Doppeltes SEPA Mandat bei Kunde %s, %s" % (customer.name, customer.surname))
                    if not existing_sepa_mandate:
                        sepa_mandate = SepaMandate(customer=customer)
                        sepa_mandate.name = "%s %s" % (customer.surname, customer.name)
                        sepa_mandate.street = customer.street
                        sepa_mandate.city = "%s %s" % (customer.zip_code, customer.city)
                        sepa_mandate.mandate_id = dRow['kontoinhaber_mandatsreferenz']
                        sepa_mandate.iban = dRow['kontoinhaber_iban']
                        sepa_mandate.bic = dRow['kontoinhaber_bic']
                        sepa_mandate.is_valid = True
                        sepa_mandate.on_paper = True
                        sepa_mandate.last_debit_date = date(2015,1,1)
                        sepa_mandate.last_debit_sequence = 'RCUR'
                        sepa_mandate.save(force_insert=True)
                        iSepaCounter += 1

                    # Kind anlegen und in richtige Einrichtung/Klasse speichern
                    if dRow['kind_einrichtung'] == '88.OS' or dRow['kind_einrichtung'] == '88.OS':
                        facility = Facility.objects.get(pk=1)
                    elif dRow['kind_einrichtung'] == 'Jona':
                        facility = Facility.objects.get(pk=2)
                    facility_subunit = FacilitySubunit.objects.filter(facility=facility,
                                                                      name_short=dRow['kind_klasse'])
                    if not facility_subunit or len(facility_subunit) != 1:
                        print "Facility Subunit nicht gefunden oder nicht eindeutig: %s, %s" % (dRow['kind_nachname'], dRow['kind_vorname'])
                    child_birthday = datetime.strptime(dRow['kind_geburtsdatum'], "%d.%m.%Y" )
                    child, was_created = Child.objects.get_or_create(customer=customer, name=dRow['kind_nachname'],
                                                                     surname=dRow['kind_vorname'],
                                                                     birthday=child_birthday,
                                                                     facility=facility, facility_subunit=facility_subunit[0])
                    if was_created:
                        iChildrenCounter += 1

                    iRowCounter += 1

                except Exception as e:
                    self.stdout.write("Exception: %s\n" % e)

        self.stdout.write("Anzahl Zeilen: %d\n" % iRowCounter)
        self.stdout.write("Angelegte User: %d\n" % iUserCounter)
        self.stdout.write("Angelegte Customer: %d\n" % iCustomerCounter)
        self.stdout.write("Angelegte OnlineInvoices: %d\n" % iOnlineInvoiceCounter)
        self.stdout.write("Angelegte SepaMandate: %d\n" % iSepaCounter)
        self.stdout.write("Angelegte Children: %d\n" % iChildrenCounter)