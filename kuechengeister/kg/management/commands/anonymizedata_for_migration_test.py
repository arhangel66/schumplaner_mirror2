# -*- coding: utf-8 -*-

import traceback

from django.core.management.base import BaseCommand, CommandError
from django.core.exceptions import ObjectDoesNotExist

from kg.models import (User, Child, Customer, SepaMandate, ActionLog, InvoiceLog, Facility, FacilitySubunit,
                       FacilityType, CustomerAccountTransaction, Reminder, BankAccountTransaction)


class Command(BaseCommand):
    help = ("Anonymizes all data in the DB.\n"
            "ATTENTION: All original data will be overwritten! Do NOT use on production Server!\n\n")

    def handle(self, *args, **options):
        self.stdout.write("%s" % self.help)
        choice = None
        while not choice:
            val = raw_input("Enter YES if you want to proceed: ")
            choice = str(val)

        if choice != "YES":
            self.stdout.write("Aborted by user!\n")
        else:

            loop_counter = 1
            try:
                self.stdout.write("Anonymize users, customers, children and sepa mandates...\n")
                customers = Customer.objects.all()
                for customer in customers:

                    children = Child.objects.filter(customer=customer)
                    child_counter = 1
                    for oChild in children:
                        oChild.name = 'Kunde%s' % loop_counter
                        oChild.surname = 'Kind%s' % child_counter
                        oChild.birthday = '2000-01-01'
                        oChild.note = ''
                        oChild.rfid_tag = ''
                        oChild.chip_number = ''
                        oChild.chip_deposit = False
                        #oChild.save()
                        child_counter += 1

                    try:
                        user = User.objects.get(customer=customer)
                        user.username = 'kunde%s' % loop_counter
                        user.set_password('test123')
                        user.first_name = ''
                        user.last_name = ''
                        user.email = ''
                        user.save()
                    except ObjectDoesNotExist:
                        pass

                    #customer.name = 'Kunde%s' % loop_counter
                    #customer.surname = 'Vorname'
                    customer.street = 'Musterstraße 33'
                    customer.zip_code = '12345'
                    customer.city = 'Musterstadt'
                    customer.phone = '0371 11111111'
                    customer.phone2 = ''
                    customer.email = 'testaccount@schulmenueplaner.de'
                    customer.note = ''
                    customer.bank_account_owner = ''
                    customer.bank_account = ''
                    customer.bank_icode = ''
                    customer.iban = ''
                    customer.bic = ''
                    customer.save()

                    # anonymize customers sepa mandates
                    SepaMandate.objects.filter(customer=customer)\
                                       .update(name='%s %s' % (customer.surname, customer.name),
                                               street='Musterstraße 33',
                                               city='Musterstadt',
                                               bank='Musterbank',
                                               iban='DE04399340668928275395',
                                               bic='BELADEBEXXX')

                    loop_counter += 1

                self.stdout.write("Anonymize users without customer...\n")
                users = User.objects.exclude(is_staff=True)\
                                    .exclude(is_superuser=True)\
                                    .exclude(username__istartswith='kunde')
                for user in users:
                    user.username = "kunde%s" % loop_counter
                    user.set_password('test123')
                    user.first_name = ""
                    user.last_name = ""
                    user.email = ""
                    #user.save()
                    loop_counter += 1

                self.stdout.write("Delete all users which are not customers...\n")
                #User.objects.exclude(username__istartswith='kunde').delete()

                self.stdout.write("Create an new admin user...\n")
                user = User.objects.create(username='verwaltung',
                                           first_name='',
                                           last_name='',
                                           email='',
                                           is_active=True,
                                           is_staff=True,
                                           is_superuser=True)
                user.set_password('test123')
                #user.save()

                self.stdout.write("Rename facilities...\n")
                facilities = Facility.objects.all()
                facility_counter = 1
                for facility in facilities:
                    if facility.type.id == FacilityType.SCHOOL:
                        facility.name = 'Schule ' + str(facility_counter)
                        facility.name_short = 'S' + str(facility_counter)
                    if facility.type.id == FacilityType.KINDERGARTEN:
                        facility.name = 'Kindergarten ' + str(facility_counter)
                        facility.name_short = 'K' + str(facility_counter)
                    #facility.save()
                    facility_counter += 1

                '''
                self.stdout.write("Rename facility subunits...\n")
                facility_subunits = FacilitySubunit.objects.all()
                for facility_subunit in facility_subunits:
                    if facility_subunit.facility.type.id == FacilityType.SCHOOL:
                        facility_subunit.name = 'Klasse ' + str(facility_subunit.id)
                        facility_subunit.name_short = 'K' + str(facility_subunit.id)
                    if facility_subunit.facility.type.id == FacilityType.KINDERGARTEN:
                        facility_subunit.name = 'Gruppe ' + str(facility_subunit.id)
                        facility_subunit.name_short = 'G' + str(facility_subunit.id)
                    facility_subunit.save()
                '''

                self.stdout.write("Delete action log...\n")
                ActionLog.objects.all().delete()

                self.stdout.write("Delete invoice log...\n")
                #InvoiceLog.objects.all().delete()

                self.stdout.write("Delete customeraccounttransactions...\n")
                #CustomerAccountTransaction.objects.all().delete()

                self.stdout.write("Delete bankaccounttransactions...\n")
                BankAccountTransaction.objects.all().delete()

                self.stdout.write("Delete reminders...\n")
                #Reminder.objects.all().delete()

                self.stdout.write("\nUser data anonymized!\n")
                self.stdout.write("Admin Login: verwaltung\n")
                self.stdout.write("Customer Login: kunde[iii]\n")
                self.stdout.write("Password for all accounts: test123\n")

            except Exception:
                traceback.print_exc()
                raise CommandError("Abortion through Exception\n")
