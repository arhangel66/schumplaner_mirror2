from decimal import Decimal
from django.core.management.base import BaseCommand, CommandError

from kg.models import Customer, CustomerAccountTransaction


class Command(BaseCommand):
    help = "Checks for every customer with 0.00 balance and sets all of his CATs to status BOOKED.\n"

    def handle(self, *args, **options):
        self.stdout.write("%s" % self.help)
        choice = None
        while not choice:
            val = raw_input("Enter YES if you want to proceed: ")
            choice = str(val)

        if choice != "YES":
            self.stdout.write("Aborted by user!\n")
        else:
            iCounter = 0

            try:
                for customer in Customer.objects.filter(account_balance=Decimal('0.00')):

                    test_cat = CustomerAccountTransaction.objects.filter(customer=customer, status=CustomerAccountTransaction.STATUS_OPEN)
                    if len(test_cat) > 0:
                        self.stdout.write("%s\n" % customer)
                        test_cat.update(status=CustomerAccountTransaction.STATUS_BOOKED)
                        iCounter += 1

            except Exception as e:
                raise CommandError("Error\n%s\n" % e.message)

            self.stdout.write("Betroffene Kunden gesamt: %d\n" % iCounter)


