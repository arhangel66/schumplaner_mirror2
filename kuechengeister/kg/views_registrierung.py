# -*- coding: utf-8 -*-

from __future__ import with_statement
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
from django.shortcuts import Http404
from django.utils.log import getLogger

from kg.forms import NewCustomerForm, RegistrationForm, PasswordRecoverForm, SepaMandateForm
from django.contrib.auth.models import User
from django.db import transaction
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.utils.encoding import force_unicode
from kg.models import Customer, ActionLog, PayType
from django.db.models import Q
from settings import (
    SITE_CONTEXT, DEFAULT_FROM_EMAIL, EXIST_CUST_REGISTRATION_TYPE,
    NEW_CUSTOMER_INFORMATION_EMAIL, ACTIVATE_ONLINE_INVOICE_FOR_NEW_CUSTOMERS,
    NEW_CUSTOMER_REGISTRATION
)


logger = getLogger(__name__)


def neuanmeldung(request):
    # Refuse if not allowing new customer registration
    if not NEW_CUSTOMER_REGISTRATION:
        raise Http404

    if request.method == 'POST':

        customer_form = NewCustomerForm(request.POST)
        sepa_form = SepaMandateForm(request.POST, prefix="sepa")

        if customer_form.is_valid():
            if (customer_form.cleaned_data['pay_type'].id != PayType.DEBIT or
                    customer_form.cleaned_data['pay_type'].id == PayType.DEBIT and sepa_form.is_valid()):
            
                with transaction.commit_manually():
                    try:
                        new_passwd = User.objects.make_random_password(length=8)
                        new_user = User.objects.create_user(customer_form.cleaned_data['email'].lower(), customer_form.cleaned_data['email'], new_passwd)
                        new_user.last_name = customer_form.cleaned_data['name']
                        new_user.first_name = customer_form.cleaned_data['surname']
                        new_user.save()

                        new_customer = customer_form.save()
                        new_customer.user = new_user
                        new_customer.save()

                        if new_customer.pay_type.id == PayType.DEBIT:
                            new_sepa_mandate = sepa_form.save(commit=False)
                            new_sepa_mandate.customer = new_customer
                            new_sepa_mandate.save()

                        # mail password
                        sMailText = render_to_string('email/registrierung_neuanmeldung_passwort.html',
                                                     {'title': customer_form.cleaned_data['title'],
                                                      'name': customer_form.cleaned_data['name'],
                                                      'email': customer_form.cleaned_data['email'],
                                                      'password': new_passwd,
                                                      })
                        send_mail('Ihre Anmeldung bei ' + SITE_CONTEXT['SITE_NAME'], sMailText, DEFAULT_FROM_EMAIL, [customer_form.cleaned_data['email']])

                    except:
                        transaction.rollback()
                        logger.exception('Error in new customer registration.')
                        return redirect('kg.views_registrierung.neuanmeldung_error')
                    else:
                        transaction.commit()

                # log action
                ActionLog.objects.log_addition(request=request, object=new_customer, message=u'Kunde neu: %s' % force_unicode(new_customer))

                # check for duplicate customers; log warning if found one
                try:
                    oDuplicateCustomers = Customer.objects.filter(name=new_customer.name, surname=new_customer.surname)
                    # because new customer is still saved, the search should find one minimum
                    # duplicate customer is detected if search finds more than one
                    if oDuplicateCustomers.count() > 1:
                        ActionLog.objects.log_warning(request=request, object=new_customer, message=u'Kunde existiert bereits: %s' % force_unicode(new_customer))
                except:
                    pass


                # send information mail to configured admin mailaddress
                try:
                    if NEW_CUSTOMER_INFORMATION_EMAIL:
                        sInfoMailText = render_to_string('email/registrierung_admin_information.html', {'customer' : new_customer})
                        send_mail('Neuanmeldung Kunde', sInfoMailText, DEFAULT_FROM_EMAIL, NEW_CUSTOMER_INFORMATION_EMAIL)
                except Exception as exc:
                    logger.exception('Error in trying to send information email about new customer registration.')

                return redirect('kg.views_registrierung.neuanmeldung_success')

    else:
        customer_form = NewCustomerForm()
        sepa_form = SepaMandateForm(prefix="sepa")

    return render_to_response('kg/registrierung_neuanmeldung.html',
                              {'customer_form': customer_form,
                               'sepa_form': sepa_form},
                              context_instance=RequestContext(request))


def neuanmeldung_success(request): 
    return render_to_response('kg/registrierung_neuanmeldung_status.html', {'status': 'success'}, context_instance=RequestContext(request))


def neuanmeldung_error(request): 
    return render_to_response('kg/registrierung_neuanmeldung_status.html', {'status' : 'error'}, context_instance = RequestContext(request))


def registrierung(request):
    
    if request.method == 'POST':
        
        form = RegistrationForm(request.POST)
        
        if form.is_valid():
            with transaction.commit_manually():
                try:
                    
                    if EXIST_CUST_REGISTRATION_TYPE == 'INVOICENUMBER':
                        sInvoiceNumber = form.cleaned_data['rnum']
                        if len(sInvoiceNumber)  < 5:
                            sInvoiceNumber = '999999'
                        sInvoiceNumber = sInvoiceNumber[-5:]
                        oCustomer = Customer.objects.get(child__cust_nbr=sInvoiceNumber)
                    
                    else:
                        oCustomer = Customer.objects.get(child__cust_nbr=form.cleaned_data['rnum'])

                    # check if form name matches user name
                    if oCustomer.name.strip() != form.cleaned_data['name'].strip():
                        raise StandardError
                    
                    # check if customer is still registered
                    if oCustomer.user is not None:
                        raise StandardError

                    oCustomer.email = form.cleaned_data['email']
                    sNewPasswd = User.objects.make_random_password(length=8)
                    oNewUser = User.objects.create_user(form.cleaned_data['email'].lower(), form.cleaned_data['email'], sNewPasswd)
                    oNewUser.last_name = oCustomer.name
                    oNewUser.first_name = oCustomer.surname
                    oNewUser.save()
                    oCustomer.user = oNewUser
                    oCustomer.save()
                    # mail password
                    mail_text = render_to_string('email/registrierung_bestandskunde_passwort.html',
                                                 {'title'     : oCustomer.title,
                                                  'name'      : oCustomer.name,
                                                  'email'     : oCustomer.email,
                                                  'password'  : sNewPasswd,
                                                 })
                    send_mail('Ihre Anmeldung bei ' + SITE_CONTEXT['SITE_NAME'], mail_text, DEFAULT_FROM_EMAIL, [form.cleaned_data['email']])
                except:
                    transaction.rollback()
                    return redirect('kg.views_registrierung.registrierung_error')

                else:
                    transaction.commit()

            # log action
            ActionLog.objects.log_addition(request=request, object=oCustomer, message=u'Anmeldung Bestandskunde: %s' % force_unicode(oCustomer))

            # send information mail to configured mailaddress
            try:
                if NEW_CUSTOMER_INFORMATION_EMAIL != '':
                    mail_text = render_to_string('email/registrierung_admin_information.html', {'customer' : oCustomer})
                    send_mail('Anmeldung Bestandskunde', mail_text, DEFAULT_FROM_EMAIL, [NEW_CUSTOMER_INFORMATION_EMAIL])
            except:
                pass

            return redirect('kg.views_registrierung.registrierung_success')

    else:
        form = RegistrationForm()
        
    return render_to_response('kg/registrierung_bestandskunde.html', {'form': form}, context_instance=RequestContext(request))


def registrierung_success(request): 
    return render_to_response('kg/registrierung_bestandskunde_status.html', {'status': 'success'}, context_instance=RequestContext(request))


def registrierung_error(request):
    return render_to_response('kg/registrierung_bestandskunde_status.html', {'status': 'error'}, context_instance=RequestContext(request))


def password_lost(request):
    

    # form sent    
    if request.method == 'POST':

        form = PasswordRecoverForm(request.POST)
        
        if form.is_valid():

            user = User.objects.get( Q(username__iexact=form.cleaned_data['email']) | Q(email__iexact=form.cleaned_data['email']))
            
            with transaction.commit_manually():
                try:
                    
                    new_passwd_raw = User.objects.make_random_password(length=8)
                    token = User.objects.make_random_password(length=30)
                    
                    # customer exists
                    try:
                        customer = Customer.objects.get(user=user)
                        customer_exists = True
                        title = customer.title
                        name  = customer.name
                        
                        old_passwd = user.password
                        user.set_password(new_passwd_raw)
                        new_passwd = user.password
                        user.password = old_passwd
                        customer.new_password = new_passwd
                        customer.new_password_check = token
                        customer.save()
                        
                    # customer exists not / admins for example
                    except:
                        customer_exists = False
                        title = ''
                        name = user.first_name + ' ' + user.last_name
                        new_passwd = ''
                        user.set_password(new_passwd_raw)

                    user.save()

                    mail_text = render_to_string('email/passwort_vergessen.html',
                                                     {'title'     : title,
                                                      'name'      : name,
                                                      'email'     : user.email,
                                                      'token'     : token,
                                                      'password'  : new_passwd_raw,
                                                      'customer_exists' : customer_exists,
                                                      }
                                                 )
                    send_mail('Ihr neues Passwort für ' + SITE_CONTEXT['SITE_NAME'], mail_text, DEFAULT_FROM_EMAIL, [user.email,])
                    
                except:
                    transaction.rollback()
                    return redirect('kg.views_registrierung.password_error')
                else:
                    transaction.commit()
                    return redirect('kg.views_registrierung.password_success')

    # activating new password
    elif "token" in request.GET:
        with transaction.commit_manually():
            try:
                token =  request.GET.get('token')
                customer = Customer.objects.get(new_password_check = token)
                user = customer.user
                user.password = customer.new_password
                user.save()
                customer.new_password = ''
                customer.new_password_check = ''
                customer.save()
            except:
                transaction.rollback()
                return redirect('kg.views_registrierung.password_error')
            else:
                transaction.commit()
                return redirect('kg.views_registrierung.password_changed')
        
    else:
        form = PasswordRecoverForm()
        
        
    return render_to_response('kg/passwort.html', {'form' : form}, context_instance = RequestContext(request))


def password_success(request):
    return render_to_response('kg/passwort_status.html', {'status': 'success'}, context_instance = RequestContext(request))


def password_error(request):
    return render_to_response('kg/passwort_status.html', {'status': 'error'}, context_instance = RequestContext(request))


def password_changed(request):
    return render_to_response('kg/passwort_status.html', {'status': 'changed'}, context_instance = RequestContext(request))
