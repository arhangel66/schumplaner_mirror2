from django.utils.log import getLogger
from django.db import transaction

from jobtastic import JobtasticTask

from kg import celery_app
from kg.models import Child, InvoiceLog
from kg_utils.invoice_generator import InvoiceGenerator


logger = getLogger(__name__)


class InvoiceGenerationTask(JobtasticTask):
    # These are the Task kwargs that matter for caching purposes
    significant_kwargs = [
        ('child_ids', str),
        ('year', str),
        ('month', str),
    ]
    herd_avoidance_timeout = 60  # Shouldn't take more than 60 seconds
    cache_duration = 0  # Cache these results forever. Math is pretty stable.

    def calculate_result(self, child_ids, year, month, **kwargs):
        children_total = len(child_ids)
        count = 0
        sid = transaction.savepoint()
        try:
            for count, child_id in enumerate(child_ids):
                child = Child.objects.get(pk=child_id)
                invoice = InvoiceGenerator(child, year, month, None)
                invoice.save_invoice()
                self.update_progress(count+1, children_total)
            transaction.savepoint_commit(sid)
        except:
            transaction.savepoint_rollback(sid)
            logger.exception('Failed to generate Invoice')
            raise
        return {'invoice_count': count+1}
