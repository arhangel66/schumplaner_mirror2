# -*- coding: utf-8 -*-
from __future__ import with_statement, absolute_import
from datetime import datetime

from django.utils import simplejson
from django.utils.log import getLogger
from django.http import HttpResponse
from django.conf import settings
from django.template.loader import render_to_string
from django.core.mail import send_mail

from celery.result import EagerResult

from kg.models import InvoiceLog, Child, Customer, PayType
from kg.tasks import InvoiceGenerationTask
from settings import OVERLOAD_SITE


logger = getLogger(__name__)


def perform_billing(year, month, btype, bid, request):
    """Perform generating bills for child or children of given facility."""

    def count_invoice():
        params = {
            'invoice_date__year': year,
            'invoice_date__month': month,
        }
        if btype == 'facility':
            params.update({'child__facility__id': bid})
        else:
            params.update({'child_id': bid})
        invoiced = InvoiceLog.objects.filter(**params).count()
        released = InvoiceLog.objects.released().filter(**params).count()
        return invoiced, released

    try:
        if btype == 'facility':
            qsChildrenExistingInvoice = Child.objects.filter(
                facility__id=bid,
                invoicelog__invoice_date__year=year,
                invoicelog__invoice_date__month=month
                ).values_list('id', flat=True)
            qsChildrenWithoutInvoice = Child.objects.distinct().filter(
                facility__id=bid,
                order__quantity__gt=0,
                order__menu__date__year=year,
                order__menu__date__month=month
                ).exclude(id__in=qsChildrenExistingInvoice)

            # Abraxas: exclude children with extra_attribute 'Hort' and 'GGB'
            if OVERLOAD_SITE == 'abraxas':
                qsChildrenWithoutInvoice = qsChildrenWithoutInvoice.exclude(extra_attribute__exact='Hort')
                qsChildrenWithoutInvoice = qsChildrenWithoutInvoice.exclude(extra_attribute__exact='GGB')

            # Sonneblick: exclude interns
            if OVERLOAD_SITE == 'sonnenblick':
                qsChildrenWithoutInvoice = qsChildrenWithoutInvoice.exclude(customer__pay_type__id=PayType.OTHER)

        if btype == 'child':
            qsChildrenWithoutInvoice = Child.objects.filter(pk=bid)

        child_ids = qsChildrenWithoutInvoice.values_list('id', flat=True)
        result = InvoiceGenerationTask.delay_or_eager(
            child_ids=child_ids, year=year, month=month)
        if isinstance(result, EagerResult):
            if result.failed():
                raise Exception('Failed to create invoice')
            result = result
    except:
        logger.exception('Error when generating invoice PDF')
        iInvoiceCount, iReleaseCount = count_invoice()
        return HttpResponse(simplejson.dumps({
            'error': True,
            'code': request.GET.get('value'),
            'billing_status': 'Fehler',
            'invoice_count': iInvoiceCount,
            'release_count': iReleaseCount
        }))

    iInvoiceCount, iReleaseCount = count_invoice()
    if btype == 'facility':
        download_param = ''
    else:
        download_param = 'year={0}&month={1}&child_id={2}'.format(
            year, month, bid)

    return HttpResponse(
        simplejson.dumps({
            'error': False,
            'code': request.GET.get('value'),
            'billing_status': 'OK',
            'invoice_count': iInvoiceCount,
            'release_count': iReleaseCount,
            'download_param': download_param,
            'task': getattr(result, 'task_id', None)
        }),
        mimetype='application/javascript')


def perform_release(year, month, btype, bid, request):
    try:
        # retrieve invoices logs
        oInvoiceLogs = InvoiceLog.objects.select_related().filter(
            invoice_date__year=year,
            invoice_date__month=month,
            child__facility__id=bid,
            released=None)

        for oLog in oInvoiceLogs:
            oChild = oLog.child
            oCustomer = Customer.objects.get(child=oChild)

            # set released status
            oLog.released = datetime.now()

            # send mail if customer has online invoice activated and mail was
            # not already sent (only for customers with active login)
            customer_valid = (
                oCustomer.user is not None and oCustomer.user.is_active and
                oCustomer.email and not oCustomer.paper_invoice
                )
            if customer_valid and oLog.notification_sent is None:
                try:
                    # send invoice notification mail if not in DEMO modus
                    if not settings.DEMO_MODUS:
                        sMailText = render_to_string(
                            'email/rechnung_information.html',
                            {
                                'child': oChild,
                                'customer': oCustomer,
                                'invoice_log': oLog
                            }
                        )
                        sMailSubject = \
                            'Ihre Rechnung Online, Rechnungs-Nr. %s vom %s' % (
                                oLog.invoice_number,
                                oLog.invoice_date.strftime('%d.%m.%Y'))
                        send_mail(
                            sMailSubject, sMailText,
                            settings.DEFAULT_FROM_EMAIL, [oCustomer.email])

                    # set time
                    oLog.notification_sent = datetime.now()
                    oLog.save()

                except:
                    # TODO: log errors
                    logger.exception(u'Error when releasing invoice: {}'.format(oLog))
                    continue

            # save otherwise
            else:
                oLog.save()

    except:
        logger.exception('Error when releasing invoices')
        iInvoiceCount = InvoiceLog.objects.filter(
            child__facility__id=bid, invoice_date__year=year,
            invoice_date__month=month).count()
        iReleaseCount = InvoiceLog.objects.released().filter(
            child__facility__id=bid, invoice_date__year=year,
            invoice_date__month=month).count()
        return HttpResponse(
            simplejson.dumps({
                'error': True,
                'code': request.GET.get('value'),
                'release_status': 'Fehler',
                'invoice_count': iInvoiceCount,
                'release_count': iReleaseCount
            }),
            mimetype='application/javascript')

    else:
        iInvoiceCount = InvoiceLog.objects.filter(
            child__facility__id=bid,
            invoice_date__year=year,
            invoice_date__month=month).count()
        iReleaseCount = InvoiceLog.objects.released().filter(
            child__facility__id=bid,
            invoice_date__year=year,
            invoice_date__month=month).count()
        return HttpResponse(
            simplejson.dumps({
                'error': False,
                'code': request.GET.get('value'),
                'release_status': 'OK',
                'invoice_count': iInvoiceCount,
                'release_count': iReleaseCount
            }),
            mimetype='application/javascript')
