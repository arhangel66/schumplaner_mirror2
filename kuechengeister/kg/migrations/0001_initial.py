# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

from settings import OVERLOAD_SITE

class Migration(SchemaMigration):

    depends_on = (
        ("menus", "0001_initial"),
    )

    migration_instances = ['demo', 'leibundseele']

    def forwards(self, orm):
        """"
        Updates the DB to the master14 initial state from the latest state in master branch
        if instance is in migation instances. Otherwise creates initial migration for master14 branch.

        Before migrating master to master14 run the following SQL on DB:
          DELETE FROM south_migrationhistory WHERE app_name LIKE 'kg'
        """
        from django.db import connection
        if OVERLOAD_SITE in self.migration_instances and \
           'kg_allergen' in connection.introspection.table_names():
                # Removing unique constraints
                db.delete_unique('kg_menu', ['mealtime_id', 'date'])
                db.delete_unique('kg_price', ['facility_type_id', 'price', 'price_group_id', 'start_date'])

                # Deleting models transfered to menus
                db.delete_table('kg_mealtimecategory')
                db.delete_table('kg_allergen')
                db.delete_table('kg_mealcategory')
                db.delete_table('kg_pricegroup')
                db.delete_table(db.shorten_name('kg_pricegroup_mealtimes'))
                db.delete_table('kg_price')
                db.delete_table('kg_meal')
                db.delete_table('kg_menu')
                db.delete_table('kg_mealtime')

                # Update kg.Flatrate
                db.delete_unique('kg_flatrate', ['child_id', 'mealtime_id', 'weekday'])
                db.rename_column('kg_flatrate', 'mealtime_id', 'meal_id')
                db.alter_column('kg_flatrate', 'meal_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['menus.Meal']))
                db.create_unique('kg_flatrate', ['child_id', 'meal_id', 'weekday'])

                # Switch to using menus.Menu
                db.alter_column('kg_order', 'menu_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['menus.Menu']))
                db.alter_column('kg_orderhistory', 'menu_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['menus.Menu']))

                # Make sure the fields are not null
                db.alter_column('kg_facilitysubunit', 'mealplan_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['menus.MealPlan']))
                db.alter_column('kg_facilitysubunit', 'price_group_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['menus.PriceGroup']))

                # Removing M2M table for field mealtimes on 'Facility'
                db.delete_table(db.shorten_name('kg_facility_mealtimes'))

                # Prevent normal flow of migration be executed
                return

        # In case of not existing, all the tables will be created normally

        # Adding model 'FacilityType'
        db.create_table('kg_facilitytype', (
            ('id', self.gf('django.db.models.fields.PositiveSmallIntegerField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('name_short', self.gf('django.db.models.fields.CharField')(max_length=20, blank=True)),
        ))
        db.send_create_signal('kg', ['FacilityType'])

        # Adding model 'Facility'
        db.create_table('kg_facility', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('is_active', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('name_short', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['kg.FacilityType'])),
            ('flatrate_customizable', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('tax_rate', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=19)),
        ))
        db.send_create_signal('kg', ['Facility'])

        # Adding model 'FacilitySubunit'
        db.create_table('kg_facilitysubunit', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('name_short', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('facility', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['kg.Facility'])),
            ('mealplan', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['menus.MealPlan'])),
            ('price_group', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['menus.PriceGroup'])),
            ('sortKey', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
        ))
        db.send_create_signal('kg', ['FacilitySubunit'])

        # Adding model 'Reduction'
        db.create_table('kg_reduction', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('reduction', self.gf('django.db.models.fields.DecimalField')(max_digits=4, decimal_places=2)),
        ))
        db.send_create_signal('kg', ['Reduction'])

        # Adding model 'PayType'
        db.create_table('kg_paytype', (
            ('id', self.gf('django.db.models.fields.PositiveSmallIntegerField')(primary_key=True)),
            ('new_customers', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('kg', ['PayType'])

        # Adding model 'CancelReason'
        db.create_table('kg_cancelreason', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('name_short', self.gf('django.db.models.fields.CharField')(max_length=20, blank=True)),
        ))
        db.send_create_signal('kg', ['CancelReason'])

        # Adding model 'Customer'
        db.create_table('kg_customer', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['auth.User'], unique=True, null=True, blank=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('surname', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('street', self.gf('django.db.models.fields.CharField')(max_length=50, null=True)),
            ('zip_code', self.gf('django.db.models.fields.CharField')(max_length=50, null=True)),
            ('city', self.gf('django.db.models.fields.CharField')(max_length=50, null=True)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('phone2', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75, null=True, blank=True)),
            ('pay_type', self.gf('django.db.models.fields.related.ForeignKey')(default=2, to=orm['kg.PayType'])),
            ('bank_account_owner', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('sepa_mandate_id', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('bank_account', self.gf('django.db.models.fields.CharField')(max_length=10, null=True, blank=True)),
            ('bank_icode', self.gf('django.db.models.fields.CharField')(max_length=8, null=True, blank=True)),
            ('iban', self.gf('django.db.models.fields.CharField')(max_length=34, blank=True)),
            ('bic', self.gf('django.db.models.fields.CharField')(max_length=11, blank=True)),
            ('note', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('is_suspended', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('suspension_reason', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('new_password', self.gf('django.db.models.fields.CharField')(max_length=128, blank=True)),
            ('new_password_check', self.gf('django.db.models.fields.CharField')(max_length=30, blank=True)),
            ('credit_minimum', self.gf('django.db.models.fields.DecimalField')(default='20.00', max_digits=5, decimal_places=2)),
            ('account_balance', self.gf('django.db.models.fields.DecimalField')(default=0, max_digits=8, decimal_places=2)),
            ('paper_invoice', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('last_modified', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
        ))
        db.send_create_signal('kg', ['Customer'])

        # Adding model 'CustomerAccountTransaction'
        db.create_table('kg_customeraccounttransaction', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('customer', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['kg.Customer'])),
            ('bat_hash', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('transaction_id', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('amount', self.gf('django.db.models.fields.DecimalField')(default=0, max_digits=8, decimal_places=2)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=1000, blank=True)),
            ('type', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('transaction_date', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('status', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('connected_to', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('invoice_id', self.gf('django.db.models.fields.PositiveIntegerField')(default=0)),
            ('reminder_id', self.gf('django.db.models.fields.PositiveIntegerField')(default=0)),
            ('on_next_invoice', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('last_modified', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
        ))
        db.send_create_signal('kg', ['CustomerAccountTransaction'])

        # Adding model 'BankAccountTransaction'
        db.create_table('kg_bankaccounttransaction', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('t_hash', self.gf('django.db.models.fields.CharField')(unique=True, max_length=50)),
            ('t_date', self.gf('django.db.models.fields.DateField')()),
            ('booking_type', self.gf('django.db.models.fields.CharField')(default='', max_length=100, blank=True)),
            ('subject', self.gf('django.db.models.fields.CharField')(max_length=500, blank=True)),
            ('amount', self.gf('django.db.models.fields.DecimalField')(default=0, max_digits=8, decimal_places=2)),
            ('type', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('bank_account_owner', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('bank_account_number', self.gf('django.db.models.fields.CharField')(max_length=34, blank=True)),
            ('bank_icode', self.gf('django.db.models.fields.CharField')(max_length=11, blank=True)),
            ('status', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('last_modified', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
        ))
        db.send_create_signal('kg', ['BankAccountTransaction'])

        # Adding model 'Child'
        db.create_table('kg_child', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('customer', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['kg.Customer'], null=True)),
            ('cust_nbr', self.gf('django.db.models.fields.IntegerField')(unique=True, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('surname', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('birthday', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('facility', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['kg.Facility'], null=True)),
            ('facility_subunit', self.gf('smart_selects.db_fields.ChainedForeignKey')(to=orm['kg.FacilitySubunit'], null=True)),
            ('flatrate', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('note', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('rfid_tag', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('rfid_tag_update_time', self.gf('django.db.models.fields.IntegerField')(default=0, blank=True)),
            ('chip_number', self.gf('django.db.models.fields.CharField')(default='', max_length=10, blank=True)),
            ('chip_number_update_time', self.gf('django.db.models.fields.IntegerField')(default=0, blank=True)),
            ('chip_deposit', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('chip_deposit_update_time', self.gf('django.db.models.fields.IntegerField')(default=0, blank=True)),
            ('last_modified', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
        ))
        db.send_create_signal('kg', ['Child'])

        # Adding M2M table for field allergen on 'Child'
        m2m_table_name = db.shorten_name('kg_child_allergen')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('child', models.ForeignKey(orm['kg.child'], null=False)),
            ('allergen', models.ForeignKey(orm['menus.allergen'], null=False))
        ))
        db.create_unique(m2m_table_name, ['child_id', 'allergen_id'])

        # Adding model 'ChildReduction'
        db.create_table('kg_childreduction', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('child', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['kg.Child'])),
            ('reduction', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['kg.Reduction'])),
            ('facility_type', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('reference_number', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('date_start', self.gf('django.db.models.fields.DateField')()),
            ('date_end', self.gf('django.db.models.fields.DateField')()),
        ))
        db.send_create_signal('kg', ['ChildReduction'])

        # Adding model 'Holiday'
        db.create_table('kg_holiday', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('date_start', self.gf('django.db.models.fields.DateField')()),
            ('date_end', self.gf('django.db.models.fields.DateField')()),
        ))
        db.send_create_signal('kg', ['Holiday'])

        # Adding model 'SepaMandate'
        db.create_table('kg_sepamandate', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('customer', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['kg.Customer'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('street', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('city', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('mandate_id', self.gf('django.db.models.fields.CharField')(default='MA', unique=True, max_length=30)),
            ('bank', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('iban', self.gf('django.db.models.fields.CharField')(max_length=34)),
            ('bic', self.gf('django.db.models.fields.CharField')(max_length=11)),
            ('is_valid', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('on_paper', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('note', self.gf('django.db.models.fields.TextField')(default='', blank=True)),
            ('last_debit_date', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('last_debit_sequence', self.gf('django.db.models.fields.CharField')(default='', max_length=4, blank=True)),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('last_modified', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
        ))
        db.send_create_signal('kg', ['SepaMandate'])

        # Adding model 'InvoiceLog'
        db.create_table('kg_invoicelog', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('child', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['kg.Child'])),
            ('facility', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['kg.Facility'], null=True)),
            ('invoice_date', self.gf('django.db.models.fields.DateField')()),
            ('invoice_number', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('invoice_subtotal', self.gf('django.db.models.fields.DecimalField')(default=0, max_digits=5, decimal_places=2)),
            ('invoice_total', self.gf('django.db.models.fields.DecimalField')(default=0, max_digits=5, decimal_places=2)),
            ('invoice_file', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('pay_type_id', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('pay_until', self.gf('django.db.models.fields.DateField')(default=datetime.datetime(1970, 1, 1, 0, 0))),
            ('bank_account_owner', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('bank_account_iban', self.gf('django.db.models.fields.CharField')(max_length=34, blank=True)),
            ('bank_account_bic', self.gf('django.db.models.fields.CharField')(max_length=11, blank=True)),
            ('sepa_mandate_reference', self.gf('django.db.models.fields.CharField')(default='', max_length=35, blank=True)),
            ('sepa_transaction_id', self.gf('django.db.models.fields.CharField')(default='', max_length=50, blank=True)),
            ('sepa_mandate_date', self.gf('django.db.models.fields.DateField')(null=True)),
            ('sepa_description', self.gf('django.db.models.fields.CharField')(default='', max_length=140, blank=True)),
            ('sepa_seq_type', self.gf('django.db.models.fields.CharField')(default='', max_length=4, blank=True)),
            ('first_downloaded', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('notification_sent', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('released', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('cat_created', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('kg', ['InvoiceLog'])

        # Adding unique constraint on 'InvoiceLog', fields ['child', 'invoice_date']
        db.create_unique('kg_invoicelog', ['child_id', 'invoice_date'])

        # Adding model 'Reminder'
        db.create_table('kg_reminder', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('customer', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['kg.Customer'])),
            ('number', self.gf('django.db.models.fields.CharField')(max_length=20, blank=True)),
            ('parent', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='children', null=True, to=orm['kg.Reminder'])),
            ('content', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('type', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=1)),
            ('status', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('create_date', self.gf('django.db.models.fields.DateField')()),
            ('pay_until', self.gf('django.db.models.fields.DateField')()),
            ('total', self.gf('django.db.models.fields.DecimalField')(default=0, max_digits=5, decimal_places=2)),
            ('filename', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('first_downloaded', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
        ))
        db.send_create_signal('kg', ['Reminder'])

        # Adding model 'Order'
        db.create_table('kg_order', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('child', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['kg.Child'])),
            ('menu', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['menus.Menu'])),
            ('created_by', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='created_orders', null=True, to=orm['auth.User'])),
            ('modified_by', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='modified_orders', null=True, to=orm['auth.User'])),
            ('last_modified', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('canceled', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('cancel_reason', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['kg.CancelReason'], null=True, blank=True)),
            ('price', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=4, decimal_places=2, blank=True)),
            ('reduction', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['kg.Reduction'], null=True, blank=True)),
            ('pickup_time', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal('kg', ['Order'])

        # Adding unique constraint on 'Order', fields ['child', 'menu']
        db.create_unique('kg_order', ['child_id', 'menu_id'])

        # Adding model 'OrderHistory'
        db.create_table('kg_orderhistory', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('child', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['kg.Child'])),
            ('menu', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['menus.Menu'])),
            ('modified_by', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='modified_order_history', null=True, to=orm['auth.User'])),
            ('last_modified', self.gf('django.db.models.fields.DateTimeField')(blank=True)),
            ('canceled', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('kg', ['OrderHistory'])

        # Adding model 'Flatrate'
        db.create_table('kg_flatrate', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('child', self.gf('django.db.models.fields.related.ForeignKey')(related_name='flat', to=orm['kg.Child'])),
            ('meal', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['menus.Meal'])),
            ('weekday', self.gf('django.db.models.fields.SmallIntegerField')()),
            ('created_by', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='created_flatrate', null=True, to=orm['auth.User'])),
            ('modified_by', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='modified_flatrate', null=True, to=orm['auth.User'])),
            ('last_modified', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('active_on_holidays', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal('kg', ['Flatrate'])

        # Adding unique constraint on 'Flatrate', fields ['child', 'meal', 'weekday']
        db.create_unique('kg_flatrate', ['child_id', 'meal_id', 'weekday'])

        # Adding model 'ActionLog'
        db.create_table('kg_actionlog', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('action_time', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'], null=True)),
            ('content_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contenttypes.ContentType'], null=True, blank=True)),
            ('object_id', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('action_flag', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('change_message', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal('kg', ['ActionLog'])

        # Adding model 'ViewPermission'
        db.create_table('kg_viewpermission', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('dummy_content', self.gf('django.db.models.fields.CharField')(max_length=1)),
        ))
        db.send_create_signal('kg', ['ViewPermission'])

        # Adding model 'SepaDebitDownload'
        db.create_table('kg_sepadebitdownload', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('first_downloaded', self.gf('django.db.models.fields.DateTimeField')()),
            ('message_id', self.gf('django.db.models.fields.CharField')(unique=True, max_length=35)),
            ('sequence_type', self.gf('django.db.models.fields.CharField')(max_length=4)),
        ))
        db.send_create_signal('kg', ['SepaDebitDownload'])

        # Adding model 'SepaDebit'
        db.create_table('kg_sepadebit', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('customer', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['kg.Customer'])),
            ('submission_time', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('endtoend_id', self.gf('django.db.models.fields.CharField')(unique=True, max_length=35)),
            ('sequence_type', self.gf('django.db.models.fields.CharField')(max_length=4)),
            ('amount', self.gf('django.db.models.fields.DecimalField')(default=0, max_digits=5, decimal_places=2)),
            ('description', self.gf('django.db.models.fields.CharField')(default='', max_length=140, blank=True)),
            ('bank_account_owner', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('bank_account_iban', self.gf('django.db.models.fields.CharField')(max_length=34)),
            ('bank_account_bic', self.gf('django.db.models.fields.CharField')(max_length=11)),
            ('mandate_reference', self.gf('django.db.models.fields.CharField')(max_length=35)),
            ('mandate_date', self.gf('django.db.models.fields.DateField')()),
            ('debit_received', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('download', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['kg.SepaDebitDownload'], null=True)),
        ))
        db.send_create_signal('kg', ['SepaDebit'])

    def backwards(self, orm):
        # Removing unique constraint on 'Flatrate', fields ['child', 'meal', 'weekday']
        db.delete_unique('kg_flatrate', ['child_id', 'meal_id', 'weekday'])

        # Removing unique constraint on 'Order', fields ['child', 'menu']
        db.delete_unique('kg_order', ['child_id', 'menu_id'])

        # Removing unique constraint on 'InvoiceLog', fields ['child', 'invoice_date']
        db.delete_unique('kg_invoicelog', ['child_id', 'invoice_date'])

        # Deleting model 'FacilityType'
        db.delete_table('kg_facilitytype')

        # Deleting model 'Facility'
        db.delete_table('kg_facility')

        # Deleting model 'FacilitySubunit'
        db.delete_table('kg_facilitysubunit')

        # Deleting model 'Reduction'
        db.delete_table('kg_reduction')

        # Deleting model 'PayType'
        db.delete_table('kg_paytype')

        # Deleting model 'CancelReason'
        db.delete_table('kg_cancelreason')

        # Deleting model 'Customer'
        db.delete_table('kg_customer')

        # Deleting model 'CustomerAccountTransaction'
        db.delete_table('kg_customeraccounttransaction')

        # Deleting model 'BankAccountTransaction'
        db.delete_table('kg_bankaccounttransaction')

        # Deleting model 'Child'
        db.delete_table('kg_child')

        # Removing M2M table for field allergen on 'Child'
        db.delete_table(db.shorten_name('kg_child_allergen'))

        # Deleting model 'ChildReduction'
        db.delete_table('kg_childreduction')

        # Deleting model 'Holiday'
        db.delete_table('kg_holiday')

        # Deleting model 'SepaMandate'
        db.delete_table('kg_sepamandate')

        # Deleting model 'InvoiceLog'
        db.delete_table('kg_invoicelog')

        # Deleting model 'Reminder'
        db.delete_table('kg_reminder')

        # Deleting model 'Order'
        db.delete_table('kg_order')

        # Deleting model 'OrderHistory'
        db.delete_table('kg_orderhistory')

        # Deleting model 'Flatrate'
        db.delete_table('kg_flatrate')

        # Deleting model 'ActionLog'
        db.delete_table('kg_actionlog')

        # Deleting model 'ViewPermission'
        db.delete_table('kg_viewpermission')

        # Deleting model 'SepaDebitDownload'
        db.delete_table('kg_sepadebitdownload')

        # Deleting model 'SepaDebit'
        db.delete_table('kg_sepadebit')


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '75'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'kg.actionlog': {
            'Meta': {'ordering': "('-action_time',)", 'object_name': 'ActionLog'},
            'action_flag': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'action_time': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'change_message': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'object_id': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']", 'null': 'True'})
        },
        'kg.bankaccounttransaction': {
            'Meta': {'ordering': "['-t_date', '-amount']", 'object_name': 'BankAccountTransaction'},
            'amount': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '8', 'decimal_places': '2'}),
            'bank_account_number': ('django.db.models.fields.CharField', [], {'max_length': '34', 'blank': 'True'}),
            'bank_account_owner': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'bank_icode': ('django.db.models.fields.CharField', [], {'max_length': '11', 'blank': 'True'}),
            'booking_type': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_modified': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'subject': ('django.db.models.fields.CharField', [], {'max_length': '500', 'blank': 'True'}),
            't_date': ('django.db.models.fields.DateField', [], {}),
            't_hash': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'type': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'})
        },
        'kg.cancelreason': {
            'Meta': {'ordering': "['name']", 'object_name': 'CancelReason'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name_short': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'})
        },
        'kg.child': {
            'Meta': {'ordering': "['name', 'surname']", 'object_name': 'Child'},
            'allergen': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['menus.Allergen']", 'null': 'True', 'blank': 'True'}),
            'birthday': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'chip_deposit': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'chip_deposit_update_time': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'chip_number': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '10', 'blank': 'True'}),
            'chip_number_update_time': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'cust_nbr': ('django.db.models.fields.IntegerField', [], {'unique': 'True', 'blank': 'True'}),
            'customer': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['kg.Customer']", 'null': 'True'}),
            'facility': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['kg.Facility']", 'null': 'True'}),
            'facility_subunit': ('smart_selects.db_fields.ChainedForeignKey', [], {'to': "orm['kg.FacilitySubunit']", 'null': 'True'}),
            'flatrate': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_modified': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'note': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'rfid_tag': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'rfid_tag_update_time': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'surname': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'kg.childreduction': {
            'Meta': {'ordering': "['child__name']", 'object_name': 'ChildReduction'},
            'child': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['kg.Child']"}),
            'date_end': ('django.db.models.fields.DateField', [], {}),
            'date_start': ('django.db.models.fields.DateField', [], {}),
            'facility_type': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'reduction': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['kg.Reduction']"}),
            'reference_number': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'})
        },
        'kg.customer': {
            'Meta': {'ordering': "['name', 'surname']", 'object_name': 'Customer'},
            'account_balance': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '8', 'decimal_places': '2'}),
            'bank_account': ('django.db.models.fields.CharField', [], {'max_length': '10', 'null': 'True', 'blank': 'True'}),
            'bank_account_owner': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'bank_icode': ('django.db.models.fields.CharField', [], {'max_length': '8', 'null': 'True', 'blank': 'True'}),
            'bic': ('django.db.models.fields.CharField', [], {'max_length': '11', 'blank': 'True'}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True'}),
            'credit_minimum': ('django.db.models.fields.DecimalField', [], {'default': "'20.00'", 'max_digits': '5', 'decimal_places': '2'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True', 'blank': 'True'}),
            'iban': ('django.db.models.fields.CharField', [], {'max_length': '34', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_suspended': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_modified': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'new_password': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'new_password_check': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'note': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'paper_invoice': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'pay_type': ('django.db.models.fields.related.ForeignKey', [], {'default': '2', 'to': "orm['kg.PayType']"}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'phone2': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'sepa_mandate_id': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'street': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True'}),
            'surname': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'suspension_reason': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['auth.User']", 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'zip_code': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True'})
        },
        'kg.customeraccounttransaction': {
            'Meta': {'ordering': "['-transaction_date']", 'object_name': 'CustomerAccountTransaction'},
            'amount': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '8', 'decimal_places': '2'}),
            'bat_hash': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'connected_to': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'customer': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['kg.Customer']"}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '1000', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'invoice_id': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'last_modified': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'on_next_invoice': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'reminder_id': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'status': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'transaction_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'transaction_id': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'type': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        'kg.facility': {
            'Meta': {'ordering': "['name']", 'object_name': 'Facility'},
            'flatrate_customizable': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name_short': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'tax_rate': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '19'}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['kg.FacilityType']"})
        },
        'kg.facilitysubunit': {
            'Meta': {'ordering': "['sortKey', 'name']", 'object_name': 'FacilitySubunit'},
            'facility': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['kg.Facility']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mealplan': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['menus.MealPlan']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name_short': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'price_group': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['menus.PriceGroup']"}),
            'sortKey': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'})
        },
        'kg.facilitytype': {
            'Meta': {'ordering': "['name']", 'object_name': 'FacilityType'},
            'id': ('django.db.models.fields.PositiveSmallIntegerField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'name_short': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'})
        },
        'kg.flatrate': {
            'Meta': {'unique_together': "(('child', 'meal', 'weekday'),)", 'object_name': 'Flatrate'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'active_on_holidays': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'child': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'flat'", 'to': "orm['kg.Child']"}),
            'created_by': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'created_flatrate'", 'null': 'True', 'to': "orm['auth.User']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_modified': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'meal': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['menus.Meal']"}),
            'modified_by': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'modified_flatrate'", 'null': 'True', 'to': "orm['auth.User']"}),
            'weekday': ('django.db.models.fields.SmallIntegerField', [], {})
        },
        'kg.holiday': {
            'Meta': {'ordering': "['-date_start']", 'object_name': 'Holiday'},
            'date_end': ('django.db.models.fields.DateField', [], {}),
            'date_start': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'kg.invoicelog': {
            'Meta': {'unique_together': "(('child', 'invoice_date'),)", 'object_name': 'InvoiceLog'},
            'bank_account_bic': ('django.db.models.fields.CharField', [], {'max_length': '11', 'blank': 'True'}),
            'bank_account_iban': ('django.db.models.fields.CharField', [], {'max_length': '34', 'blank': 'True'}),
            'bank_account_owner': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'cat_created': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'child': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['kg.Child']"}),
            'facility': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['kg.Facility']", 'null': 'True'}),
            'first_downloaded': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'invoice_date': ('django.db.models.fields.DateField', [], {}),
            'invoice_file': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'invoice_number': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'invoice_subtotal': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '5', 'decimal_places': '2'}),
            'invoice_total': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '5', 'decimal_places': '2'}),
            'notification_sent': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'pay_type_id': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'pay_until': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(1970, 1, 1, 0, 0)'}),
            'released': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'sepa_description': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '140', 'blank': 'True'}),
            'sepa_mandate_date': ('django.db.models.fields.DateField', [], {'null': 'True'}),
            'sepa_mandate_reference': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '35', 'blank': 'True'}),
            'sepa_seq_type': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '4', 'blank': 'True'}),
            'sepa_transaction_id': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '50', 'blank': 'True'})
        },
        'kg.order': {
            'Meta': {'unique_together': "(('child', 'menu'),)", 'object_name': 'Order'},
            'cancel_reason': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['kg.CancelReason']", 'null': 'True', 'blank': 'True'}),
            'canceled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'child': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['kg.Child']"}),
            'created_by': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'created_orders'", 'null': 'True', 'to': "orm['auth.User']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_modified': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'menu': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['menus.Menu']"}),
            'modified_by': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'modified_orders'", 'null': 'True', 'to': "orm['auth.User']"}),
            'pickup_time': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'price': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '4', 'decimal_places': '2', 'blank': 'True'}),
            'reduction': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['kg.Reduction']", 'null': 'True', 'blank': 'True'})
        },
        'kg.orderhistory': {
            'Meta': {'ordering': "('last_modified',)", 'object_name': 'OrderHistory'},
            'canceled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'child': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['kg.Child']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_modified': ('django.db.models.fields.DateTimeField', [], {'blank': 'True'}),
            'menu': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['menus.Menu']"}),
            'modified_by': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'modified_order_history'", 'null': 'True', 'to': "orm['auth.User']"})
        },
        'kg.paytype': {
            'Meta': {'ordering': "['id']", 'object_name': 'PayType'},
            'id': ('django.db.models.fields.PositiveSmallIntegerField', [], {'primary_key': 'True'}),
            'new_customers': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'kg.reduction': {
            'Meta': {'ordering': "['name']", 'object_name': 'Reduction'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'reduction': ('django.db.models.fields.DecimalField', [], {'max_digits': '4', 'decimal_places': '2'})
        },
        'kg.reminder': {
            'Meta': {'ordering': "['-number']", 'object_name': 'Reminder'},
            'content': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'create_date': ('django.db.models.fields.DateField', [], {}),
            'customer': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['kg.Customer']"}),
            'filename': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'first_downloaded': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'number': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'to': "orm['kg.Reminder']"}),
            'pay_until': ('django.db.models.fields.DateField', [], {}),
            'status': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'total': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '5', 'decimal_places': '2'}),
            'type': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1'})
        },
        'kg.sepadebit': {
            'Meta': {'object_name': 'SepaDebit'},
            'amount': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '5', 'decimal_places': '2'}),
            'bank_account_bic': ('django.db.models.fields.CharField', [], {'max_length': '11'}),
            'bank_account_iban': ('django.db.models.fields.CharField', [], {'max_length': '34'}),
            'bank_account_owner': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'customer': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['kg.Customer']"}),
            'debit_received': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'description': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '140', 'blank': 'True'}),
            'download': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['kg.SepaDebitDownload']", 'null': 'True'}),
            'endtoend_id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '35'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mandate_date': ('django.db.models.fields.DateField', [], {}),
            'mandate_reference': ('django.db.models.fields.CharField', [], {'max_length': '35'}),
            'sequence_type': ('django.db.models.fields.CharField', [], {'max_length': '4'}),
            'submission_time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        'kg.sepadebitdownload': {
            'Meta': {'ordering': "('-first_downloaded', 'sequence_type')", 'object_name': 'SepaDebitDownload'},
            'first_downloaded': ('django.db.models.fields.DateTimeField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message_id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '35'}),
            'sequence_type': ('django.db.models.fields.CharField', [], {'max_length': '4'})
        },
        'kg.sepamandate': {
            'Meta': {'object_name': 'SepaMandate'},
            'bank': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'bic': ('django.db.models.fields.CharField', [], {'max_length': '11'}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'customer': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['kg.Customer']"}),
            'iban': ('django.db.models.fields.CharField', [], {'max_length': '34'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_valid': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'last_debit_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'last_debit_sequence': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '4', 'blank': 'True'}),
            'last_modified': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'mandate_id': ('django.db.models.fields.CharField', [], {'default': "'MA'", 'unique': 'True', 'max_length': '30'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'note': ('django.db.models.fields.TextField', [], {'default': "''", 'blank': 'True'}),
            'on_paper': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'street': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'kg.viewpermission': {
            'Meta': {'object_name': 'ViewPermission'},
            'dummy_content': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'menus.additional': {
            'Meta': {'ordering': "['sortKey']", 'object_name': 'Additional'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name_short': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'sortKey': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'})
        },
        'menus.allergen': {
            'Meta': {'ordering': "['name', 'name_short']", 'object_name': 'Allergen'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name_short': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'use_for_menu': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'menus.meal': {
            'Meta': {'ordering': "['sortKey']", 'object_name': 'Meal'},
            'allow_order': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'cancel_day_limit': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'cancel_time_limit': ('django.db.models.fields.TimeField', [], {'default': "'00:00:00'"}),
            'deleted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mealtime': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['menus.Mealtime']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'name_pos_app': ('django.db.models.fields.CharField', [], {'max_length': '15', 'blank': 'True'}),
            'name_public_menu': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'name_short': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'order_day_limit': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'order_time_limit': ('django.db.models.fields.TimeField', [], {'default': "'00:00:00'"}),
            'show_on_public_menu': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'show_price': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'sortKey': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        'menus.mealplan': {
            'Meta': {'ordering': "['name']", 'object_name': 'MealPlan'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'meals': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['menus.Meal']", 'symmetrical': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'menus.mealtime': {
            'Meta': {'ordering': "['sortKey']", 'object_name': 'Mealtime'},
            'id': ('django.db.models.fields.PositiveSmallIntegerField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'name_short': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'sortKey': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        'menus.menu': {
            'Meta': {'ordering': "['-date', 'meal']", 'unique_together': "(('meal', 'date'),)", 'object_name': 'Menu'},
            'additionals': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['menus.Additional']", 'symmetrical': 'False'}),
            'allergens': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['menus.Allergen']", 'symmetrical': 'False'}),
            'can_be_ordered': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'cancel_timelimit': ('django.db.models.fields.DateTimeField', [], {}),
            'date': ('django.db.models.fields.DateField', [], {}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'meal': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['menus.Meal']"}),
            'order_timelimit': ('django.db.models.fields.DateTimeField', [], {})
        },
        'menus.pricegroup': {
            'Meta': {'ordering': "['name']", 'object_name': 'PriceGroup'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        }
    }

    complete_apps = ['kg']
