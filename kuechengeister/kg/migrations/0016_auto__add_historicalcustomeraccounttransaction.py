# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'HistoricalCustomerAccountTransaction'
        db.create_table('kg_historicalcustomeraccounttransaction', (
            ('id', self.gf('django.db.models.fields.IntegerField')(db_index=True, blank=True)),
            ('history_message', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('bat_hash', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('transaction_id', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('amount', self.gf('django.db.models.fields.DecimalField')(default=0, max_digits=8, decimal_places=2)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=1000, blank=True)),
            ('type', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('transaction_date', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('status', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('connected_to', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('invoice_id', self.gf('django.db.models.fields.PositiveIntegerField')(default=0)),
            ('reminder_id', self.gf('django.db.models.fields.PositiveIntegerField')(default=0)),
            ('on_next_invoice', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('last_modified', self.gf('django.db.models.fields.DateTimeField')(blank=True)),
            ('customer', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name=u'+', null=True, on_delete=models.DO_NOTHING, to=orm['kg.Customer'])),
            (u'history_id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            (u'history_date', self.gf('django.db.models.fields.DateTimeField')()),
            (u'history_user', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'+', null=True, on_delete=models.SET_NULL, to=orm['auth.User'])),
            (u'history_type', self.gf('django.db.models.fields.CharField')(max_length=1)),
        ))
        db.send_create_signal('kg', ['HistoricalCustomerAccountTransaction'])


    def backwards(self, orm):
        # Deleting model 'HistoricalCustomerAccountTransaction'
        db.delete_table('kg_historicalcustomeraccounttransaction')


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '75'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'kg.actionlog': {
            'Meta': {'ordering': "('-action_time',)", 'object_name': 'ActionLog'},
            'action_flag': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'action_time': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'change_message': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'object_id': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']", 'null': 'True'})
        },
        'kg.bankaccounttransaction': {
            'Meta': {'ordering': "['-t_date', '-amount']", 'object_name': 'BankAccountTransaction'},
            'amount': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '8', 'decimal_places': '2'}),
            'bank_account_number': ('django.db.models.fields.CharField', [], {'max_length': '34', 'blank': 'True'}),
            'bank_account_owner': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'bank_icode': ('django.db.models.fields.CharField', [], {'max_length': '11', 'blank': 'True'}),
            'booking_type': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_modified': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'subject': ('django.db.models.fields.CharField', [], {'max_length': '500', 'blank': 'True'}),
            't_date': ('django.db.models.fields.DateField', [], {}),
            't_hash': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'type': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'})
        },
        'kg.cancelreason': {
            'Meta': {'ordering': "['name']", 'object_name': 'CancelReason'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name_short': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'})
        },
        'kg.child': {
            'Meta': {'ordering': "['name', 'surname']", 'object_name': 'Child'},
            'allergen': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['menus.Allergen']", 'null': 'True', 'blank': 'True'}),
            'birthday': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'chip_deposit': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'chip_deposit_update_time': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'chip_number': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '10', 'blank': 'True'}),
            'chip_number_update_time': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2000, 1, 1, 0, 0)', 'auto_now_add': 'True', 'blank': 'True'}),
            'cust_nbr': ('django.db.models.fields.IntegerField', [], {'unique': 'True', 'blank': 'True'}),
            'customer': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['kg.Customer']", 'null': 'True'}),
            'extra_attribute': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'extra_attribute2': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'facility': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['kg.Facility']", 'null': 'True'}),
            'facility_subunit': ('smart_selects.db_fields.ChainedForeignKey', [], {'to': "orm['kg.FacilitySubunit']", 'null': 'True'}),
            'flatrate': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_modified': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'note': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'order_allergy_meal': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'rfid_tag': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'rfid_tag_update_time': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'surname': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'kg.childreduction': {
            'Meta': {'ordering': "['child__name']", 'object_name': 'ChildReduction'},
            'child': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['kg.Child']"}),
            'date_end': ('django.db.models.fields.DateField', [], {}),
            'date_start': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'reduction': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['kg.Reduction']"}),
            'reference_number': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'})
        },
        'kg.customer': {
            'Meta': {'ordering': "['name', 'surname']", 'object_name': 'Customer'},
            'account_balance': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '8', 'decimal_places': '2'}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2000, 1, 1, 0, 0)', 'auto_now_add': 'True', 'blank': 'True'}),
            'credit_minimum': ('django.db.models.fields.DecimalField', [], {'default': "'20.00'", 'max_digits': '5', 'decimal_places': '2'}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '75', 'blank': 'True'}),
            'extra_attribute': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'extra_attribute2': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_suspended': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_modified': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'new_password': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'new_password_check': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'note': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'paper_invoice': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'pay_type': ('django.db.models.fields.related.ForeignKey', [], {'default': '2', 'to': "orm['kg.PayType']"}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'phone2': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'street': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'surname': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'suspension_reason': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['auth.User']", 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'zip_code': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'})
        },
        'kg.customeraccounttransaction': {
            'Meta': {'ordering': "['-transaction_date']", 'object_name': 'CustomerAccountTransaction'},
            'amount': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '8', 'decimal_places': '2'}),
            'bat_hash': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'connected_to': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'customer': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['kg.Customer']"}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '1000', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'invoice_id': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'last_modified': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'on_next_invoice': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'reminder_id': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'status': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'transaction_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'transaction_id': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'type': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        'kg.facility': {
            'Meta': {'ordering': "['name']", 'object_name': 'Facility'},
            'flatrate_customizable': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name_short': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'tax_rate': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '19'}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['kg.FacilityType']"})
        },
        'kg.facilitysubunit': {
            'Meta': {'ordering': "['sortKey', 'name']", 'object_name': 'FacilitySubunit'},
            'facility': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['kg.Facility']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mealplan': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['menus.MealPlan']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name_short': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'price_group': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['menus.PriceGroup']"}),
            'sortKey': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'tax_rate': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '9999'})
        },
        'kg.facilitytype': {
            'Meta': {'ordering': "['name']", 'object_name': 'FacilityType'},
            'id': ('django.db.models.fields.PositiveSmallIntegerField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'name_short': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'})
        },
        'kg.flatrate': {
            'Meta': {'unique_together': "(('child', 'meal', 'weekday'),)", 'object_name': 'Flatrate'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'active_on_holidays': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'child': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'flat'", 'to': "orm['kg.Child']"}),
            'created_by': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'created_flatrate'", 'null': 'True', 'to': "orm['auth.User']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_modified': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'meal': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['menus.Meal']"}),
            'modified_by': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'modified_flatrate'", 'null': 'True', 'to': "orm['auth.User']"}),
            'weekday': ('django.db.models.fields.SmallIntegerField', [], {})
        },
        'kg.historicalcustomeraccounttransaction': {
            'Meta': {'ordering': "(u'-history_date', u'-history_id')", 'object_name': 'HistoricalCustomerAccountTransaction'},
            'amount': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '8', 'decimal_places': '2'}),
            'bat_hash': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'connected_to': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'customer': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'+'", 'null': 'True', 'on_delete': 'models.DO_NOTHING', 'to': "orm['kg.Customer']"}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '1000', 'blank': 'True'}),
            u'history_date': ('django.db.models.fields.DateTimeField', [], {}),
            u'history_id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'history_message': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'history_type': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            u'history_user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'+'", 'null': 'True', 'on_delete': 'models.SET_NULL', 'to': "orm['auth.User']"}),
            'id': ('django.db.models.fields.IntegerField', [], {'db_index': 'True', 'blank': 'True'}),
            'invoice_id': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'last_modified': ('django.db.models.fields.DateTimeField', [], {'blank': 'True'}),
            'on_next_invoice': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'reminder_id': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'status': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'transaction_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'transaction_id': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'type': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        'kg.holiday': {
            'Meta': {'ordering': "['-date_start']", 'object_name': 'Holiday'},
            'date_end': ('django.db.models.fields.DateField', [], {}),
            'date_start': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'kg.invoicelog': {
            'Meta': {'unique_together': "(('child', 'invoice_date'),)", 'object_name': 'InvoiceLog'},
            'bank_account_bic': ('django.db.models.fields.CharField', [], {'max_length': '11', 'blank': 'True'}),
            'bank_account_iban': ('django.db.models.fields.CharField', [], {'max_length': '34', 'blank': 'True'}),
            'bank_account_owner': ('django.db.models.fields.CharField', [], {'max_length': '70', 'blank': 'True'}),
            'cat_created': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'child': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['kg.Child']"}),
            'facility': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['kg.Facility']", 'null': 'True'}),
            'first_downloaded': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'invoice_date': ('django.db.models.fields.DateField', [], {}),
            'invoice_file': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'invoice_number': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'invoice_subtotal': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '5', 'decimal_places': '2'}),
            'invoice_total': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '5', 'decimal_places': '2'}),
            'notification_sent': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'pay_type_id': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'pay_until': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(1970, 1, 1, 0, 0)'}),
            'released': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'sepa_description': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '140', 'blank': 'True'}),
            'sepa_mandate_date': ('django.db.models.fields.DateField', [], {'null': 'True'}),
            'sepa_mandate_reference': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '35', 'blank': 'True'}),
            'sepa_seq_type': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '4', 'blank': 'True'}),
            'sepa_transaction_id': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '50', 'blank': 'True'})
        },
        'kg.order': {
            'Meta': {'unique_together': "(('child', 'menu'),)", 'object_name': 'Order'},
            'child': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['kg.Child']"}),
            'created_by': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'created_orders'", 'null': 'True', 'to': "orm['auth.User']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_modified': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'menu': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['menus.Menu']"}),
            'modified_by': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'modified_orders'", 'null': 'True', 'to': "orm['auth.User']"}),
            'pickup_time': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'price': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '4', 'decimal_places': '2', 'blank': 'True'}),
            'quantity': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'}),
            'reduction': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['kg.Reduction']", 'null': 'True', 'blank': 'True'})
        },
        'kg.orderhistory': {
            'Meta': {'ordering': "('last_modified',)", 'object_name': 'OrderHistory'},
            'child': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['kg.Child']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_modified': ('django.db.models.fields.DateTimeField', [], {'blank': 'True'}),
            'menu': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['menus.Menu']"}),
            'modified_by': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'modified_order_history'", 'null': 'True', 'to': "orm['auth.User']"}),
            'quantity': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'})
        },
        'kg.paytype': {
            'Meta': {'ordering': "['id']", 'object_name': 'PayType'},
            'change_condition_type': ('django.db.models.fields.CharField', [], {'default': "'invoice'", 'max_length': '32'}),
            'change_condition_value': ('django.db.models.fields.PositiveIntegerField', [], {'default': '3'}),
            'editable_sepa_mandate': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'id': ('django.db.models.fields.PositiveSmallIntegerField', [], {'primary_key': 'True'}),
            'new_customers': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'kg.reduction': {
            'Meta': {'ordering': "['name']", 'object_name': 'Reduction'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mealtimes': ('django.db.models.fields.related.ManyToManyField', [], {'default': '[2]', 'to': "orm['menus.Mealtime']", 'symmetrical': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'per_menu': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'reduction': ('django.db.models.fields.DecimalField', [], {'max_digits': '4', 'decimal_places': '2'}),
            'valid_on_holiday': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'kg.reminder': {
            'Meta': {'ordering': "['-number']", 'object_name': 'Reminder'},
            'content': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'create_date': ('django.db.models.fields.DateField', [], {}),
            'customer': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['kg.Customer']"}),
            'filename': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'first_downloaded': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'number': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'to': "orm['kg.Reminder']"}),
            'pay_until': ('django.db.models.fields.DateField', [], {}),
            'status': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'total': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '5', 'decimal_places': '2'}),
            'type': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1'})
        },
        'kg.sepadebit': {
            'Meta': {'object_name': 'SepaDebit'},
            'amount': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '5', 'decimal_places': '2'}),
            'bank_account_bic': ('django.db.models.fields.CharField', [], {'max_length': '11'}),
            'bank_account_iban': ('django.db.models.fields.CharField', [], {'max_length': '34'}),
            'bank_account_owner': ('django.db.models.fields.CharField', [], {'max_length': '70'}),
            'customer': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['kg.Customer']"}),
            'debit_received': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'description': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '140', 'blank': 'True'}),
            'download': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['kg.SepaDebitDownload']", 'null': 'True'}),
            'endtoend_id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '35'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mandate_date': ('django.db.models.fields.DateField', [], {}),
            'mandate_reference': ('django.db.models.fields.CharField', [], {'max_length': '35'}),
            'sequence_type': ('django.db.models.fields.CharField', [], {'max_length': '4'}),
            'submission_time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        'kg.sepadebitdownload': {
            'Meta': {'ordering': "('-first_downloaded', 'sequence_type')", 'object_name': 'SepaDebitDownload'},
            'first_downloaded': ('django.db.models.fields.DateTimeField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message_id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '35'}),
            'sequence_type': ('django.db.models.fields.CharField', [], {'max_length': '4'})
        },
        'kg.sepamandate': {
            'Meta': {'object_name': 'SepaMandate'},
            'bank': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'bic': ('django.db.models.fields.CharField', [], {'max_length': '11'}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'customer': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['kg.Customer']"}),
            'iban': ('django.db.models.fields.CharField', [], {'max_length': '34'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_valid': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'last_debit_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'last_debit_sequence': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '4', 'blank': 'True'}),
            'last_modified': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'mandate_id': ('django.db.models.fields.CharField', [], {'default': "'MA'", 'unique': 'True', 'max_length': '30'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '70'}),
            'note': ('django.db.models.fields.TextField', [], {'default': "''", 'blank': 'True'}),
            'on_paper': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'street': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'kg.viewpermission': {
            'Meta': {'object_name': 'ViewPermission'},
            'dummy_content': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'menus.additional': {
            'Meta': {'ordering': "['sortKey']", 'object_name': 'Additional'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name_short': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'sortKey': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'})
        },
        'menus.allergen': {
            'Meta': {'ordering': "['name', 'name_short']", 'object_name': 'Allergen'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name_short': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'use_for_menu': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'menus.meal': {
            'Meta': {'ordering': "['sortKey']", 'object_name': 'Meal'},
            'allergy_meal': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'allow_order': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'cancel_day_limit': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'cancel_day_limit_days': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'cancel_time_limit': ('django.db.models.fields.TimeField', [], {'default': "'00:00:00'"}),
            'default_menu': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['menus.MenuImport']", 'null': 'True', 'blank': 'True'}),
            'deleted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mealtime': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['menus.Mealtime']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'name_pos_app': ('django.db.models.fields.CharField', [], {'max_length': '15', 'blank': 'True'}),
            'name_public_menu': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'name_short': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'order_day_limit': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'order_day_limit_days': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'order_time_limit': ('django.db.models.fields.TimeField', [], {'default': "'00:00:00'"}),
            'sortKey': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        'menus.mealplan': {
            'Meta': {'ordering': "['name']", 'object_name': 'MealPlan'},
            'duplicate_order_mealtime': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_order_quantity': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'show_price': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'show_price_public': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'weekdays': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '5'})
        },
        'menus.mealtime': {
            'Meta': {'ordering': "['sortKey']", 'object_name': 'Mealtime'},
            'id': ('django.db.models.fields.PositiveSmallIntegerField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'name_short': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'sortKey': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        'menus.menu': {
            'Meta': {'ordering': "['-date', 'meal']", 'unique_together': "(('meal', 'date', 'mealplan'),)", 'object_name': 'Menu'},
            'additionals': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['menus.Additional']", 'symmetrical': 'False'}),
            'allergens': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['menus.Allergen']", 'symmetrical': 'False'}),
            'can_be_ordered': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'cancel_timelimit': ('django.db.models.fields.DateTimeField', [], {}),
            'date': ('django.db.models.fields.DateField', [], {}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'disabled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'meal': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['menus.Meal']"}),
            'mealplan': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'menu_set'", 'null': 'True', 'to': "orm['menus.MealPlan']"}),
            'order_timelimit': ('django.db.models.fields.DateTimeField', [], {}),
            'required_menu': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'dependent_menus'", 'null': 'True', 'to': "orm['menus.Menu']"})
        },
        'menus.menuimport': {
            'Meta': {'object_name': 'MenuImport'},
            'additionals': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['menus.Additional']", 'symmetrical': 'False'}),
            'allergens': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['menus.Allergen']", 'symmetrical': 'False'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mealtimes': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['menus.Mealtime']", 'symmetrical': 'False'})
        },
        'menus.pricegroup': {
            'Meta': {'ordering': "['name']", 'object_name': 'PriceGroup'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mealplan': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['menus.MealPlan']", 'null': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        }
    }

    complete_apps = ['kg']